package com.fuzzythread.tyming.global

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import com.fuzzythread.tyming.global.utils.other.Constants
import com.fuzzythread.tyming.global.utils.other.Constants.Companion.APP_TAG
import timber.log.Timber
import javax.inject.Inject

class NotificationRegistrar @Inject constructor(private val notificationManager: NotificationManager) {

    fun registerApp() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            Timber.tag(APP_TAG).d("No need to register for notification channels on this SDK version")
            return
        }
        configureNotificationChannels()
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun configureNotificationChannels() {
        val importance = NotificationManager.IMPORTANCE_LOW

        // timer notification
        val timerRunningChannel = NotificationChannel(
                Constants.NOTIFICATION.CHANNEL_ID,
                Constants.NOTIFICATION.CHANNEL_NAME,
                importance)
        notificationManager.createNotificationChannels(listOf(timerRunningChannel))
    }
}