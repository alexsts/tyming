package com.fuzzythread.tyming.global

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import androidx.annotation.Nullable
import com.fuzzythread.tyming.global.services.SyncService
import com.fuzzythread.tyming.global.utils.other.Constants
import dagger.android.AndroidInjection
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

abstract class TymingService : Service(), CoroutineScope {
    val binder = LocalBinder()
    var masterJob: Job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + masterJob


    @Nullable
    override fun onBind(intent: Intent): IBinder? {
        Timber.tag(Constants.APP_TAG).d("${getServiceName()} Service bound")
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Timber.tag(Constants.APP_TAG).d("${getServiceName()} Service unbound")
        return true
    }

    abstract fun getServiceName(): String

    fun resetJob(){
        masterJob.cancel()
        masterJob = Job()
    }

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onDestroy() {
        Timber.tag(Constants.APP_TAG).d("${getServiceName()} Service destroyed")
        masterJob.cancel()
        super.onDestroy()
    }

    inner class LocalBinder : Binder() {
        val service: TymingService
            get() = this@TymingService
    }
}
