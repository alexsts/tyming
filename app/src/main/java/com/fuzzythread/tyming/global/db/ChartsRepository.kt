package com.fuzzythread.tyming.global.db

import android.content.Context
import android.graphics.Color
import com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.model.DashboardRepository
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.utils.other.TimeFrame
import com.fuzzythread.tyming.global.utils.other.WEEK_LENGTH
import com.fuzzythread.tyming.global.utils.time.sumTimeEntriesPeriods
import com.fuzzythread.tyming.screens.stats.model.TaskStatsViewModel
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import org.joda.time.*
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.round

open class ChartsRepository(
        private val areaDao: AreaDao,
        private val projectDao: ProjectDao,
        private val taskDao: TaskDao,
        private val timeEntryDao: TimeEntryDao,
        private val context: Context) : GeneralRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {

    val dateTimeFormatterWeekDay: DateTimeFormatter = DateTimeFormat.forPattern("EE")
    val dateTimeFormatterDay: DateTimeFormatter = DateTimeFormat.forPattern("EE d MMM")

    fun getProjectsTimesSortedDesc(dayTimeEntries: List<TimeEntryEntity>): List<Pair<Int, Float>> {
        val projectColorToTimeList = mutableListOf<Pair<Int, Float>>()
        val timeEntriesByDayByProject = dayTimeEntries
                .filter { getTaskEntity(it.task.targetId)?.project?.targetId != null }
                .groupBy { getTaskEntity(it.task.targetId)?.project?.targetId ?: -1 }

        timeEntriesByDayByProject.forEach { timeEntry ->
            val tempTimeEntries = timeEntry.value
            val sumTimeEntries = sumTimeEntriesPeriods(tempTimeEntries)

            // TimeEntry has task and has time:
            if (timeEntry.key >= 0 && sumTimeEntries > 0) {
                projectColorToTimeList.add(Pair(getProjectColor(timeEntry.key), sumTimeEntries))
            }
        }
        return projectColorToTimeList.sortedByDescending { it.second }
    }

    fun getDayBar(projectTimes: List<Pair<Int, Float>>, weekDayCounter: Int): BarDataSet {
        val projectTime = projectTimes.map { it.second }.toFloatArray()
        projectTime[projectTime.size - 1] = projectTime[projectTime.size - 1] + 0.000001f // Fix stack bar label
        val dayBar = BarEntry(weekDayCounter.toFloat(), projectTime)
        val dayBarSet = BarDataSet(arrayListOf(dayBar), "${weekDayCounter}")
        dayBarSet.colors = projectTimes.map { it.first }
        return dayBarSet
    }

    fun getEmptyDayBar(weekDayCounter: Int): BarDataSet {
        val dayBar = BarEntry(weekDayCounter.toFloat(), 0f)
        return BarDataSet(arrayListOf(dayBar), "${weekDayCounter}")
    }

    fun getBarData(dayBars: ArrayList<IBarDataSet>, width: Float = 0.25f): BarData {
        return BarData(dayBars).apply {
            setValueFormatter(object : ValueFormatter() {
                override fun getBarStackedLabel(value: Float, stackedEntry: BarEntry?): String {
                    if (value <= 0 || stackedEntry == null) {
                        return ""
                    }
                    val vals = stackedEntry.yVals
                    if (vals != null) {
                        // find out if we are on top of the stack
                        if (vals[vals.size - 1] == value) {
                            val valueFloat = stackedEntry.y.toDouble()
                            if (valueFloat > 0) {
                                val hours = valueFloat.toInt()
                                val minutes = round((valueFloat % 1) * 60).toInt()
                                // return the "sum" across all stack values
                                return String.format("%2d:%02d", hours, minutes)
                            } else {
                                return ""
                            }
                        } else {
                            return "" // return empty
                        }
                    }
                    return ""
                }

                override fun getFormattedValue(value: Float): String {
                    if (value <= 0) {
                        return ""
                    } else {
                        val hours = value.toInt()
                        val minutes = round((value % 1) * 60).toInt()
                        return String.format("%2d:%02d", hours, minutes)
                    }
                }
            })

            barWidth = width
            setValueTextSize(0f)
            setValueTextColor(Color.parseColor("#9e9e9e"))
            isHighlightEnabled = true
        }
    }

    fun getAverageTime(workedDays: Int, overallWeekTime: Float): Float {
        return when (workedDays) {
            0 -> 0f
            1 -> 0f
            else -> overallWeekTime / workedDays
        }
    }

    fun getSelectedDayTime(selectedDay: DateTime): SelectedDayPreview {
        val tasksData = mutableListOf<TaskStatsViewModel>()

        // Tasks RV:
        val timeEntriesByTask = timeEntryDao.getTimeEntriesByDay(selectedDay)
                .filter { getTaskEntity(it.task.targetId) != null }
                .groupBy { it.task.targetId }
        timeEntriesByTask.forEach { timeEntry ->
            val tempTask = getTaskEntity(timeEntry.key)
            tempTask?.apply {
                val tempProject = getProjectEntity(tempTask.project.targetId)
                TaskStatsViewModel().apply {
                    id = tempTask.id
                    titleTask = tempTask.title
                    titleProject = tempProject?.title ?: ""
                    color = tempProject?.color ?: ""
                    time = sumTimeEntriesPeriods(timeEntry.value)
                }.also {
                    if (it.time > 0) {
                        tasksData.add(it)
                    }
                }
            }
        }
        tasksData.sortByDescending { it.timeFormatted }
        return SelectedDayPreview(
                tasks = tasksData,
                trackedTime = getTimeFormattedForDate(selectedDay),
                formattedDate = dateTimeFormatterDay.print(selectedDay).toUpperCase(Locale.ROOT))
    }

    fun getPreviousTimeFramePeriod(timeFrame: TimeFrame, lastDayOfPreviousTimeFrame: DateTime): Float {
        val firstDayOfTimeFrame = if (timeFrame == TimeFrame.WEEK) {
            lastDayOfPreviousTimeFrame.withDayOfWeek(1)
        } else {
            lastDayOfPreviousTimeFrame.withDayOfMonth(1)
        }
        val previousTimeFrameTimeEntries = timeEntryDao.getTimeEntriesByDates(firstDayOfTimeFrame, lastDayOfPreviousTimeFrame)
        return sumTimeEntriesPeriods(previousTimeFrameTimeEntries)
    }

    data class SelectedDayPreview(val tasks: MutableList<TaskStatsViewModel>, val trackedTime: String, val formattedDate: String)
}
