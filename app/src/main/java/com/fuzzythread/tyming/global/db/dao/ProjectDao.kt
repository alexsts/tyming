package com.fuzzythread.tyming.global.db.dao

import com.fuzzythread.tyming.global.db.dao.util.Dao
import com.fuzzythread.tyming.global.db.entities.*
import com.fuzzythread.tyming.global.services.TimerService
import com.fuzzythread.tyming.global.utils.extensions.endOfDay
import com.fuzzythread.tyming.global.utils.extensions.startOfDay
import com.fuzzythread.tyming.global.utils.other.SortBy
import com.fuzzythread.tyming.global.utils.time.clearTime
import io.objectbox.BoxStore
import io.objectbox.kotlin.query
import org.joda.time.DateTime
import java.util.*


class ProjectDao(store: BoxStore) : Dao<ProjectEntity>(store) {

    override fun addOperation(entity: ProjectEntity): Long {
        return projectBox.put(entity)
    }

    override fun updateOperation(vararg entity: ProjectEntity) {
        store.runInTx {
            projectBox.put(*entity)
        }
    }

    override fun deleteOperation(entity: ProjectEntity) {
        deleteProjectTasksById(entity.id)
        projectBox.remove(entity.id)
    }

    override fun deleteByIdOperation(id: Long) {
        deleteProjectTasksById(id)
        projectBox.remove(id)
    }

    override fun get(id: Long): ProjectEntity? {
        return projectBox.get(id)
    }

    override fun getAll(): MutableList<ProjectEntity> {
        return projectBox.all
    }

    override fun amount(): Long {
        return projectBox.count()
    }

    fun deleteByIdWithTimerService(id: Long, timerService: TimerService? = null) {
        updateVersion()
        deleteProjectTasksById(id, timerService)
        projectBox.remove(id)
    }

    private fun deleteTaskTimeEntriesById(taskId: Long) {
        store.runInTx {
            timeEntryBox.query()
                    .equal(TimeEntryEntity_.taskId, taskId)
                    .build()
                    .find()
                    .forEach {
                        timeEntryBox.remove(it)
                    }
        }
    }

    private fun deleteProjectTasksById(projectId: Long, timerService: TimerService? = null) {
        store.runInTx {
            taskBox.query()
                    .equal(TaskEntity_.projectId, projectId)
                    .build()
                    .find()
                    .forEach {
                        timerService?.stopTimer(it.id, showToast = false)
                        deleteTaskTimeEntriesById(it.id)
                        taskBox.remove(it)
                    }
        }
    }

    fun getSingularProjects(isArchived: Boolean, sortBy: SortBy = SortBy.DESC): MutableList<ProjectEntity> {
        val query = projectBox.query()

        if (isArchived) {
            query.notNull(ProjectEntity_.archivedDate)
        } else {
            query.isNull(ProjectEntity_.archivedDate)
        }

        query.and().equal(ProjectEntity_.areaId, 0)

        when (sortBy) {
            SortBy.DESC -> {
                query.orderDesc(ProjectEntity_.createdDate)
            }
            SortBy.ASC -> {
                query.order(ProjectEntity_.createdDate)
            }
            SortBy.CUSTOM -> {
                query.order(ProjectEntity_.displayOrder)
            }
        }

        return query.build().find()
    }

    fun getSingularProjects(): MutableList<ProjectEntity> {
        return projectBox.query()
                .equal(ProjectEntity_.areaId, 0)
                .orderDesc(ProjectEntity_.createdDate)
                .build()
                .find()
    }

    fun getProjectTasksSorted(projectId: Long, completed: Boolean, sortBy: SortBy = SortBy.DESC): MutableList<TaskEntity> {
        val query = taskBox.query()

        if (completed) {
            query.notNull(TaskEntity_.completedDate)
                    .and()
                    .equal(TaskEntity_.projectId, projectId)
                    .orderDesc(TaskEntity_.completedDate)
        } else {
            query.isNull(TaskEntity_.completedDate)
                    .and()
                    .equal(TaskEntity_.projectId, projectId)

            when (sortBy) {
                SortBy.DESC -> {
                    query.orderDesc(TaskEntity_.createdDate)
                }
                SortBy.ASC -> {
                    query.order(TaskEntity_.createdDate)
                }
                SortBy.CUSTOM -> {
                    query.order(TaskEntity_.displayOrder)
                }
            }
        }

        return query.build().find()
    }

    fun getProjectTimeEntries(projectId: Long): MutableList<TimeEntryEntity> {
        val query = taskBox.query()
        query.equal(TaskEntity_.projectId, projectId)
        val tasks = query.build().find()
        val tasksIds = tasks.map { it.id }
        return timeEntryBox.query {
            filter {
                it.task.targetId in tasksIds
            }
        }.find()
    }

    fun getProjectTasksUncompletedSorted(projectId: Long): MutableList<TaskEntity> {
        val tasks = taskBox
                .query()
                .isNull(TaskEntity_.completedDate)
                .equal(TaskEntity_.projectId, projectId)
                .orderDesc(TaskEntity_.createdDate)
                .build()
                .find()

        return tasks
    }

    fun getAllSorted(isArchived: Boolean): MutableList<ProjectEntity> {
        val query = projectBox.query()

        if (isArchived) {
            query.notNull(ProjectEntity_.archivedDate)
        } else {
            query.isNull(ProjectEntity_.archivedDate)
        }

        return query
                .orderDesc(ProjectEntity_.createdDate)
                .build()
                .find()
    }

    fun collapse(projectId: Long) {
        projectBox.get(projectId)?.apply {
            isCollapsed = !isCollapsed
            projectBox.put(this)
        }
    }

    fun collapseAll() {
        val projects = projectBox.query().isNull(ProjectEntity_.archivedDate).build().find()
        store.runInTx {
            projects.forEach {
                it.isCollapsed = true
                projectBox.put(it)
            }
        }
    }

    fun archive(projectId: Long, timerService: TimerService? = null, markArchived: Boolean) {
        var archiveDate: Date? = null
        if (markArchived) {
            archiveDate = Date()
        }
        store.runInTx {
            updateVersion()
            projectBox.get(projectId)?.apply {
                archivedDate = archiveDate
                isCollapsed = true
                projectBox.put(this)
                this.tasks.forEach { taskEntity ->
                    if (taskEntity.isTimerRunning) {
                        // Stop and save timer:
                        timerService?.stopTimer(taskEntity.id, showToast = false)
                        taskEntity.isTimerRunning = false
                        taskBox.put(taskEntity)
                    }
                    taskEntity.timeEntries.forEach { timeEntryEntity ->
                        timeEntryEntity.archivedDate = archiveDate
                        timeEntryBox.put(timeEntryEntity)
                    }
                }
            }
        }
    }

    /**
     * Projects that have:
     *
     * 1. deadline equals week or less from today [today, tomorrow, week]
     * 2. not archived
     * 3. track progress true
     */
    fun getProjectsWithDeadline(date: DateTime): MutableList<ProjectEntity> {
        val dateCleared = date.clearTime()
        val todayStart = dateCleared.startOfDay().toDate()
        val dayNextWeek = dateCleared.plusDays(7).endOfDay().toDate()

        return projectBox
                .query()
                .equal(ProjectEntity_.trackProgress, true)
                .notNull(ProjectEntity_.deadlineDate)
                .isNull(ProjectEntity_.archivedDate)
                .between(ProjectEntity_.deadlineDate, todayStart, dayNextWeek)
                .build()
                .find()
    }
}
