package com.fuzzythread.tyming.global.db.dao.util

import com.fuzzythread.tyming.global.db.entities.*
import io.objectbox.Box
import io.objectbox.BoxStore
import java.util.*

abstract class Dao<T>(val store: BoxStore) {
    val areaBox: Box<AreaEntity> = store.boxFor(AreaEntity::class.java)
    val taskBox: Box<TaskEntity> = store.boxFor(TaskEntity::class.java)
    val projectBox: Box<ProjectEntity> = store.boxFor(ProjectEntity::class.java)
    val timeEntryBox: Box<TimeEntryEntity> = store.boxFor(TimeEntryEntity::class.java)
    private val versionBox: Box<VersionEntity> = store.boxFor(VersionEntity::class.java)

    fun add(entity: T, ignoreVersion: Boolean = false): Long {
        if (!ignoreVersion) {
            updateVersion()
        }
        return addOperation(entity)
    }

    fun update(vararg entity: T, ignoreVersion: Boolean = false) {
        if (!ignoreVersion) {
            updateVersion()
        }
        updateOperation(*entity)
    }

    fun delete(entity: T, ignoreVersion: Boolean = false) {
        if (!ignoreVersion) {
            updateVersion()
        }
        deleteOperation(entity)
    }

    fun deleteById(id: Long, ignoreVersion: Boolean = false) {
        if (!ignoreVersion) {
            updateVersion()
        }
        deleteByIdOperation(id)
    }

    protected abstract fun addOperation(entity: T): Long
    protected abstract fun updateOperation(vararg entity: T)
    protected abstract fun deleteOperation(entity: T)
    protected abstract fun deleteByIdOperation(id: Long)

    abstract fun get(id: Long): T?
    abstract fun getAll(): MutableList<T>
    abstract fun amount(): Long

    fun dropDatabase() {
        store.runInTx {
            versionBox.removeAll()
            timeEntryBox.removeAll()
            taskBox.removeAll()
            projectBox.removeAll()
            areaBox.removeAll()
        }
    }

    fun getLatestVersion(): VersionEntity {
        val versions = versionBox.all
        return if (versions.size > 0) {
            versionBox.all[0] ?: createNewVersion()
        } else {
            createNewVersion()
        }
    }

    fun setVersion(versionNumber: Long, date: Date) {
        val versions = versionBox.all
        if (versions.size > 0) {
            versionBox.all[0].apply {
                this.number = versionNumber
                this.date = date
            }.also {
                versionBox.put(it)
            }
        } else {
            createNewVersion(date,versionNumber)
        }
    }

    private fun createNewVersion(date: Date = Date(), versionNumber: Long = 1): VersionEntity {
        val newVersion = VersionEntity(date = date, number = versionNumber)
        versionBox.put(newVersion)
        return newVersion
    }

    fun updateVersion() {
        val versions = versionBox.all
        if (versions.size > 0) {
            versionBox.all[0]?.apply {
                this.date = Date()
                this.number = this.number + 1
            }?.also {
                versionBox.put(it)
            }
        } else {
            // create first version entity:
            versionBox.put(VersionEntity(date = Date(), number = 1))
        }
    }
}
