package com.fuzzythread.tyming.global.db.entities

import androidx.annotation.Nullable
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.relation.ToOne
import io.objectbox.annotation.Backlink
import io.objectbox.relation.ToMany
import java.util.*

@Entity
class TaskEntity {
    @Id(assignable = true)
    var id: Long = 0
    var title: String = ""
    var notes: String = ""
    var createdDate: Date? = null
    var deadlineDate: Date? = null
    var completedDate: Date? = null
    var timeElapsed: Long = 0
    var isTimerRunning: Boolean = false
    var displayOrder: Long = 0

    // region relations
    lateinit var project: ToOne<ProjectEntity>
    @Backlink(to = "task")
    lateinit var timeEntries: MutableList<TimeEntryEntity>
    @Backlink(to = "tasks")
    lateinit var tags: ToMany<TagEntity>
    // endregion

    constructor()

    constructor(
            title: String,
            notes: String,
            createdDate: Date,
            deadlineDate: Date? = null,
            completedDate: Date? = null,
            projectId: Long = 0,
            timeElapsed: Long = 0,
            isTimerRunning: Boolean = false,
            displayOrder: Long = 0
    ) {
        this.title = title
        this.notes = notes
        this.createdDate = createdDate
        this.deadlineDate = deadlineDate
        this.completedDate = completedDate
        this.project.targetId = projectId
        this.timeElapsed = timeElapsed
        this.isTimerRunning = isTimerRunning
        this.displayOrder = displayOrder
    }

}
