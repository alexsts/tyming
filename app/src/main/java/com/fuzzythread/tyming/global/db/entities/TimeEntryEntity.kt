package com.fuzzythread.tyming.global.db.entities

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.relation.ToOne
import java.util.*

@Entity
class TimeEntryEntity {
    @Id(assignable = true)
    var id: Long = 0
    var notes: String = ""
    var location: String = ""
    var startDate: Date? = null
    var endDate: Date? = null
    var archivedDate: Date? = null

    // region relations
    lateinit var task: ToOne<TaskEntity>
    // endregion

    constructor()

    constructor(notes: String, location: String = "", startDate: Date?, endDate: Date?, taskId: Long, archivedDate: Date? = null) {
        this.notes = notes
        this.location = location
        this.startDate = startDate
        this.endDate = endDate
        this.archivedDate = archivedDate
        this.task.targetId = taskId
    }
}