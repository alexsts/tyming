package com.fuzzythread.tyming.global.di

import com.fuzzythread.tyming.global.services.TimerService
import com.fuzzythread.tyming.screens.archive.ArchiveActivity
import com.fuzzythread.tyming.screens.journal.ui.JournalActivity
import com.fuzzythread.tyming.screens.journal.ui.JournalFragment
import com.fuzzythread.tyming.screens.home.ui.HomeActivity
import com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.ui.DashboardFragment
import com.fuzzythread.tyming.global.services.SyncService
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.NavigationFragment
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.*
import com.fuzzythread.tyming.screens.launch.LaunchActivity
import com.fuzzythread.tyming.screens.premium.PremiumActivity
import com.fuzzythread.tyming.screens.search.ui.SearchActivity
import com.fuzzythread.tyming.screens.settings.SettingsActivity
import com.fuzzythread.tyming.screens.stats.ui.StatsActivity
import com.fuzzythread.tyming.screens.stats.ui.StatsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AndroidBindingModule {

    /* Activities */

    @ActivityScoped
    @ContributesAndroidInjector
    abstract fun navigationActivity(): HomeActivity

    @ActivityScoped
    @ContributesAndroidInjector
    abstract fun archiveActivity(): ArchiveActivity

    @ActivityScoped
    @ContributesAndroidInjector
    abstract fun journalActivity(): JournalActivity

    @ActivityScoped
    @ContributesAndroidInjector
    abstract fun searchActivity(): SearchActivity

    @ActivityScoped
    @ContributesAndroidInjector
    abstract fun statsActivity(): StatsActivity

    @ActivityScoped
    @ContributesAndroidInjector
    abstract fun launchActivity(): LaunchActivity

    @ActivityScoped
    @ContributesAndroidInjector
    abstract fun settingsActivity(): SettingsActivity

    @ActivityScoped
    @ContributesAndroidInjector
    abstract fun premiumActivity(): PremiumActivity

    /* Services */

    @ContributesAndroidInjector
    abstract fun timerService(): TimerService

    @ContributesAndroidInjector
    abstract fun syncService(): SyncService

    /* Fragments */

    @ContributesAndroidInjector
    abstract fun navigationFragment(): NavigationFragment

    @ContributesAndroidInjector
    abstract fun journalFragment(): JournalFragment

    @ContributesAndroidInjector
    abstract fun statsFragment(): StatsFragment

    @ContributesAndroidInjector
    abstract fun profileFragment(): DashboardFragment

    /* Fragment Dialogs */

    @ContributesAndroidInjector
    abstract fun taskDialog(): TaskDialog

    @ContributesAndroidInjector
    abstract fun projectDialog(): ProjectDialog

    @ContributesAndroidInjector
    abstract fun areaDialog(): AreaDialog

    @ContributesAndroidInjector
    abstract fun timeEntryDialog(): TimeEntryDialog

    @ContributesAndroidInjector
    abstract fun parentDialog(): ParentDialog
}
