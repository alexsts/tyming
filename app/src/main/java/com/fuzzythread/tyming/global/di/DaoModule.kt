package com.fuzzythread.tyming.global.di

import com.fuzzythread.tyming.global.db.AppDatabase
import dagger.Module
import dagger.Provides

@Module
class DaoModule {

    @Provides
    fun provideAreaDao(database: AppDatabase) = database.categoryDao()

    @Provides
    fun provideProjectDao(database: AppDatabase) = database.projectDao()

    @Provides
    fun provideTaskDao(database: AppDatabase) = database.taskDao()

    @Provides
    fun provideTimeEntryDao(database: AppDatabase) = database.timeEntryDao()
}