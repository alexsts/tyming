package com.fuzzythread.tyming.global.di

import android.content.Context
import com.fuzzythread.tyming.global.services.syncing.DatabaseManager
import com.fuzzythread.tyming.global.services.syncing.LocalDbSnapshotFactory
import com.fuzzythread.tyming.global.services.syncing.Synchronizator
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [DatabaseManagerModule::class])
class SyncModule {
    @Provides
    @Singleton
    fun provideSynchronizator(context: Context, databaseManager: DatabaseManager, localDbSnapshotFactory: LocalDbSnapshotFactory): Synchronizator {
        return Synchronizator(context, databaseManager, localDbSnapshotFactory)
    }
}
