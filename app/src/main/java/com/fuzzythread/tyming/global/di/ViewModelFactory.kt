package com.fuzzythread.tyming.global.di

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model.AreaDialogVM
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.utils.time.DefaultTimer
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.HomeFragmentRepository
import com.fuzzythread.tyming.screens.journal.model.JournalRepository
import com.fuzzythread.tyming.screens.launch.model.LaunchRepository
import com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.model.DashboardRepository
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model.ParentDialogVM
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model.ProjectDialogRepository
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model.TaskDialogVM
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model.TimeEntryDialogRepository
import com.fuzzythread.tyming.screens.search.model.SearchRepository
import com.fuzzythread.tyming.screens.stats.model.StatsRepository
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class ViewModelFactory @Inject constructor(

        private val areaDao: AreaDao,
        private val projectDao: ProjectDao,
        private val taskDao: TaskDao,
        private val timeEntryDao: TimeEntryDao,
        private val timer: DefaultTimer,
        private val context: Context
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(DashboardRepository::class.java) -> DashboardRepository(areaDao, projectDao, taskDao, timeEntryDao, context)
                    isAssignableFrom(StatsRepository::class.java) -> StatsRepository(areaDao, projectDao, taskDao, timeEntryDao, context)
                    isAssignableFrom(SearchRepository::class.java) -> SearchRepository(areaDao, projectDao, taskDao, timeEntryDao, context)
                    isAssignableFrom(HomeFragmentRepository::class.java) -> HomeFragmentRepository(areaDao, projectDao, taskDao, timeEntryDao, context)
                    isAssignableFrom(LaunchRepository::class.java) -> LaunchRepository(areaDao, projectDao, taskDao, timeEntryDao, context)
                    isAssignableFrom(JournalRepository::class.java) -> JournalRepository(areaDao, projectDao, taskDao, timeEntryDao, context)
                    isAssignableFrom(ParentDialogVM::class.java) -> ParentDialogVM(areaDao, projectDao, taskDao, timeEntryDao, context)
                    isAssignableFrom(ProjectDialogRepository::class.java) -> ProjectDialogRepository(areaDao, projectDao, taskDao, timeEntryDao, context)
                    isAssignableFrom(TaskDialogVM::class.java) -> TaskDialogVM(areaDao, projectDao, taskDao, timeEntryDao, context)
                    isAssignableFrom(AreaDialogVM::class.java) -> AreaDialogVM(areaDao, projectDao, taskDao, timeEntryDao, context)
                    isAssignableFrom(TimeEntryDialogRepository::class.java) -> TimeEntryDialogRepository(areaDao, taskDao, projectDao, timeEntryDao, context)
                    else -> throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T
}
