package com.fuzzythread.tyming.global.exception;

public class DurationNotSelectedException extends RuntimeException {
    public DurationNotSelectedException() {
        super("Please select a duration");
    }
}
