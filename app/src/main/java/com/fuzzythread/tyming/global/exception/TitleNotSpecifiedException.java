package com.fuzzythread.tyming.global.exception;

public class TitleNotSpecifiedException extends RuntimeException {
    public TitleNotSpecifiedException(String entityType) {
        super("Please specify name of " + entityType.toLowerCase());
    }
}
