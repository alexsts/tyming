package com.fuzzythread.tyming.global.services.syncing

import android.content.Context
import com.fuzzythread.tyming.global.utils.extensions.createGsonBuilder
import com.fuzzythread.tyming.global.utils.extensions.isNetworkAvailable
import com.fuzzythread.tyming.global.utils.extensions.justTry
import com.fuzzythread.tyming.global.utils.other.Constants
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageException
import com.google.firebase.storage.StorageMetadata
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.component1
import com.google.firebase.storage.ktx.component2
import com.google.firebase.storage.ktx.storageMetadata
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class Synchronizator @Inject constructor(
        var context: Context,
        var databaseManager: DatabaseManager,
        var localDbSnapshotFactory: LocalDbSnapshotFactory
) {
    private var boundLoadingCallback: ((Boolean, Boolean) -> Unit)? = null
    private var storage: FirebaseStorage? = null
    private var auth: FirebaseAuth? = null

    fun setCallback(callback: ((isLoading: Boolean, updatedUI: Boolean) -> Unit)?) {
        this.boundLoadingCallback = callback
    }

    fun removeCallback() {
        this.boundLoadingCallback = null
    }

    fun sync() {
        isUserAuthenticated(auth, { uid: String ->
            justTry {
                if (isNetworkAvailable(context)) {
                    Timber.tag(Constants.APP_TAG).d("Started synchronization")
                    boundLoadingCallback?.invoke(true, false)
                    storage?.apply {
                        val fileRef = getFileReference(this, uid)
                        val request = fileRef.getBytes(ONE_MEGABYTE)
                        request.addOnSuccessListener { bytes ->
                            if (bytes != null) {
                                Timber.tag(Constants.APP_TAG).d("Download is completed")
                                handleServerResponse(bytes)
                            } else {
                                Timber.tag(Constants.APP_TAG).d("DB snapshot does not exist")
                                storeLocalDBToServer()
                            }
                        }.addOnFailureListener { result ->
                            Timber.tag(Constants.APP_TAG).d("Download is failed")
                            if (result is StorageException && result.errorCode == StorageException.ERROR_OBJECT_NOT_FOUND) {
                                Timber.tag(Constants.APP_TAG).d("DB snapshot does not exist")
                                storeLocalDBToServer()
                            }
                        }.addOnCompleteListener {
                            if (request.isSuccessful) {
                                Timber.tag(Constants.APP_TAG).d("Synchronization completed")
                                PrefUtil.setLastSyncDate(Date(), context)
                            } else {
                                Timber.tag(Constants.APP_TAG).e("Request failed")
                                boundLoadingCallback?.invoke(false, false)
                            }
                        }
                    }
                } else {
                    Timber.tag(Constants.APP_TAG).e("Network is not available")
                }
            }
        }, { authenticate() })
    }

    private fun handleServerResponse(bytes: ByteArray) {
        val json = String(bytes)
        val jsonDbFactory = JsonDbSnapshotFactory(json)
        val localVersion = localDbSnapshotFactory.getLatestVersion().number
        Timber.tag(Constants.APP_TAG).d("Processing server response (Local DB version: $localVersion, Server DB version: ${jsonDbFactory.version})")
        when {
            localVersion < jsonDbFactory.version -> {
                Timber.tag(Constants.APP_TAG).d("Local DB is outdated")
                databaseManager.drop()
                databaseManager.create(jsonDbFactory)
                boundLoadingCallback?.invoke(false, true)
            }
            localVersion > jsonDbFactory.version -> {
                Timber.tag(Constants.APP_TAG).d("Server DB snapshot is outdated")
                storeLocalDBToServer()
            }
            else -> {
                Timber.tag(Constants.APP_TAG).d("Both Server and Local DB synchronized")
                boundLoadingCallback?.invoke(false, false)
            }
        }
    }

    private fun storeLocalDBToServer() {
        isUserAuthenticated(auth, { uid: String ->
            val currentDB = localDbSnapshotFactory.createDbSnapshot()
            val json = createGsonBuilder().toJson(currentDB)

            storage?.apply {
                val fileRef = getFileReference(this, uid)
                val uploadTask = fileRef.putBytes(json.toByteArray(), getJsonMetadata())

                uploadTask.addOnFailureListener {
                    Timber.tag(Constants.APP_TAG).e("Upload is failed")
                }.addOnSuccessListener { taskSnapshot ->
                    Timber.tag(Constants.APP_TAG).d("Upload is completed")
                }.addOnProgressListener { (bytesTransferred, totalByteCount) ->
                    val progress = (100.0 * bytesTransferred) / totalByteCount
                    Timber.tag(Constants.APP_TAG).d("Upload is $progress% done".toString())
                }.addOnPausedListener {
                    Timber.tag(Constants.APP_TAG).d("Upload is paused")
                }
                boundLoadingCallback?.invoke(false, false)
            }
        }, { authenticate() })
    }

    private inline fun isUserAuthenticated(auth: FirebaseAuth?, block: (uid: String) -> Unit, retryAuthBlock: () -> Unit) {
        val uid = auth?.currentUser?.uid
        if (uid == null) {
            Timber.tag(Constants.APP_TAG).d("User is not authenticated")
            retryAuthBlock()
            return
        } else {
            block(uid)
        }
    }

    private fun getJsonMetadata(): StorageMetadata {
        return storageMetadata {
            contentType = "application/json"
        }
    }

    private fun getFileReference(storage: FirebaseStorage, uid: String): StorageReference {
        return storage.reference.child("users").child(uid).child("db.json")
    }

    private fun authenticate() {
        boundLoadingCallback?.invoke(false, false)
        auth = FirebaseAuth.getInstance()
        storage = FirebaseStorage.getInstance()
    }

    fun syncDifferentAccount() {
        authenticate() // update auth
        isUserAuthenticated(auth, { uid: String ->
            justTry {
                if (isNetworkAvailable(context)) {
                    Timber.tag(Constants.APP_TAG).d("Check if Db exists")
                    boundLoadingCallback?.invoke(true, false)
                    storage?.apply {
                        val fileRef = getFileReference(this, uid)
                        val request = fileRef.getBytes(ONE_MEGABYTE)
                        request.addOnCompleteListener { result ->
                            // database exists for user:
                            if (result.isSuccessful) {
                                Timber.tag(Constants.APP_TAG).e("Database exist for different user, dropping local DB")
                                databaseManager.drop()
                            } else {
                                Timber.tag(Constants.APP_TAG).e("Database does not exist for different user")
                            }
                            sync()
                        }
                    }
                }
            }
        }, { authenticate() })
    }

    companion object {
        const val ONE_MEGABYTE: Long = 1024 * 1024
    }
}
