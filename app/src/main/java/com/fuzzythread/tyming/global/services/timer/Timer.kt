package com.fuzzythread.tyming.global.services.timer

import com.fuzzythread.tyming.global.services.TimerService
import com.fuzzythread.tyming.global.services.TimerService.TimerState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicLong

class Timer(
        var timeElapsed: AtomicLong = AtomicLong(0),
        var timerState: TimerState = TimerState.STOPPED,
        var coroutineScope: CoroutineScope
) {
    private var timerJob: Job = Job()

    init {
        if (timerState == TimerState.STARTED) {
            start()
        }
    }

    fun start() {
        timerState = TimerState.STARTED
        coroutineScope.launch(timerJob) {
            while (true) {
                delay(TimerService.TIMER_REPEAT_MILLIS)
                timeElapsed.getAndIncrement()
            }
        }
    }

    fun pause() {
        timerState = TimerState.PAUSED
        timerJob.cancel()
    }

    fun stop() {
        timerState = TimerState.STOPPED
        timerJob.cancel()
    }
}
