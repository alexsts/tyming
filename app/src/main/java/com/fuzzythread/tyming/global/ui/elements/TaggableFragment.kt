package com.fuzzythread.tyming.global.ui.elements

abstract class TaggableFragment : TymingFragment() {
    var updateTimers: Boolean = false
    abstract fun getTAG(): String
    abstract fun refreshFragment()
    abstract fun showTutorial()
}
