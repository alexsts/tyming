package com.fuzzythread.tyming.global.ui.elements

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import dagger.android.support.DaggerAppCompatDialogFragment


abstract class TymingDialog : DaggerAppCompatDialogFragment() {
    private val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?

    fun hideKeyboard() {
        if (this.activity != null) {
            val view: View? = this.activity!!.currentFocus
            if (view != null) {
                val imm = this.activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                imm?.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
    }
}
