package com.fuzzythread.tyming.global.ui.helpers

interface IAdapterComparable<T> {
    /**
     * Compares an item implementing this interface to a passed item.
     */
    fun areItemsTheSame(itemToCompare: T): Boolean

    /**
     * Compares a content of an item implementing this interface to a content of the passed item.
     */
    fun areContentsTheSame(itemToCompare: T): Boolean
}