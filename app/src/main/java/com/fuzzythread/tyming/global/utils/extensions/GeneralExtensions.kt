package com.fuzzythread.tyming.global.utils.extensions

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.fuzzythread.tyming.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.util.*
import kotlin.math.round

inline fun debugMode(block: () -> Unit) {
    if (BuildConfig.DEBUG) {
        block()
    }
}

inline fun oreoAndAbove(block: () -> Unit) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        block()
    }
}

inline fun withPermissions(context: Context, block: () -> Unit) {
    try {
        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            block()
        }
    } catch (e: SecurityException) {

    }
}

inline fun <T> justTry(block: () -> T) = try {
    block()
} catch (e: Exception) {
}

fun IntRange.random() =
        Random().nextInt((endInclusive + 1) - start) +  start

fun Double.round(decimals: Int): Double {
    var multiplier = 1.0
    repeat(decimals) { multiplier *= 10 }
    return round(this * multiplier) / multiplier
}

fun Float.round(decimals: Int): Float {
    var multiplier = 1.0
    repeat(decimals) { multiplier *= 10 }
    return (round(this * multiplier) / multiplier).toFloat()
}

fun createGsonBuilder(): Gson {
    return GsonBuilder()
            .setDateFormat("EEE, dd MMM yyyy HH:mm:ss")
            .create()
}

fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    val activeNetworkInfo = connectivityManager!!.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}

fun allPermissionsGranted(context: Context): Boolean {
    return isGranted(context, Manifest.permission.READ_EXTERNAL_STORAGE) &&
            isGranted(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
            isGranted(context, Manifest.permission.INTERNET)
}

fun isGranted(context: Context, permission: String): Boolean {
    val checkVal = context.checkCallingOrSelfPermission(permission)
    return checkVal == PackageManager.PERMISSION_GRANTED
}
