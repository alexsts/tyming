package com.fuzzythread.tyming.global.utils.extensions

import com.fuzzythread.tyming.global.db.entities.AreaEntity
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.AreaViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TaskViewModel
import java.util.*


fun TaskViewModel.toEntity(): TaskEntity {
    return TaskEntity(
            title = this.title,
            notes = this.notes,
            createdDate = this.createdDate ?: Date(),
            deadlineDate = this.deadlineDate,
            completedDate = this.completedDate,
            projectId = this.parentId,
            timeElapsed = this.timeElapsed,
            isTimerRunning = this.timerRunning,
            displayOrder = this.displayOrder
    ).also {
        it.id = id
    }
}

fun AreaViewModel.toEntity(): AreaEntity {
    return AreaEntity(
            title = this.title,
            notes = this.notes,
            color = this.color,
            isCollapsed = this.collapsed,
            archivedDate = this.archivedDate,
            createdDate = this.createdDate ?: Date(),
            displayOrder = this.displayOrder
    ).also {
        it.id = id
    }
}

fun ProjectViewModel.toEntity(): ProjectEntity {
    return ProjectEntity(
            title = this.title,
            notes = this.notes,
            color = this.color,
            deadlineDate = this.deadlineDate,
            createdDate = this.createdDate ?: Date(),
            archivedDate = this.archivedDate,
            areaId = this.area?.id ?: 0,
            trackProgress = this.trackProgress,
            isCollapsed = this.collapsed,
            displayOrder = this.displayOrder
    ).also {
        it.id = id
    }
}
