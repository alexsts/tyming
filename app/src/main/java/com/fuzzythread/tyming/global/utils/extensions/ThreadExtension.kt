package com.fuzzythread.tyming.global.utils.extensions

import android.os.Looper

fun Thread.isAndroidMainThread(): Boolean =
        Thread.currentThread().id == Looper.getMainLooper().thread.id