package com.fuzzythread.tyming.global.utils.extensions

import org.joda.time.DateTime

fun DateTime.endOfDay() : DateTime {
    return this.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59)
}

fun DateTime.startOfDay() : DateTime {
    return this.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0)
}