package com.fuzzythread.tyming.global.utils.logs

import android.annotation.SuppressLint
import android.content.Context
import android.os.Environment
import java.io.File.separator
import android.os.Environment.DIRECTORY_DOWNLOADS
import android.os.Environment.getExternalStoragePublicDirectory
import android.util.Log
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class FileLogsTree(private val context: Context) : Timber.DebugTree() {

    @SuppressLint("LogNotTimber")
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        try {
            val direct = File(Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    .toString() + "/TymingLogs")
            if (!direct.exists()) {
                direct.mkdir()
            }
            val fileNameTimeStamp = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date())
            val logTimeStamp = SimpleDateFormat("E MMM dd yyyy 'at' hh:mm:ss:SSS aaa", Locale.getDefault()).format(Date())
            val fileName = "$fileNameTimeStamp.html"
            val file = File(getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS).toString() + "/TymingLogs" + separator + fileName)
            file.createNewFile()
            if (file.exists()) {
                val fileOutputStream = FileOutputStream(file, true)
                fileOutputStream
                        .write(("<p style=\"background:lightgray;\"><strong style=\"background:lightblue;\">&nbsp&nbsp" +
                                "$logTimeStamp :&nbsp&nbsp</strong>&nbsp&nbsp$message</p>")
                                .toByteArray())
                fileOutputStream.close()
            }
        } catch (e: Exception) {
            Log.e("FileLogsTree", "Error while logging into file : $e")
        }
    }
}