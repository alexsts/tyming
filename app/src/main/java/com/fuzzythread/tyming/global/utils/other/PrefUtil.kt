package com.fuzzythread.tyming.global.utils.other

import android.content.Context
import androidx.preference.PreferenceManager
import java.util.*

class PrefUtil {

    companion object {
        private const val TIME_ELAPSED_ID = "com.fuzzythread.tyming.time_elapsed"
        private const val TIMER_STATE_ID = "com.fuzzythread.tyming.timer_state"
        private const val USERNAME = "com.fuzzythread.tyming.username"
        private const val TIMERS = "com.fuzzythread.tyming.timers"
        private const val TUTORIALS = "com.fuzzythread.tyming.tutorials"
        private const val TASK_ID = "com.fuzzythread.tyming.task_id"
        private const val TIMERS_MULTIPLE = "com.fuzzythread.tyming.timers_multiple"
        private const val TUTORIAL_COMPLETED = "com.fuzzythread.tyming.tutorial_completed"
        private const val GOAL_TRACKING = "com.fuzzythread.tyming.goal_tracking"
        private const val GENERATE_DATA = "com.fuzzythread.tyming.generate_data"
        private const val GOAL_TRACKING_TIME_HOURS = "com.fuzzythread.tyming.goal_tracking_time_hours"
        private const val GOAL_TRACKING_TIME_MINUTES = "com.fuzzythread.tyming.goal_tracking_time_minutes"
        private const val TIME_PERIOD = "com.fuzzythread.tyming.time_period"
        private const val ENABLE_AUTO_SYNC = "com.fuzzythread.tyming.enable_auto_sync"
        private const val LAST_SYNC_DATE = "com.fuzzythread.tyming.last_sync_date"
        private const val SORT_BY_JOURNAL = "com.fuzzythread.tyming.sort_by_journal"
        private const val SORT_BY_PROJECTS = "com.fuzzythread.tyming.sort_by_projects"
        private const val DISPLAY_MODE = "com.fuzzythread.tyming.display_mode"
        private const val APP_INITIALIZED = "com.fuzzythread.tyming.app_initialized"
        private const val DISPLAY_COMPLETED_TASKS = "com.fuzzythread.tyming.display_completed_tasks"
        private const val DB_USER_EMAIL = "com.fuzzythread.tyming.uid"
        private const val NOTIFICATION_SNOOZE_DATE = "com.fuzzythread.tyming.notification_snooze_date"
        private const val DISPLAY_DEADLINE_NOTIFICATIONS = "com.fuzzythread.tyming.display_deadline_notifications"
        private const val TUTORIAL_DATA_CLEARED = "com.fuzzythread.tyming.tutorial_data_cleared"

        fun setIsTutorialDataCleared(isTutorialCleared: Boolean, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putBoolean(TUTORIAL_DATA_CLEARED, isTutorialCleared)
            editor.apply()
        }

        fun getIsTutorialDataCleared(context: Context): Boolean {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getBoolean(TUTORIAL_DATA_CLEARED, false)
        }

        fun setIsAppInitialized(isAppInitialized: Boolean, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putBoolean(APP_INITIALIZED, isAppInitialized)
            editor.apply()
        }

        fun getIsAppInitialized(context: Context): Boolean {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getBoolean(APP_INITIALIZED, false)
        }

        fun setShowDeadlineNotifications(showDeadlineNotifications: Boolean, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putBoolean(DISPLAY_DEADLINE_NOTIFICATIONS, showDeadlineNotifications)
            editor.apply()
        }

        fun getShowDeadlineNotifications(context: Context): Boolean {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getBoolean(DISPLAY_DEADLINE_NOTIFICATIONS, false)
        }

        fun setNotificationSnoozeDate(date: Date, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putLong(NOTIFICATION_SNOOZE_DATE, date.time)
            editor.apply()
        }

        fun getNotificationSnoozeDate(context: Context): Date? {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val lastSyncTime = preferences.getLong(NOTIFICATION_SNOOZE_DATE, -1)
            return if (lastSyncTime > 0){
                Date(lastSyncTime)
            }else {
                null
            }
        }

        fun setUserEmail(email: String, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString(DB_USER_EMAIL, email)
            editor.apply()
        }

        fun getUserEmail(context: Context): String? {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getString(DB_USER_EMAIL, null)
        }

        fun setAutoSync(isAuthSyncEnabled: Boolean,context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putBoolean(ENABLE_AUTO_SYNC, isAuthSyncEnabled)
            editor.apply()
        }

        fun getIsAutoSyncEnabled(context: Context): Boolean {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getBoolean(ENABLE_AUTO_SYNC, false)
        }

        fun setLastSyncDate(date: Date, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putLong(LAST_SYNC_DATE, date.time)
            editor.apply()
        }

        fun getLastSyncDate(context: Context): Date? {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val lastSyncTime = preferences.getLong(LAST_SYNC_DATE, -1)
            return if (lastSyncTime > 0){
                Date(lastSyncTime)
            }else {
                null
            }
        }

        fun setTimeFrame(timeFrame: TimeFrame,context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString(TIME_PERIOD, timeFrame.name)
            editor.apply()
        }

        fun getTimeFrame(context: Context): TimeFrame {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return TimeFrame.valueOf(preferences.getString(TIME_PERIOD, "WEEK")!!)
        }

        fun setSortByJournal(sortBy: SortBy, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString(SORT_BY_JOURNAL, sortBy.name)
            editor.apply()
        }

        fun getSortByJournal(context: Context): SortBy {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return SortBy.valueOf(preferences.getString(SORT_BY_JOURNAL, "DESC")!!)
        }

        fun setSortByProjects(sortBy: SortBy, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString(SORT_BY_PROJECTS, sortBy.name)
            editor.apply()
        }

        fun getSortByProjects(context: Context): SortBy {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return SortBy.valueOf(preferences.getString(SORT_BY_PROJECTS, "DESC")!!)
        }

        fun setDisplayMode(displayMode: DisplayMode, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString(DISPLAY_MODE, displayMode.name)
            editor.apply()
        }

        fun getDisplayMode(context: Context): DisplayMode {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return DisplayMode.valueOf(preferences.getString(DISPLAY_MODE, "CLUSTER")!!)
        }

        fun setDisplayCompletedTasksVisible(displayCompletedTasks:  DisplayCompletedTasks, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString(DISPLAY_COMPLETED_TASKS, displayCompletedTasks.name)
            editor.apply()
        }

        fun getDisplayCompletedTasksVisible(context: Context): DisplayCompletedTasks {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return DisplayCompletedTasks.valueOf(preferences.getString(DISPLAY_COMPLETED_TASKS, "VISIBLE")!!)
        }

        // set timer length will be applied for future timers
        fun getIdleTime(context: Context): Long {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getLong(TIME_ELAPSED_ID, 0)
        }

        fun setIdleTime(seconds: Long, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putLong(TIME_ELAPSED_ID, seconds)
            editor.apply()
        }

        fun setDataGenerated(generateData: Boolean, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putBoolean(GENERATE_DATA, generateData)
            editor.apply()
        }

        fun getDataGenerated(context: Context): Boolean {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getBoolean(GENERATE_DATA, false)
        }

        fun setGoalTrackingEnabled(isTrackingEnabled: Boolean, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putBoolean(GOAL_TRACKING, isTrackingEnabled)
            editor.apply()
        }

        fun getGoalTrackingEnabled(context: Context): Boolean {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getBoolean(GOAL_TRACKING, false)
        }

        fun setIsTutorialCompleted(isTutorialCompleted: Boolean, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putBoolean(TUTORIAL_COMPLETED, isTutorialCompleted)
            editor.apply()
        }

        fun getIsTutorialCompleted(context: Context): Boolean {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getBoolean(TUTORIAL_COMPLETED, false)
        }

        fun setMultipleTimersEnabled(isMultipleTimersEnabled: Boolean, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putBoolean(TIMERS_MULTIPLE, isMultipleTimersEnabled)
            editor.apply()
        }

        fun getMultipleTimersEnabled(context: Context): Boolean {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getBoolean(TIMERS_MULTIPLE, false)
        }

        fun setGoalTrackingTime(minutes: Int, hours: Int, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putInt(GOAL_TRACKING_TIME_MINUTES, minutes)
            editor.putInt(GOAL_TRACKING_TIME_HOURS, hours)
            editor.apply()
        }

        fun getGoalTrackingTime(context: Context): GoalTime {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val minutes = preferences.getInt(GOAL_TRACKING_TIME_MINUTES, 0)
            val hours = preferences.getInt(GOAL_TRACKING_TIME_HOURS, 0)
            return GoalTime(hours, minutes)
        }

        fun setUserName(username: String, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString(USERNAME, username)
            editor.apply()
        }

        fun getUserName(context: Context): String {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getString(USERNAME, "Username") ?: "Username"
        }

        fun setTimers(timersJSON: String, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString(TIMERS, timersJSON)
            editor.apply()
        }

        fun setTutorials(tutorialsJSON: String, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString(TUTORIALS, tutorialsJSON)
            editor.apply()
        }

        fun getTutorials(context: Context): String {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getString(TUTORIALS, "{}") ?: "{}"
        }

        fun getTimers(context: Context): String {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getString(TIMERS, "{}") ?: "{}"
        }

        fun setCurrentTask(taskId: Long, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putLong(TASK_ID, taskId)
            editor.apply()
        }

        fun getCurrentTask(context: Context): Long {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getLong(TASK_ID, 0)
        }
    }
}

data class GoalTime(var hours: Int, var minutes: Int)
