package com.fuzzythread.tyming.global.utils.ui

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.SpotlightTargetBinding
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import com.fuzzythread.tyming.global.utils.other.TUTORIAL
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.takusemba.spotlight.OnSpotlightListener
import com.takusemba.spotlight.Spotlight
import com.takusemba.spotlight.Target
import com.takusemba.spotlight.shape.Circle

fun Fragment.createTarget(anchorView: View, shapeRadius: Int, title: String, description: String): Target {
    val binding = SpotlightTargetBinding.inflate(layoutInflater)
    binding.title.text = title
    binding.customText.text = description

    return Target.Builder()
            .setAnchor(anchorView)
            .setShape(Circle(convertDpToPx(requireContext(), shapeRadius).toFloat()))
            .setOverlay(binding.root)
            .build()
}

fun Activity.createTarget(anchorView: View, shapeRadius: Int, title: String, description: String): Target {
    val binding = SpotlightTargetBinding.inflate(layoutInflater)
    binding.title.text = title
    binding.customText.text = description

    return Target.Builder()
            .setAnchor(anchorView)
            .setShape(Circle(convertDpToPx(this, shapeRadius).toFloat()))
            .setOverlay(binding.root)
            .build()
}

fun Fragment.createTarget(x: Float, y: Float, shapeRadius: Int, title: String, description: String): Target {
    val binding = SpotlightTargetBinding.inflate(layoutInflater)
    binding.title.text = title
    binding.customText.text = description

    return Target.Builder()
            .setAnchor(x, y)
            .setShape(Circle(convertDpToPx(requireContext(), shapeRadius).toFloat()))
            .setOverlay(binding.root)
            .build()
}

fun Activity.createTarget(x: Float, y: Float, shapeRadius: Int, title: String, description: String): Target {
    val binding = SpotlightTargetBinding.inflate(layoutInflater)
    binding.title.text = title
    binding.customText.text = description

    return Target.Builder()
            .setAnchor(x, y)
            .setShape(Circle(convertDpToPx(this, shapeRadius).toFloat()))
            .setOverlay(binding.root)
            .build()
}

fun Fragment.createSpotlight(activity: FragmentActivity, targets: ArrayList<Target>, actionEnded: () -> Unit): Spotlight {
    return Spotlight.Builder(activity)
            .setTargets(targets)
            .setBackgroundColorRes(R.color.spotlightBackground)
            .setDuration(1000L)
            .setAnimation(DecelerateInterpolator(2f))
            .setOnSpotlightListener(object : OnSpotlightListener {
                override fun onStarted() {

                }

                override fun onEnded() {
                    actionEnded.invoke()
                }
            })
            .build()
}

fun Activity.createSpotlight(targets: ArrayList<Target>, actionEnded: () -> Unit): Spotlight {
    return Spotlight.Builder(this)
            .setTargets(targets)
            .setBackgroundColorRes(R.color.spotlightBackground)
            .setDuration(1000L)
            .setAnimation(DecelerateInterpolator(2f))
            .setOnSpotlightListener(object : OnSpotlightListener {
                override fun onStarted() {

                }

                override fun onEnded() {
                    actionEnded.invoke()
                }
            })
            .build()
}

fun addSpotLightListeners(spotlight: Spotlight, targets: ArrayList<Target>) {
    val nextTarget = View.OnClickListener { spotlight.next() }
    val closeSpotlight = View.OnClickListener { spotlight.finish() }
    // add spotlight to targets:
    targets.forEach {
        it.overlay?.apply {
            this.setOnClickListener(nextTarget)

            val binding = SpotlightTargetBinding.bind(this)
            binding.nextBtn.setOnClickListener(nextTarget)
            binding.close.setOnClickListener(closeSpotlight)
        }
    }
}


private fun Context.createTutorials() {
    val tutorialsMap = HashMap<String, Boolean>()
    tutorialsMap[TUTORIAL.PROJECTS] = false
    tutorialsMap[TUTORIAL.DASHBOARD] = false
    tutorialsMap[TUTORIAL.JOURNAL] = false
    tutorialsMap[TUTORIAL.STATISTICS] = false

    PrefUtil.setTutorials(
            tutorialsJSON = Gson().toJson(tutorialsMap),
            context = this
    )
}

private fun Context.checkIfTutorialsCreated() {
    val tutorialsJSON = PrefUtil.getTutorials(this)
    val gsonBuilder = GsonBuilder()
    val type = object : TypeToken<HashMap<String, Boolean>>() {}.type
    val tutorialsMap: HashMap<String, Boolean> = gsonBuilder
            .create()
            .fromJson(tutorialsJSON, type)

    if (tutorialsMap.size == 0) {
        createTutorials()
    }
}

fun Context.isTutorialCompleted(tutorial: String): Boolean {
    checkIfTutorialsCreated()

    val tutorialsJSON = PrefUtil.getTutorials(this)
    val gsonBuilder = GsonBuilder()
    val type = object : TypeToken<HashMap<String, Boolean>>() {}.type
    val tutorialsMap: HashMap<String, Boolean> = gsonBuilder
            .create()
            .fromJson(tutorialsJSON, type)

    for ((tutorialName, isCompleted) in tutorialsMap) {
        if (tutorialName == tutorial){
            return isCompleted
        }
    }
    return false
}

fun Context.markAllTutorialsCompleted() {
    checkIfTutorialsCreated()

    val tutorialsJSON = PrefUtil.getTutorials(this)
    val gsonBuilder = GsonBuilder()
    val type = object : TypeToken<HashMap<String, Boolean>>() {}.type
    val tutorialsMap: HashMap<String, Boolean> = gsonBuilder
            .create()
            .fromJson(tutorialsJSON, type)

    tutorialsMap.replaceAll { t, u -> true }
    PrefUtil.setTutorials(
            tutorialsJSON = Gson().toJson(tutorialsMap),
            context = this
    )
}

fun Context.markTutorialCompleted(tutorial: String) {
    checkIfTutorialsCreated()

    val tutorialsJSON = PrefUtil.getTutorials(this)
    val gsonBuilder = GsonBuilder()
    val type = object : TypeToken<HashMap<String, Boolean>>() {}.type
    val tutorialsMap: HashMap<String, Boolean> = gsonBuilder
            .create()
            .fromJson(tutorialsJSON, type)

    tutorialsMap[tutorial] = true
    PrefUtil.setTutorials(
            tutorialsJSON = Gson().toJson(tutorialsMap),
            context = this
    )
}


