package com.fuzzythread.tyming.screens.archive

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.ActivityArchiveBinding
import com.fuzzythread.tyming.global.TymingActivity
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.HomeFragmentRepository
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter.NavigationAdapter
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter.Refreshable
import com.fuzzythread.tyming.screens.home.model.NavigationItem
import javax.inject.Inject

class ArchiveActivity : TymingActivity(), Refreshable {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val repository: HomeFragmentRepository by lazy {
        ViewModelProvider(this, viewModelFactory).get(HomeFragmentRepository::class.java)
    }

    private var navigationItems: MutableList<NavigationItem<*>> = mutableListOf()
    private var navigationAdapter: NavigationAdapter? = null
    private lateinit var binding: ActivityArchiveBinding

    // region Android hooks
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArchiveBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTopBarPadding(binding.topBar)
        initialize()
    }

    override fun onResume() {
        super.onResume()
        refreshSelf()
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    // endregion

    // region UI
    fun initialize() {
        initializeUI()
    }

    fun initializeUI() {
        binding.backBtn.setOnClickListener {
            finish()
        }
        showEmptyScene()
        initializeRecyclerViews()
    }

    private fun initializeRecyclerViews() {
        navigationAdapter = NavigationAdapter(this, this, repository, isArchive = true)
        binding.areasProjects.adapter = navigationAdapter
        binding.areasProjects.layoutManager = LinearLayoutManager(this)
        refreshSelf()
    }

    private fun showEmptyScene() {
        if (navigationItems.isEmpty()) {
            binding.emptyStage?.visibility = View.VISIBLE
        } else {
            binding.emptyStage?.visibility = View.GONE
        }
    }
    // endregion

    // region Data
    fun refresh(){
        navigationItems = repository.getNavigationData(isArchived = true, sortBy = PrefUtil.getSortByProjects(this))
        navigationAdapter?.refreshAdapterWithData(navigationItems)
        showEmptyScene()
    }

    override fun refreshSelf(refreshTimers: Boolean) {
        refresh()
    }

    override fun refreshAll() {
        refresh()
    }
    // endregion
}
