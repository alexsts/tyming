package com.fuzzythread.tyming.screens.home.model

import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.AreaViewModel
import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TaskViewModel
import java.util.*

class NavigationItem<T>(
         var entity: T
) : IAdapterComparable<NavigationItem<*>> {

    var id: Long = 0
    var title: String = ""
    var childNumber: Int = 0
    var progress: Int = 0
    var timeTracked: String = ""
    var lastWorked: String = ""
    var color: String = ""
    var trackProgress: Boolean = false
    var isCollapsed: Boolean = false
    var deadlineDate: Date? = null
    var completed: Boolean = false
    var displayOrder: Long = 0

    private var itemType: Int = 0

    init {
        val itemType = when (entity) {
            is AreaViewModel -> AREA
            is ProjectViewModel -> PROJECT
            is TaskViewModel -> TASK
            else -> -1
        }
        this.itemType = itemType
    }

    fun getItemType(): Int {
        return itemType
    }

    fun getItemEntity(): T {
        return entity
    }

    fun isTask(): Boolean{
        return itemType == TASK
    }

    fun isProject(): Boolean{
        return itemType == PROJECT
    }

    companion object {
        val AREA = 1
        val PROJECT = 2
        val TASK = 3
    }

    override fun areItemsTheSame(itemToCompare: NavigationItem<*>): Boolean {
        return itemToCompare.id == id &&
                itemToCompare.itemType == itemType
    }

    override fun areContentsTheSame(itemToCompare: NavigationItem<*>): Boolean {
        return itemToCompare.id == id &&
                itemToCompare.title == title &&
                itemToCompare.color == color &&
                itemToCompare.lastWorked == lastWorked &&
                itemToCompare.timeTracked == timeTracked &&
                itemToCompare.childNumber == childNumber &&
                itemToCompare.progress == progress &&
                itemToCompare.itemType == itemType &&
                itemToCompare.trackProgress == trackProgress &&
                itemToCompare.isCollapsed == isCollapsed &&
                itemToCompare.deadlineDate == deadlineDate &&
                itemToCompare.completed == completed &&
                itemToCompare.displayOrder == displayOrder
    }
}
