package com.fuzzythread.tyming.screens.home.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import androidx.viewpager.widget.ViewPager.SCROLL_STATE_IDLE
import com.fuzzythread.tyming.BuildConfig
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.ActivityHomeBinding
import com.fuzzythread.tyming.global.TymingActivity
import com.fuzzythread.tyming.global.ui.elements.TaggableFragment
import com.fuzzythread.tyming.global.utils.other.Constants
import com.fuzzythread.tyming.screens.home.ui.dialog.NavigationCombinedDialog
import com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.ui.DashboardFragment
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.NavigationFragment
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.AreaDialog
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.ProjectDialog
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.TaskDialog
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.TimeEntryDialog
import com.google.android.gms.ads.*
import org.joda.time.DateTime
import org.joda.time.Minutes
import timber.log.Timber


class HomeActivity : TymingActivity() {

    private val fragments: MutableList<TaggableFragment> = mutableListOf()
    private var isFirstLaunch: Boolean = true
    private var homeFragment: NavigationFragment? = null
    private var profileFragment: DashboardFragment? = null
    private var adLastOpened: DateTime? = null
    private lateinit var binding: ActivityHomeBinding

    // region Android hooks
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initialize(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        if (!isFirstLaunch) {
            setupFreeAccountAds(isActivityResumed = true)
            refresh()
        }
        isFirstLaunch = false
    }
    // endregion

    // region Data

    fun refresh() {
        updateFragments()
    }

    private fun updateFragments() {
        fragments.forEach {
            it.refreshFragment()
        }
    }

    private fun setupFreeAccountAds(isActivityResumed: Boolean = false) {
        if (!BuildConfig.IS_PREMIUM) {
            if (isActivityResumed) {
                // not opened or ads clicked more than 30 min ago:
                if (adLastOpened == null || adLastOpened != null && Minutes.minutesBetween(adLastOpened, DateTime()).minutes >= ADS_STANDBY_DURATION) {
                    initializeAds()
                }
            } else {
                initializeAds()
            }
        }
    }

    private fun initializeAds() {
        Timber.tag(Constants.APP_TAG).d("Initializing Ads")

        MobileAds.initialize(this)
        val adView = AdView(this)
        adView.setAdSize(AdSize(360, 60))

        if (BuildConfig.BUILD_TYPE == "debug") {
            adView.adUnitId = "ca-app-pub-3940256099942544/6300978111"
        } else {
            adView.adUnitId = "ca-app-pub-7758677713625242/2633048371"
        }

        binding.adPlaceholder.removeAllViews()
        binding.adPlaceholder.addView(adView)

        // Load ads:
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
        adView.adListener = object : AdListener() {
            override fun onAdClicked() {
                binding.adContainer.visibility = View.GONE
                Timber.tag(Constants.APP_TAG).d("Ad clicked")
                super.onAdClicked()
            }

            override fun onAdOpened() {
                binding.adContainer.visibility = View.GONE
                adLastOpened = DateTime()
                Timber.tag(Constants.APP_TAG).d("Ad opened")
                super.onAdOpened()
            }

            override fun onAdClosed() {
                super.onAdClosed()
                Timber.tag(Constants.APP_TAG).d("Ad closed")
            }

            override fun onAdLoaded() {
                Timber.tag(Constants.APP_TAG).d("Ad loaded")
                binding.adContainer.visibility = View.VISIBLE
                super.onAdLoaded()
            }
        }
    }

    // endregion

    // region UI
    private fun initializeNavigationBar() {
        val fab = binding.fab

        fab.setOnClickListener {
            fab.hide()
            NavigationCombinedDialog()
                    .setAddTaskCallBack {
                        TaskDialog()
                                .getInstance()
                                .setDismissCallBack {
                                    refresh()
                                    fab.show()
                                }
                                .show(supportFragmentManager, "Add Task")
                    }
                    .setAddProjectCallBack {
                        ProjectDialog()
                                .getInstance()
                                .setDismissCallBack {
                                    refresh()
                                    fab.show()
                                }
                                .show(supportFragmentManager, "Add Project")
                    }
                    .setAddAreaCallBack {
                        AreaDialog()
                                .getInstance()
                                .setDismissCallBack {
                                    refresh()
                                    fab.show()
                                }
                                .show(supportFragmentManager, "Add Area")
                    }
                    .setAddTimeRecordCallBack {
                        TimeEntryDialog.getInstance()
                                .setDismissCallBack {
                                    refresh()
                                    fab.show()
                                }
                                .show(supportFragmentManager, "Add TimeEntry")
                    }
                    .setDismissCallBack {
                        fab.show()
                    }
                    .show(supportFragmentManager, "Combined Dialog")
        }

        if (homeFragment != null && profileFragment != null) {
            fragments.add(homeFragment!!)
            fragments.add(profileFragment!!)

            val adapter = ScreenSlidePagerAdapter(supportFragmentManager)
            var positionSelected  = 0

            val mainNavigation = binding.mainNavigation
            mainNavigation.adapter = adapter
            mainNavigation.offscreenPageLimit = 3
            mainNavigation.addOnPageChangeListener(object : OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {
                    homeFragment?.updateTimers = state == SCROLL_STATE_IDLE
                    if (state == SCROLL_STATE_IDLE){
                        fragments[positionSelected].showTutorial()
                    }
                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    if (!isFirstLaunch) {
                        positionSelected = position
                        homeFragment?.updateTimers = position == 0
                    }
                }
            })

            binding.dotsIndicator.setViewPager(mainNavigation)
            homeFragment?.updateTimers = true
        }
    }

    fun openProjects() {
        binding.mainNavigation.setCurrentItem(0, true)
    }

    fun openDashboard() {
        binding.mainNavigation.setCurrentItem(1, true)
    }

    private fun initializeFragments(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            homeFragment = NavigationFragment.newInstance()
            profileFragment = DashboardFragment.newInstance()
        } else {
            homeFragment = if (savedInstanceState.containsKey(NavigationFragment.TAG)) {
                supportFragmentManager.getFragment(savedInstanceState, NavigationFragment.TAG) as NavigationFragment
            } else {
                NavigationFragment.newInstance()
            }

            profileFragment = if (savedInstanceState.containsKey(DashboardFragment.TAG)) {
                supportFragmentManager.getFragment(savedInstanceState, DashboardFragment.TAG) as DashboardFragment
            } else {
                DashboardFragment.newInstance()
            }
        }
    }

    private fun initialize(savedInstanceState: Bundle?) {
        setupFreeAccountAds()
        // UI:
        initializeFragments(savedInstanceState)
        initializeNavigationBar()
    }

    // endregion

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int = 2
        override fun getItem(position: Int): Fragment = fragments[position]
    }

    companion object {
        const val ADS_STANDBY_DURATION = 10
        fun intent(context: Context): Intent {
            return Intent(context, HomeActivity::class.java)
        }
    }
}

