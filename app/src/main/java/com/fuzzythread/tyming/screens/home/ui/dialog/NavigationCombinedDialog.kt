package com.fuzzythread.tyming.screens.home.ui.dialog

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.*
import com.fuzzythread.tyming.R
import android.view.Gravity
import com.fuzzythread.tyming.databinding.DialogNavigationCombinedBinding
import com.fuzzythread.tyming.global.ui.elements.PieProgressDrawable
import com.fuzzythread.tyming.global.utils.ui.convertDpToPx

class NavigationCombinedDialog : DialogFragment() {

    private var callbackDismiss: (() -> Unit)? = null
    private var callbackAddTask: (() -> Unit)? = null
    private var callbackAddArea: (() -> Unit)? = null
    private var callbackAddProject: (() -> Unit)? = null
    private var callbackAddTimeRecord: (() -> Unit)? = null

    private var clickedAdd: Boolean = false

    fun setDismissCallBack(func: () -> Unit): NavigationCombinedDialog {
        callbackDismiss = func
        return this
    }

    fun setAddTaskCallBack(func: () -> Unit): NavigationCombinedDialog {
        callbackAddTask = func
        return this
    }

    fun setAddProjectCallBack(func: () -> Unit): NavigationCombinedDialog {
        callbackAddProject = func
        return this
    }

    fun setAddAreaCallBack(func: () -> Unit): NavigationCombinedDialog {
        callbackAddArea = func
        return this
    }

    fun setAddTimeRecordCallBack(func: () -> Unit): NavigationCombinedDialog {
        callbackAddTimeRecord = func
        return this
    }

    private fun removeDismissCallBacks() {
        callbackDismiss = null
        callbackAddTask = null
        callbackAddArea = null
        callbackAddProject = null
        callbackAddTimeRecord = null
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val binding = DialogNavigationCombinedBinding.inflate(inflater, container, false)

        val progressDrawable = PieProgressDrawable(resources.displayMetrics)
        binding.projectImg.setImageDrawable(progressDrawable)

        progressDrawable.apply {
            level = 65
            setColor(Color.parseColor("#1e88e5"))
            completable = true
        }

        binding.projectImg.invalidate()

        binding.addBtn.setOnClickListener {
            clickedAdd = true
            callbackAddTask?.invoke()
            dialog?.dismiss()
        }

        binding.editBtn.setOnClickListener {
            clickedAdd = true
            callbackAddProject?.invoke()
            dialog?.dismiss()
        }

        binding.deleteBtn.setOnClickListener {
            clickedAdd = true
            callbackAddArea?.invoke()
            dialog?.dismiss()
        }

        binding.addBtnTime.setOnClickListener {
            clickedAdd = true
            callbackAddTimeRecord?.invoke()
            dialog?.dismiss()
        }

        setDialogPosition()
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        val params = window!!.attributes
        params.dimAmount = 0.6f
        window.attributes = params
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(width, height)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.setCanceledOnTouchOutside(true)
    }

    private fun setDialogPosition() {
        val window = dialog?.window
        window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM)
        val params = window.attributes
        params.y = convertDpToPx(this.requireContext(), 8)
        params.windowAnimations = R.style.DialogSlideBottom
        window.attributes = params
    }

    override fun onDismiss(dialog: DialogInterface) {
        if (!clickedAdd) {
            callbackDismiss?.invoke()
        }
        removeDismissCallBacks()
        super.onDismiss(dialog)
    }
}
