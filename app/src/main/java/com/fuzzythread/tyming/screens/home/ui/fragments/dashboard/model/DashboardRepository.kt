package com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.model

import android.content.Context
import com.fuzzythread.tyming.global.db.ChartsRepository
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import com.fuzzythread.tyming.global.utils.other.TimeFrame
import com.fuzzythread.tyming.global.utils.other.WEEK_LENGTH
import com.fuzzythread.tyming.global.utils.time.formatTimeToString
import com.fuzzythread.tyming.global.utils.time.isCurrentWeek
import com.fuzzythread.tyming.global.utils.time.sumTimeEntriesPeriods
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import org.joda.time.DateTime
import kotlin.collections.ArrayList

class DashboardRepository(
        private val areaDao: AreaDao,
        private val projectDao: ProjectDao,
        taskDao: TaskDao,
        private val timeEntryDao: TimeEntryDao,
        private val context: Context
) : ChartsRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {

    fun getArchiveOverviewData(): ArchiveOverviewData {
        val archivedProjects: MutableList<ProjectEntity> = mutableListOf()
        val archivedTimeEntries: MutableList<TimeEntryEntity> = mutableListOf()

        archivedProjects.addAll(projectDao.getSingularProjects(isArchived = true))
        areaDao.getAllSorted().forEach {
            archivedProjects.addAll(areaDao.getAreaProjectsSorted(it.id, isArchived = true))
        }

        archivedProjects.forEach { projectEntity ->
            projectEntity.tasks.forEach {
                archivedTimeEntries.addAll(it.timeEntries)
            }
        }
        return ArchiveOverviewData(archivedProjectCount = archivedProjects.size, archivedTime = formatTimeToString(sumTimeEntriesPeriods(archivedTimeEntries)))
    }

    fun getOverallTime(): String {
        return formatTimeToString(sumTimeEntriesPeriods(timeEntryDao.getAll()))
    }

    fun getStatsPreviewData(date: DateTime): MutableList<StatsWeek> {
        // 28 days
        val dayAmount = (WEEK_LENGTH * 4) // 4 weeks
        val firstDay = date.weekOfWeekyear().roundCeilingCopy().minusDays(dayAmount)
        val weeksBars = mutableListOf<StatsWeek>()
        val todayDate = DateTime()
        val weekChunks = (0 until dayAmount).toList().chunked(7)

        weekChunks.forEach { week ->
            val dayBars = ArrayList<IBarDataSet>()
            val days = mutableListOf<DateTime>()
            val daysNames = ArrayList<String>()
            var weekDayCounter = 0
            var overallWeekTime = 0f
            var workedDays = 0

            week.forEach { dayId ->
                val day = firstDay.plusDays(dayId)
                daysNames.add(dateTimeFormatterWeekDay.print(day)[0] + "\n" + day.dayOfMonth)
                days.add(day)
                // combine time entries by project:
                val projectTimes = getProjectsTimesSortedDesc(timeEntryDao.getTimeEntriesByDay(day))
                if (projectTimes.isNotEmpty()) {
                    dayBars.add(getDayBar(projectTimes, weekDayCounter))
                    overallWeekTime += projectTimes.map { it.second }.sum()
                    workedDays++
                } else {
                    dayBars.add(getEmptyDayBar(weekDayCounter))
                }
                weekDayCounter++
            }
            weeksBars.add(StatsWeek(weekBarData = getBarData(dayBars),
                    overallWeekTime = overallWeekTime,
                    averageHours = getAverageTime(workedDays, overallWeekTime),
                    daysNames = daysNames,
                    days = days,
                    previousWeekTime = getPreviousTimeFramePeriod(TimeFrame.WEEK, firstDay.plusDays(week[0]).minusDays(1)),
                    isCurrentWeek = days.first().isCurrentWeek(todayDate)))
        }
        return weeksBars
    }

    fun getUncompletedProjectsWithUpcomingDeadline(): MutableList<ProjectViewModel> {
        val uncompletedProjectsVms: MutableList<ProjectViewModel> = mutableListOf()
        val today = DateTime()

        projectDao.getProjectsWithDeadline(today).forEach { projectEntity ->
            createProjectVmFromEntity(projectEntity).also {
                if (!it.completed && it.tasks.size > 0) {
                    uncompletedProjectsVms.add(it)
                }
            }
        }

        return uncompletedProjectsVms
    }

    data class ArchiveOverviewData(val archivedProjectCount: Int, val archivedTime: String)
    data class StatsWeek(val weekBarData: BarData, val averageHours: Float, val daysNames: List<String>, val isCurrentWeek: Boolean, val days: MutableList<DateTime>, val overallWeekTime: Float, val previousWeekTime: Float)
}
