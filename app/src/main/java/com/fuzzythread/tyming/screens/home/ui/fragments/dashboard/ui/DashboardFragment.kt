package com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.ViewCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.fuzzythread.tyming.BuildConfig
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.FragmentDashboardBinding
import com.fuzzythread.tyming.databinding.SpotlightTargetBinding
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.global.ui.elements.TaggableFragment
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import com.fuzzythread.tyming.global.utils.other.TUTORIAL
import com.fuzzythread.tyming.global.utils.ui.*
import com.fuzzythread.tyming.screens.archive.ArchiveActivity
import com.fuzzythread.tyming.screens.home.ui.HomeActivity
import com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.model.DashboardRepository
import com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.model.DashboardRepository.ArchiveOverviewData
import com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.model.DashboardRepository.StatsWeek
import com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.ui.adapter.WeeksAdapter
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter.CustPageTransformer
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter.DeadlinesAdapter
import com.fuzzythread.tyming.screens.journal.ui.JournalActivity
import com.fuzzythread.tyming.screens.settings.SettingsActivity
import com.fuzzythread.tyming.screens.stats.ui.StatsActivity
import com.fuzzythread.tyming.screens.stats.ui.adapter.TasksDetailsAdapter
import com.takusemba.spotlight.Target
import kotlinx.coroutines.*
import org.joda.time.DateTime
import java.util.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

@Suppress("DEPRECATION")
class DashboardFragment : TaggableFragment(), CoroutineScope {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val repository: DashboardRepository by lazy {
        ViewModelProvider(this, viewModelFactory).get(DashboardRepository::class.java)
    }

    private var masterJob: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + masterJob

    private var statsDetailed: ConstraintLayout? = null
    private var statsPreviewData: MutableList<StatsWeek> = mutableListOf()
    private var dayTime: TextView? = null
    private var weekDay: TextView? = null
    private var monthLabel: TextView? = null
    private var hrsLabelTV: TextView? = null
    private var timeEntryCountTV: TextView? = null
    private var container: CoordinatorLayout? = null
    private var weeksRV: ViewPager2? = null
    private var weeksAdapter: WeeksAdapter? = null
    private var overallTime: String = ""
    private var todayTime: String = ""
    private var separator: View? = null
    private var archiveProjectLabel: TextView? = null
    private var archiveLabelTime: TextView? = null
    private var archiveLabelCount: TextView? = null
    private var archiveData: ArchiveOverviewData? = null
    private var nestedScrollView: NestedScrollView? = null
    private var tasksAdapter: TasksDetailsAdapter? = null
    private var deadlinesRV: ViewPager2? = null
    private var deadlinesAdapter: DeadlinesAdapter? = null
    private var deadlinesProjectsData: MutableList<ProjectViewModel> = mutableListOf()
    private var tasksRV: RecyclerView? = null
    private val today = DateTime()

    @SuppressLint("DefaultLocale")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentDashboardBinding.inflate(inflater, container, false)
        initializeUI(binding)
        updateUIWithValues()
        return binding.root
    }

    private var isCreated = false

    // region Android hooks
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCreated = true
    }

    override fun onResume() {
        super.onResume()
        resetDayPreview()
        refreshAsync()
    }

    override fun onDestroy() {
        masterJob.cancel()
        weeksAdapter?.unbindCallbacks()
        super.onDestroy()
    }

    override fun onPause() {
        masterJob.cancel()
        super.onPause()
    }
    // endregion

    // region Data
    private fun refreshAsync() {
        // reset previous job:
        masterJob.cancel()
        masterJob = Job()

        launch(this.coroutineContext) {
            val refresh = async {
                refreshData()
            }
            refresh.await()
            withContext(Dispatchers.Main) {
                updateUIWithValues()
            }
        }
    }

    private fun refreshData() {
        if (this@DashboardFragment::viewModelFactory.isInitialized) {
            archiveData = repository.getArchiveOverviewData()
            overallTime = repository.getOverallTime()
            statsPreviewData = repository.getStatsPreviewData(today)
            todayTime = repository.getTimeFormattedForDate(today)
            deadlinesProjectsData = repository.getUncompletedProjectsWithUpcomingDeadline()
        }
    }

    override fun refreshFragment() {
        refreshAsync()
    }
    // endregion

    // region UI
    private fun initializeUI(binding: FragmentDashboardBinding) {
        archiveLabelCount = binding.projectsCount
        archiveLabelTime = binding.archiveTime
        archiveProjectLabel = binding.statsLabel6
        monthLabel = binding.statsLabel5
        dayTime = binding.dayTime
        weekDay = binding.weekDay
        container = binding.mainContainer
        timeEntryCountTV = binding.journalTimeEntryCount
        hrsLabelTV = binding.hrsLabel
        weeksRV = binding.weeksRv
        tasksRV = binding.tasksStats
        separator = binding.separator
        deadlinesRV = binding.deadlinesRv
        statsDetailed = binding.statsDetailed

        deadlinesAdapter = DeadlinesAdapter(requireContext(), this)
        deadlinesRV?.run {
            orientation = ViewPager2.ORIENTATION_HORIZONTAL
            adapter = deadlinesAdapter
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 3
            (getChildAt(0) as RecyclerView).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
            setPageTransformer(CustPageTransformer(this@run.context))
        }

        binding.navigateProjects.setOnClickListener {
            (activity as HomeActivity?)?.openProjects()
        }

        binding.archive.setOnClickListener {
            with(it.context) {
                val intent = Intent(this, ArchiveActivity::class.java)
                startActivity(intent)
                activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }
        }

        binding.journal.setOnClickListener {
            with(it.context) {
                val intent = Intent(this, JournalActivity::class.java)
                startActivity(intent)
                activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }
        }

        binding.statsDetailed.setOnClickListener {
            with(it.context) {
                val intent = Intent(this, StatsActivity::class.java)
                startActivity(intent)
                activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }
        }

        binding.settings.setOnClickListener {
            with(it.context) {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }
        }

        binding.appVersion.text = "${getString(R.string.version)}: ${BuildConfig.VERSION_NAME}"

        val month: String = DateTime().toString("MMMM")
        monthLabel?.text = month
        initStatsViewPager()
        initScroll(binding)

        // Tasks Adapter:
        tasksAdapter = TasksDetailsAdapter(requireContext())
        tasksRV!!.adapter = tasksAdapter
    }

    private fun changeSearchMargins(dp: Int) {
        val context: Context = this.context ?: return
        val layoutParams = statsDetailed?.layoutParams as ConstraintLayout.LayoutParams
        layoutParams.setMargins(layoutParams.leftMargin, convertDpToPx(context, dp), layoutParams.rightMargin, layoutParams.bottomMargin)
        statsDetailed?.layoutParams = layoutParams
    }

    private fun initScroll(binding: FragmentDashboardBinding) {
        val scrollEdge = convertDpToPx(binding.root.context, 2)
        binding.mainContainer.setPadding(0, binding.root.context.getStatusBarHeight(), 0, 0)
        nestedScrollView = binding.nestedScrollProfile
        nestedScrollView?.isFocusableInTouchMode = true
        nestedScrollView?.descendantFocusability = ViewGroup.FOCUS_BEFORE_DESCENDANTS
        nestedScrollView?.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY <= scrollEdge) {
                binding.topBarSeparator.visibility = View.GONE
                ViewCompat.setElevation(binding.topBar, 0f)
            } else if (scrollY > scrollEdge) {
                binding.topBarSeparator.visibility = View.VISIBLE
                ViewCompat.setElevation(binding.topBar, 15f)
            }
        })
    }

    private fun updateUIWithValues() {
        archiveData?.apply {
            if (archivedTime == "00:00") {
                archiveLabelTime?.visibility = View.GONE
            } else {
                archiveLabelTime?.visibility = View.VISIBLE
                archiveLabelTime?.text = archivedTime
            }

            archiveLabelCount?.text = archivedProjectCount.toString()
            if (archivedProjectCount == 1) {
                archiveProjectLabel?.text = getString(R.string.project)
            } else {
                archiveProjectLabel?.text = getString(R.string._projects)
            }
        }
        timeEntryCountTV?.text = overallTime
        weeksAdapter?.refreshAdapterWithData(statsPreviewData)
        weeksRV?.setCurrentItem(statsPreviewData.size, true)
        dayTime?.text = todayTime
        handleDeadlineNotificationsVisibility()
    }

    private fun initStatsViewPager() {
        weeksAdapter = WeeksAdapter(this)
        weeksRV?.run {
            orientation = ViewPager2.ORIENTATION_HORIZONTAL
            adapter = weeksAdapter
            clipToPadding = false
            clipChildren = false

            val recyclerView = (getChildAt(0) as RecyclerView)
            recyclerView.overScrollMode = RecyclerView.OVER_SCROLL_NEVER

            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageScrollStateChanged(state: Int) {
                    recyclerView.findViewHolderForAdapterPosition(weeksRV?.currentItem
                            ?: 0)?.apply {
                        val weekViewHolder = this as WeeksAdapter.WeekViewHolder
                        weekViewHolder.removeHighlight()
                        resetDayPreview()
                    }

                    // updateTimers = (state == ViewPager2.SCROLL_STATE_IDLE)
                    super.onPageScrollStateChanged(state)
                }
            })
        }

        // selected day (tasks, day, time):
        weeksAdapter?.bindDaySelectedCallback { selectedDay ->
            if (selectedDay != null) {
                repository.getSelectedDayTime(selectedDay).apply {
                    dayTime?.text = this.trackedTime
                    weekDay?.text = this.formattedDate
                    if (this.tasks.isNotEmpty()) {
                        separator?.visibility = View.VISIBLE
                        tasksRV?.visibility = View.VISIBLE
                        tasksAdapter?.refresh(this.tasks)
                    } else {
                        separator?.visibility = View.GONE
                        tasksRV?.visibility = View.GONE
                    }
                }
            } else {
                resetDayPreview()
            }
        }
    }

    fun handleDeadlineNotificationsVisibility() {
        val context: Context = this.context ?: return

        if (PrefUtil.getShowDeadlineNotifications(context)
                && !isNotificationDisplayedToday()
                && deadlinesProjectsData.size > 0) {

            changeSearchMargins(12)
            deadlinesRV?.visibility = View.VISIBLE
            deadlinesAdapter?.refreshAdapterWithData(deadlinesProjectsData)
            PrefUtil.setNotificationSnoozeDate(Date(), context)
        } else {
            deadlinesRV?.visibility = View.GONE
            changeSearchMargins(2)
        }
    }

    private fun resetDayPreview() {
        dayTime?.text = todayTime
        weekDay?.text = getString(R.string.today_uppercase)
        separator?.visibility = View.GONE
        tasksRV?.visibility = View.GONE
    }

    private fun isNotificationDisplayedToday(): Boolean {
        val context: Context = this.context ?: return false
        val notificationSnoozeDate = PrefUtil.getNotificationSnoozeDate(context)
        return notificationSnoozeDate != null && DateTime(notificationSnoozeDate).isAfter(DateTime().withTimeAtStartOfDay())
    }
    // endregion

    // region Tutorial
    override fun showTutorial() {
        if (context?.isTutorialCompleted(TUTORIAL.DASHBOARD) != true) {
            startTutorial()
        }
    }

    private fun startTutorial() {
        val activity = this.activity ?: return
        val targets = createTutorialTargets()
        val spotlight = createSpotlight(activity, targets) {
            context?.markTutorialCompleted(TUTORIAL.DASHBOARD)
        }
        spotlight.start()
        addSpotLightListeners(spotlight, targets)
    }

    private fun createTutorialTargets(): ArrayList<Target> {
        val targets = arrayListOf<Target>()

        container?.apply {
            val target1 = createTarget(
                    anchorView = findViewById<View>(R.id.stats_icon),
                    shapeRadius = 40,
                    title = context.getString(R.string.statistics),
                    description = context.getString(R.string.open_stats_to_show_charts)
            )
            targets.add(target1)

            val target2 = createTarget(
                    anchorView = findViewById<View>(R.id.journal_icon),
                    shapeRadius = 40,
                    title = context.getString(R.string.journal),
                    description = context.getString(R.string.open_journal_to_see)
            )
            targets.add(target2)

            val target3 = createTarget(
                    anchorView = findViewById<View>(R.id.icon),
                    shapeRadius = 40,
                    title = context.getString(R.string.archive),
                    description = context.getString(R.string.access_archived_projects)
            )

            target3.overlay?.apply {
                val binding = SpotlightTargetBinding.bind(this)
                binding.nextBtn.text = context.getString(R.string.finish)
                binding.nextBtn.setMarginBottom(320)
            }

            targets.add(target3)
        }

        return targets
    }
    // endregion

    override fun getTAG(): String {
        return TAG
    }

    companion object {
        const val TAG = "PROFILE_FRAGMENT"
        fun newInstance(): DashboardFragment {
            val fragment = DashboardFragment()
            val args = Bundle()
            args.putString(TAG, "TAG")
            fragment.arguments = args
            return fragment
        }
    }
}
