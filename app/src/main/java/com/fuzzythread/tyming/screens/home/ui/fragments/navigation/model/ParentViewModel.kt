package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model

import androidx.databinding.Bindable
import com.fuzzythread.tyming.BR
import com.fuzzythread.tyming.global.ObservableViewModel
import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable
import com.fuzzythread.tyming.global.utils.other.ParentType

class ParentViewModel : ObservableViewModel(), IAdapterComparable<ParentViewModel> {
    var id: Long = 0

    @get:Bindable
    var title: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }

    @get:Bindable
    var color: String = "#1e88e5"
        get() {
            if (field.isEmpty()) {
                return "#1e88e5"
            }
            return field
        }
        set(value) {
            field = value
            notifyPropertyChanged(BR.color)
        }

    @get:Bindable
    var type: ParentType = ParentType.NONE
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentType)
        }

    @get:Bindable
    var selected: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.selected)
        }

    override fun areItemsTheSame(itemToCompare: ParentViewModel): Boolean {
        return itemToCompare.id == id && itemToCompare.type == type
    }

    override fun areContentsTheSame(itemToCompare: ParentViewModel): Boolean {
        return itemToCompare.id == id &&
                itemToCompare.type == type &&
                itemToCompare.title == title &&
                itemToCompare.color == color &&
                itemToCompare.selected == selected
    }
}
