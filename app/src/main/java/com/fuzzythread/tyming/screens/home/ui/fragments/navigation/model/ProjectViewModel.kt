package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model

import android.content.Context
import androidx.databinding.Bindable
import com.fuzzythread.tyming.BR
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.ObservableViewModel
import com.fuzzythread.tyming.global.db.entities.AreaEntity
import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.exception.TitleNotSpecifiedException
import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable
import com.fuzzythread.tyming.global.utils.extensions.endOfDay
import com.fuzzythread.tyming.global.utils.extensions.startOfDay
import com.fuzzythread.tyming.global.utils.other.EntityType
import com.fuzzythread.tyming.global.utils.time.clearTime
import com.fuzzythread.tyming.global.utils.time.formatTimeToString
import org.joda.time.DateTime
import org.joda.time.Days
import org.joda.time.Duration
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import java.util.*

class ProjectViewModel(val context: Context?) :
        ObservableViewModel(),
        IAdapterComparable<ProjectViewModel> {

    // region Database Model Attributes

    var id: Long = 0

    @get:Bindable
    var title: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }

    @get:Bindable
    var notes: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.notes)
        }

    @get:Bindable
    var collapsed: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.collapsed)
            notifyPropertyChanged(BR.lastWorkedFormatted)
        }

    @get:Bindable
    var trackProgress: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.trackProgress)
        }

    @get:Bindable
    var displayOrder: Long = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.displayOrder)
        }

    var createdDate: Date? = null
    var archivedDate: Date? = null
    var deadlineDate: Date? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.hasDeadline)
            notifyPropertyChanged(BR.deadlineFormatted)
            notifyPropertyChanged(BR.deadlineExpired)
            notifyPropertyChanged(BR.trackProgress)
        }

    var timerElapsedRunning: Long = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.lastWorkedFormatted)
            notifyPropertyChanged(BR.timerRunning)
        }

    @get:Bindable
    var color: String = "#1e88e5"
        get() {
            if (field.isEmpty()) {
                return "#1e88e5"
            }
            return field
        }
        set(value) {
            field = value
            notifyPropertyChanged(BR.color)
        }

    var area: AreaEntity? = null
        set(value) {
            field = value
            if (value != null) {
                parentName = value.title
                parentDrawable = R.drawable.ic_area
                parentColor = value.color
            } else {
                parentName = context?.getString(R.string.root) ?: "Root"
                parentDrawable = R.drawable.ic_root
                parentColor = "#1e88e5"
            }
        }

    var tasks: MutableList<TaskEntity> = mutableListOf()
        set(value) {
            field = value
            val allTasks = value.size
            tasksCompletedNumber = value.filter { it.completedDate != null }.size
            tasksNumber = value.filter { it.completedDate == null }.size

            // get tasks that are completed
            progress = if (allTasks > 0) {
                ((tasksCompletedNumber.toDouble() / allTasks.toDouble()) * 100).toInt()
            } else {
                0
            }
        }
    // endregion

    // region Dates

    private var dateFormatter = DateTimeFormat.forPattern("d MMM yyyy")
    private val currentDate = DateTime().withTimeAtStartOfDay()

    @get:Bindable
    var deadlineFormatted: String = ""
        get() {
            var value = ""
            if (deadlineDate != null) {
                val deadlineDate = DateTime(deadlineDate)
                val duration = Duration(currentDate, deadlineDate)
                value = when (duration.toStandardHours().hours) {
                    in 0..23 -> context?.getString(R.string.today) ?: "Today"
                    else -> dateFormatter.print(deadlineDate)
                }
            }
            return value
        }

    val completedDate: Date?
        get() {
            var tempCompletedDate: Date? = null
            if (tasksNumber == 0 && tasksCompletedNumber > 0) {
                val lastTask = tasks
                        .filter { it.completedDate != null }
                        .sortedByDescending { it.completedDate }.firstOrNull()

                lastTask?.apply {
                    tempCompletedDate = lastTask.completedDate
                }
            }
            return tempCompletedDate
        }

    @get:Bindable
    var completedAt: String = ""
        get() {
            var tempCompletedDate = ""
            if (tasksNumber == 0 && tasksCompletedNumber > 0) {
                val lastTask = tasks
                        .filter { it.completedDate != null }
                        .sortedByDescending { it.completedDate }.firstOrNull()

                lastTask?.apply {
                    tempCompletedDate = dateFormatter.print(DateTime(lastTask.completedDate))
                }
            }
            return tempCompletedDate
        }

    @get:Bindable
    var deadlineExpired: Boolean = false
        get() {
            var value = false
            if (deadlineDate != null) {
                val deadlineDate = DateTime(deadlineDate).withTimeAtStartOfDay()
                return currentDate.isAfter(deadlineDate)
            }
            return value
        }

    @get:Bindable
    var hasDeadline: Boolean = false
        get() {
            return deadlineDate != null
        }

    @get:Bindable
    var timeTracked: String = "00:00"
        get() {
            return if (!tasks.isEmpty()) {

                var periods = 0f

                tasks.forEach {
                    periods += sumPeriods(it.timeEntries)
                }

                formatTimeToString(periods)
            } else {
                "00:00"
            }
        }

    @get:Bindable
    var lastWorkedFormatted: String = ""
        get() {
            var value = ""
            if (completed) {
                value = "${context?.getString(R.string.completed) ?: "Completed"}: ${completedAt}"
            } else {
                if (collapsed) {
                    value = if (tasksNumber == 0) {
                        context?.getString(R.string.no_tasks) ?: "No Tasks"
                    } else {
                        "$tasksNumber ${if (tasksNumber > 1) {
                            context?.getString(R.string.tasks) ?: "Tasks"
                        } else {
                            context?.getString(R.string.task) ?: "Task"
                        }}"
                    }
                } else {
                    if (tasksNumber == 0) {
                        value = context?.getString(R.string.no_tasks) ?: "No Tasks"
                    }
                    if (timerElapsedRunning > 0) {
                        value += "${context?.getString(R.string.timer_running) ?: "Timer running"}: "

                        val seconds = ((timerElapsedRunning + 10) / 10)
                        val minutes = (seconds / 60)
                        val hours = (minutes / 60)
                        if (hours > 0) {
                            value += "$hours ${if (hours > 1) {
                                context?.getString(R.string.hours)?.toLowerCase() ?: "hours"
                            } else {
                                context?.getString(R.string.hour) ?: "hour"
                            }} "
                        }
                        if (minutes > 0 && hours < 1) {
                            value += "$minutes ${if (minutes > 1) {
                                context?.getString(R.string.minutes) ?: "minutes"
                            } else {
                                context?.getString(R.string.minute) ?: "minute"
                            }} "
                        }
                        if (seconds > 0 && minutes < 1 && hours < 1) {
                            value += "< 1 ${context?.getString(R.string.minute) ?: "minute"}"
                        }
                    } else {
                        if (tasks.isNotEmpty()) {
                            val latestTimeEntriesForProjectTasks = mutableListOf<TimeEntryEntity>()
                            tasks.forEach {
                                if (!it.timeEntries.isEmpty()) {
                                    // get latest time entry:
                                    it.timeEntries
                                            .filter { it.startDate != null }.maxBy { it.startDate!! }
                                            ?.apply {
                                                latestTimeEntriesForProjectTasks.add(this)
                                            }
                                }
                            }

                            if (latestTimeEntriesForProjectTasks.size > 0) {
                                val latestProjectTimeEntry = latestTimeEntriesForProjectTasks
                                        .filter { it.startDate != null }.maxBy { it.startDate!! }

                                if (latestProjectTimeEntry?.startDate != null) {
                                    val period = Period(DateTime(latestProjectTimeEntry.startDate), DateTime())

                                    if (period.minutes < 1) {
                                        value = context?.getString(R.string.just_know) ?: "just know"
                                    }

                                    if (period.minutes > 0) {
                                        value = "${period.minutes} ${if (period.minutes > 1) {
                                            context?.getString(R.string.minutes) ?: "minutes"
                                        } else {
                                            context?.getString(R.string.minute) ?: "minute"
                                        }} ${context?.getString(R.string.ago) ?: "ago"}"
                                    }

                                    if (period.hours > 0) {
                                        value = "${period.hours} ${if (period.hours > 1) {
                                            context?.getString(R.string.hours) ?: "hours"
                                        } else {
                                            context?.getString(R.string.hour) ?: "hour"
                                        }} ${context?.getString(R.string.ago) ?: "ago"}"
                                    }

                                    if (period.days > 0) {
                                        value = "${period.days} ${if (period.days > 1) {
                                            context?.getString(R.string.days) ?: "days"
                                        } else {
                                            context?.getString(R.string.day) ?: "day"
                                        }} ${context?.getString(R.string.ago) ?: "ago"}"
                                    }

                                    if (period.months > 0) {
                                        value = "${period.months} ${if (period.months > 1) {
                                            context?.getString(R.string.months) ?: "months"
                                        } else {
                                            context?.getString(R.string.month) ?: "month"
                                        }} ${context?.getString(R.string.ago) ?: "ago"}"
                                    }

                                    if (period.years > 0) {
                                        value = "${period.years} ${if (period.years > 1) {
                                            context?.getString(R.string.years) ?: "years"
                                        } else {
                                            context?.getString(R.string.year) ?: "year"
                                        }} ${context?.getString(R.string.ago) ?: "ago"}"
                                    }
                                }
                            } else {
                                // display if timer running
                            }
                        }
                    }
                }
            }

            if (tasks.filter { it.timeEntries.isNotEmpty() }.isNotEmpty()) {
                value = " • $value"
            }

            return value
        }

    // endregion

    // region Relations

    @get:Bindable
    var parentName: String = context?.getString(R.string.root) ?: "Root"
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentName)
        }

    @get:Bindable
    var parentDrawable: Int = R.drawable.ic_root
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentDrawable)
        }

    @get:Bindable
    var parentColor: String = "#1e88e5"
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentColor)
        }

    @get:Bindable
    var tasksNumber: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.tasksNumber)
            notifyPropertyChanged(BR.completedAt)
            notifyPropertyChanged(BR.completed)
        }

    @get:Bindable
    var tasksCompletedNumber: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.tasksCompletedNumber)
            notifyPropertyChanged(BR.completedAt)
            notifyPropertyChanged(BR.completed)
        }

    @get:Bindable
    var progress: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.progress)
        }

    @get:Bindable
    var deadlineDetails: String = ""
        get() {
            if (deadlineDate != null) {
                val today = DateTime().clearTime().startOfDay()
                val deadline = DateTime(deadlineDate).clearTime().endOfDay()
                val daysDifference = Days.daysBetween(today.toLocalDate(), deadline.toLocalDate()).getDays()
                val message = when {
                    daysDifference < 0 -> context?.getString(R.string.overdue_lower) ?: "overdue"
                    daysDifference == 0 -> context?.getString(R.string.today_lower) ?: "today"
                    daysDifference == 1 -> context?.getString(R.string.tomorrow) ?: "tomorrow"
                    daysDifference > 1 -> "${context?.getString(R.string._in) ?: "in"} $daysDifference ${ context?.getString(R.string.days_on) ?: "days on"} ${dateFormatter.print(deadline)}"
                    else -> context?.getString(R.string.deadline_date) ?: "Deadline Date"
                }
                return "${context?.getString(R.string.the_project_deadline) ?: "The project deadline is"} $message. ${context?.getString(R.string.you_have) ?: "You have"} $tasksNumber ${if (tasksNumber != 1) {
                    context?.getString(R.string.uncompleted_tasks) ?: "uncompleted tasks"
                } else {
                    context?.getString(R.string.uncompleted_task) ?: "uncompleted task"
                }}."
            } else {
                return  context?.getString(R.string.deadline_details) ?: "Deadline details"
            }
        }

    // endregion

    // region View States

    @get:Bindable
    var editing: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.editing)
        }

    @get:Bindable
    var timerRunning: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.timerRunning)
        }

    @get:Bindable
    var completed: Boolean = false
        get() = tasksNumber == 0 && tasksCompletedNumber > 0

    fun removeDeadline() {
        deadlineDate = null
    }

    // endregion

    private fun sumPeriods(list: List<TimeEntryEntity>): Float {
        var sum = 0f
        list.forEach {
            val period = Period(DateTime(it.startDate), DateTime(it.endDate))
            sum += period.hours + period.days * 24
            sum += period.minutes / 60f
        }
        return sum
    }

    // region DiffUtil Functions

    override fun areItemsTheSame(itemToCompare: ProjectViewModel): Boolean {
        return itemToCompare.id == id
    }

    override fun areContentsTheSame(itemToCompare: ProjectViewModel): Boolean {
        return itemToCompare.title == title &&
                itemToCompare.color == color &&
                itemToCompare.notes == notes &&
                itemToCompare.createdDate == createdDate &&
                itemToCompare.createdDate == createdDate &&
                itemToCompare.deadlineDate == deadlineDate &&
                itemToCompare.area == area &&
                itemToCompare.tasksNumber == tasksNumber &&
                itemToCompare.timeTracked == timeTracked &&
                itemToCompare.lastWorkedFormatted == lastWorkedFormatted &&
                itemToCompare.progress == progress &&
                itemToCompare.trackProgress == trackProgress &&
                itemToCompare.displayOrder == displayOrder
    }

    fun validate() {
        validateProjectName()
    }

    private fun validateProjectName() {
        if (title.isEmpty() || title.isBlank()) {
            throw TitleNotSpecifiedException(EntityType.PROJECT.toString())
        }
    }

    // endregion
}
