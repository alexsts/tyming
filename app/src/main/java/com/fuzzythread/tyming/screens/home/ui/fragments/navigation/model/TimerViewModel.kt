package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model

import android.content.Context
import androidx.databinding.Bindable
import com.fuzzythread.tyming.BR
import com.fuzzythread.tyming.global.ObservableViewModel
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.db.entities.AreaEntity
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.db.entities.interfaces.ITaskParent
import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable
import com.fuzzythread.tyming.global.utils.other.ParentType
import java.util.*

class TimerViewModel(val context: Context) : ObservableViewModel(),
        IAdapterComparable<TimerViewModel> {

    // region Database Model Attributes

    var id: Long = 0

    @get:Bindable
    var title: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }

    @get:Bindable
    var notes: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.notes)
        }

    var createdDate: Date? = null
    var completedDate: Date? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.completed)
        }

    var deadlineDate: Date? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.hasDeadline)
            notifyPropertyChanged(BR.deadlineFormatted)
        }

    var parent: ITaskParent? = null
        set(value) {
            field = value
            if (value != null) {
                parentType = value.getParentType()
                when (parentType) {
                    ParentType.PROJECT -> {
                        with(value as ProjectEntity) {
                            parentName = title
                            parentColor = color
                            parentId = id
                            parentDrawable = R.drawable.ic_todo
                        }
                    }
                    ParentType.AREA -> {
                        with(value as AreaEntity) {
                            parentName = title
                            parentColor = color
                            parentId = id
                            parentDrawable = R.drawable.ic_area
                        }
                    }
                    else -> {
                    }
                }
            } else {
                parentId = 0
                parentType = ParentType.NONE
                parentName = context.getString(R.string.agenda)
                parentColor = context.getString(R.string.blue_600)
                parentDrawable = R.drawable.ic_task
            }
        }

    var timeEntries: MutableList<TimeEntryEntity> = mutableListOf()
        set(value) {
            field = value
            notifyPropertyChanged(BR.timeTrackedFormatted)
            notifyPropertyChanged(BR.lastWorkedFormatted)
            notifyPropertyChanged(BR.timeTracked)
        }

    // endregion

    // toggle switch
    @get:Bindable
    var timerRunning: Boolean = false
        get() {
            return timeElapsed > 0
        }

    @get:Bindable
    var timeElapsed: Long = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.timeElapsed)
            notifyPropertyChanged(BR.timerRunning)
            notifyPropertyChanged(BR.timeElapsedFormatted)
        }

    @get:Bindable
    var timeElapsedFormatted: String = ""
        get() {
            var value = "00:00"
            if (timeElapsed > 0) {
                val seconds = ((timeElapsed + 10) / 10) - 1
                val secondsCount = (seconds % 60)
                val minutes = (seconds / 60)
                val minutesCount = (minutes % 60)
                val hours = (minutes / 60)

                value = if (hours > 0) {
                    String.format("%02d:%02d", hours, minutesCount)
                } else {
                    String.format("%02d:%02d", minutesCount, secondsCount)
                }
            }
            return value
        }

    @get:Bindable
    var hasTags: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.hasTags)
        }

    @get:Bindable
    var parentType: ParentType = ParentType.NONE
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentType)
        }

    @get:Bindable
    var parentId: Long = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentId)
        }

    @get:Bindable
    var parentColor: String = "#1e88e5"
        get() {
            if (field.isEmpty()) {
                return "#1e88e5"
            }
            return field
        }
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentColor)
        }

    @get:Bindable
    var parentName: String = context.getString(R.string.project)
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentName)
        }

    @get:Bindable
    var parentDrawable: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentDrawable)
        }

    override fun areItemsTheSame(itemToCompare: TimerViewModel): Boolean {
        return itemToCompare.id == id
    }

    override fun areContentsTheSame(itemToCompare: TimerViewModel): Boolean {
        return itemToCompare.title == title &&
                itemToCompare.notes == notes &&
                itemToCompare.createdDate == createdDate &&
                itemToCompare.completedDate == completedDate &&
                itemToCompare.deadlineDate == deadlineDate &&
                itemToCompare.parentId == parentId &&
                itemToCompare.parentName == parentName &&
                itemToCompare.timeEntries == timeEntries &&
                itemToCompare.parentColor == parentColor
    }
}

