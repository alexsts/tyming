package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter;

import android.content.Context;
import android.view.View;

import androidx.core.view.ViewCompat;
import androidx.viewpager2.widget.ViewPager2;

public class CustPageTransformer implements ViewPager2.PageTransformer {

    private int maxTranslateOffsetX;
    private int pageMarginPx;
    private int offsetPx;
    private Context context;
    private ViewPager2 viewPager;

    public CustPageTransformer(Context context) {
        this.maxTranslateOffsetX = dp2px(context, 8);
        this.pageMarginPx = dp2px(context, 16);
        this.offsetPx = dp2px(context, 8);
        this.context = context;
    }

    public void transformPage(View view, float position) {
        if (viewPager == null) {
            viewPager = (ViewPager2) view.getParent().getParent();
        }

        int leftInScreen = view.getLeft() - viewPager.getScrollX();
        int centerXInViewPager = leftInScreen + view.getMeasuredWidth() / 2;
        int offsetX = centerXInViewPager - viewPager.getMeasuredWidth() / 2;
        float offsetRate = (float) offsetX * 0.05f / viewPager.getMeasuredWidth();
        float scaleFactor = 1 - Math.abs(offsetRate);
        if (scaleFactor > 0) {
            view.setScaleX(scaleFactor);
//            view.setScaleY(scaleFactor);
        }

        float offset = position * -(2 * offsetPx + pageMarginPx);
        if (viewPager.getOrientation() == ViewPager2.ORIENTATION_HORIZONTAL) {
            if (ViewCompat.getLayoutDirection(viewPager) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                view.setTranslationX(-offset);
            } else {
                view.setTranslationX(offset);
            }
        } else {
            view.setTranslationY(offset);
        }
    }


    /**
     * dp和像素转换
     */
    private int dp2px(Context context, float dipValue) {
        float m = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * m + 0.5f);
    }

}
