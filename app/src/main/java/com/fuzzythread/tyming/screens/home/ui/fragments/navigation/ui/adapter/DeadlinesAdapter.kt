package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter

import android.content.Context
import android.util.TypedValue
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.ui.DashboardFragment
import com.fuzzythread.tyming.databinding.ItemDeadlineBinding
import com.fuzzythread.tyming.global.utils.extensions.updateAdapterWithData
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel

open class DeadlinesAdapter(private val context: Context, private val homeFragment: DashboardFragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<ProjectViewModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemBinding = ItemDeadlineBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProjectDeadlinesViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder as ProjectDeadlinesViewHolder) {
            bind(items[position], position)
        }
    }

    fun removeItemAt(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, items.size)

        if (items.size == 0) {
            homeFragment.handleDeadlineNotificationsVisibility()
        }
    }

    fun refreshAdapterWithData(newDeadlines: MutableList<ProjectViewModel>) {
        val deadlinesViewModelsOld = items.toMutableList()
        items = newDeadlines
        updateAdapterWithData(deadlinesViewModelsOld, items)
    }

    private fun convertDpToPx(dp: Int): Int {
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp.toFloat(),
                context.resources.displayMetrics
        ).toInt()
    }

    override fun getItemCount() = items.size

    inner class ProjectDeadlinesViewHolder(
            private val itemBinding: ItemDeadlineBinding,
            private val view: View = itemBinding.root
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        var projectVM: ProjectViewModel? = null

        fun bind(projectViewModel: ProjectViewModel, position: Int) {
            projectVM = projectViewModel

            // margin for first and last item
            val params = ConstraintLayout.LayoutParams(
                    ConstraintLayout.LayoutParams.MATCH_PARENT,
                    ConstraintLayout.LayoutParams.MATCH_PARENT
            )
            if (position == 0) {
                params.setMargins(convertDpToPx(12), 0, convertDpToPx(16), 0)
            } else if (position == itemCount - 1) {
                params.setMargins(convertDpToPx(16), 0, convertDpToPx(12), 0)
            } else {
                params.setMargins(convertDpToPx(16), 0, convertDpToPx(16), 0)
            }
            if (itemCount == 1) {
                params.setMargins(convertDpToPx(12), 0, convertDpToPx(12), 0)
            }
            view.layoutParams = params

            itemBinding.dismissBtn.setOnClickListener {
                removeItemAt(position)
            }

            // bind repository:
            itemBinding.viewmodel = projectViewModel
        }
    }
}
