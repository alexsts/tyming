package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.fuzzythread.tyming.BuildConfig
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.ItemAreaBinding
import com.fuzzythread.tyming.databinding.ItemProjectBinding
import com.fuzzythread.tyming.databinding.ItemTaskBinding
import com.fuzzythread.tyming.global.TymingActivity
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.services.TimerService
import com.fuzzythread.tyming.global.ui.helpers.DragDropHelper
import com.fuzzythread.tyming.global.utils.extensions.updateAdapterWithData
import com.fuzzythread.tyming.global.utils.other.*
import com.fuzzythread.tyming.global.utils.ui.hideKeyboard
import com.fuzzythread.tyming.global.utils.ui.setBackgroundTint
import com.fuzzythread.tyming.global.utils.ui.setMarginStartEnd
import com.fuzzythread.tyming.screens.home.model.NavigationItem
import com.fuzzythread.tyming.screens.home.ui.dialog.NavigationItemDialog
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.AreaViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.HomeFragmentRepository
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TaskViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*
import kotlin.coroutines.CoroutineContext

class NavigationAdapter(
        private val activity: TymingActivity,
        private val refreshable: Refreshable,
        private val repository: HomeFragmentRepository,
        private val isArchive: Boolean,
        private val nestedScrollView: NestedScrollView? = null) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), DragDropHelper.Callback, CoroutineScope {

    var items: MutableList<NavigationItem<*>> = mutableListOf()
    private var mRecyclerView: RecyclerView? = null
    private var isDraggingEnabled: Boolean = false
    private var itemTouchHelper: DragDropHelper? = DragDropHelper()

    private var masterJob = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + masterJob

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
        mRecyclerView?.layoutManager = LinearLayoutManager(activity)
        (mRecyclerView?.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        mRecyclerView?.itemAnimator?.changeDuration = 0
    }

    private var timerService: TimerService? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            NavigationItem.AREA -> {
                val itemBinding = ItemAreaBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                AreaViewHolder(itemBinding)
            }
            NavigationItem.TASK -> {
                val itemBinding = ItemTaskBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                TaskViewHolder(itemBinding)
            }
            else -> {
                val itemBinding = ItemProjectBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                ProjectViewHolder(itemBinding)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item: NavigationItem<*> = items.get(position)
        with(item) {
            when (this.getItemType()) {
                NavigationItem.AREA -> {
                    with(holder as AreaViewHolder) {
                        bind(items[position], position)
                    }
                }
                NavigationItem.TASK -> {
                    with(holder as TaskViewHolder) {
                        bind(items[position])
                    }
                }
                NavigationItem.PROJECT -> {
                    with(holder as ProjectViewHolder) {
                        bind(items[position], position)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        when (items.get(position).getItemType()) {
            1 -> return NavigationItem.AREA
            2 -> return NavigationItem.PROJECT
            3 -> return NavigationItem.TASK
            else -> return -1
        }
    }

    fun removeItemAt(position: Int, entityType: EntityType) {
        // show dismiss dialog
        val dialog = ConfirmationDialog.getInstance(ActionType.DELETE, entityType)
        dialog.setConfirmationCallback { isConfirmed ->
            if (isConfirmed) {
                when (items[position].getItemType()) {
                    NavigationItem.AREA -> {
                        Toast.makeText(activity, activity.getString(R.string.area_deleted), Toast.LENGTH_LONG).show()
                    }
                    NavigationItem.TASK -> {
                        Toast.makeText(activity, activity.getString(R.string.task_deleted), Toast.LENGTH_LONG).show()
                    }
                    else -> {
                        Toast.makeText(activity, activity.getString(R.string.project_deleted), Toast.LENGTH_LONG).show()
                    }
                }
                repository.deleteNavigationItem(items[position], timerService)
                refreshable.refreshSelf()
            }
        }
        dialog.show(activity.supportFragmentManager, "Delete Navigation Item")
    }

    private fun convertDpToPx(dp: Int): Int {
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp.toFloat(),
                activity.resources.displayMetrics
        ).toInt()
    }


    fun refreshAdapterWithData(newNavItems: MutableList<NavigationItem<*>>) {
        val oldNavItems = items.toMutableList()
        items = newNavItems
        updateAdapterWithData(oldNavItems, items)

        // Updated areas element:
        items.forEachIndexed { index, navigationItem ->
            if (navigationItem.getItemType() == NavigationItem.AREA) {
                if (newNavItems.size != oldNavItems.size) {
                    notifyItemChanged(index)
                } else {
                    // old nav area item position changed
                    if (oldNavItems.size > 0 && oldNavItems[0].getItemType() == NavigationItem.AREA && newNavItems[0].getItemType() == NavigationItem.PROJECT) {
                        notifyItemChanged(index)
                    }
                }
            }

            if (navigationItem.getItemType() == NavigationItem.PROJECT) {
                if (newNavItems.size != oldNavItems.size) {
                    notifyItemChanged(index)
                } else {
                    // old nav area item position changed
                    if (oldNavItems.size > 0 && oldNavItems[0].getItemType() == NavigationItem.PROJECT && newNavItems[0].getItemType() == NavigationItem.TASK) {
                        notifyItemChanged(index)
                    }
                }
            }
        }
    }

    fun setTimerService(timerService: TimerService) {
        this.timerService = timerService
    }

    // region Drag & Drop, Custom Order
    fun setDraggingEnabled(isEnabled: Boolean) {
        if (isEnabled) {
            itemTouchHelper = DragDropHelper()
            itemTouchHelper?.isScrollEnabled = true
            itemTouchHelper?.isTranslateEnabled = true
            itemTouchHelper?.attach(mRecyclerView, this, nestedScrollView)
            isDraggingEnabled = true
        } else {
            itemTouchHelper = null
            isDraggingEnabled = false
        }
    }

    private fun updateDisplayOrder() {
        launch(Dispatchers.IO) {
            repository.updateDisplayOrder(items)
            refreshable.refreshSelf()
        }
    }

    override fun onDragStarted(holder: RecyclerView.ViewHolder, create: Boolean) {
        when (holder) {
            is ProjectViewHolder -> {
                with(holder.itemBinding) {
                    this@with.surfaceProjectView.setMarginStartEnd(0, 0)
                    this@with.surfaceProjectView.setBackgroundTint(R.color.grey_850)
                    this@with.handleProject.visibility = View.VISIBLE
                    this@with.projectDialog.visibility = View.GONE
                    this@with.surfaceProjectView.translationZ = 100f
                }
            }
            is TaskViewHolder -> {
                with(holder.itemBinding) {
                    this@with.surfaceViewTask.setMarginStartEnd(0, 0)
                    this@with.surfaceViewTask.setBackgroundTint(R.color.grey_850)
                    this@with.handleTask.visibility = View.VISIBLE
                    this@with.taskDialog.visibility = View.GONE
                    this@with.surfaceViewTask.translationZ = 100f
                }
            }
            is AreaViewHolder -> {
                with(holder.itemBinding) {
                    this@with.surfaceViewA.setMarginStartEnd(0, 0)
                    this@with.surfaceViewA.setBackgroundTint(R.color.grey_850)
                    this@with.areaDialog.visibility = View.GONE
                    this@with.handleArea.visibility = View.VISIBLE
                    this@with.surfaceViewA.translationZ = 100f
                }
            }
        }
    }

    override fun onDragMoved(holder: RecyclerView.ViewHolder, x: Int, y: Int) {
    }

    override fun onDragTo(holder: RecyclerView.ViewHolder, to: Int): Int {
        val from = holder.adapterPosition
        Collections.swap(items, from, to)
        notifyItemMoved(from, to)
        return to
    }

    override fun onDragStopped(holder: RecyclerView.ViewHolder, destroy: Boolean) {
        when (holder) {
            is TaskViewHolder -> {
                with(holder.itemBinding) {
                    this@with.surfaceViewTask.setMarginStartEnd(12, 12)
                    this@with.surfaceViewTask.setBackgroundTint(R.color.grey_875)
                    this@with.handleTask.visibility = View.GONE
                    this@with.taskDialog.visibility = View.VISIBLE
                    this@with.surfaceViewTask.translationZ = 1f
                }
            }
            is ProjectViewHolder -> {
                with(holder.itemBinding) {
                    this@with.surfaceProjectView.setMarginStartEnd(12, 12)
                    this@with.surfaceProjectView.setBackgroundTint(R.color.grey_875)
                    this@with.handleProject.visibility = View.GONE
                    this@with.projectDialog.visibility = View.VISIBLE
                    this@with.surfaceProjectView.translationZ = 1f
                }
            }
            is AreaViewHolder -> {
                with(holder.itemBinding) {
                    this@with.surfaceViewA.setMarginStartEnd(12, 12)
                    this@with.surfaceViewA.setBackgroundTint(R.color.grey_875)
                    this@with.areaDialog.visibility = View.VISIBLE
                    this@with.handleArea.visibility = View.GONE
                    this@with.surfaceViewA.translationZ = 1f
                }
            }
        }
        updateDisplayOrder()
    }
    // endregion

    inner class AreaViewHolder(
            val itemBinding: ItemAreaBinding,
            private val view: View = itemBinding.root
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(navigationItem: NavigationItem<*>, position: Int) {

            val areaVM = navigationItem.getItemEntity() as AreaViewModel

            // bind repository:
            itemBinding.viewmodel = areaVM
            itemBinding.executePendingBindings()

            val params = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    convertDpToPx(60)
            )
            if (position == 0) {
                params.setMargins(0, 0, 0, convertDpToPx(3))
            } else {
                params.setMargins(0, convertDpToPx(9), 0, convertDpToPx(3))
                if (areaVM.collapsed) {
                    // item at top is collapsed area
                    if (position > 0 && items.size > 1 && items[position - 1].getItemType() == NavigationItem.AREA && items[position - 1].isCollapsed) {
                        params.setMargins(0, 0, 0, convertDpToPx(3))
                    }
                }
            }

            itemBinding.itemArea.layoutParams = params

            itemBinding.surfaceViewA.setOnLongClickListener {
                val collapsed = areaVM.collapsed
                if (areaVM.collapsed) {
                    itemTouchHelper?.start(adapterPosition)
                } else {
                    if (isDraggingEnabled) {
                        Toast.makeText(activity, activity.getString(R.string.collapse_item), Toast.LENGTH_LONG).show()
                    }
                }
                collapsed
            }

            itemBinding.areaDialog.setOnClickListener {
                activity.hideKeyboard()
                NavigationItemDialog()
                        .setDialogConfiguration(entityType = EntityType.AREA, flag = isArchive)
                        .setAddCallBack {
                            ProjectDialog()
                                    .getInstance(parentId = areaVM.id, parentType = ParentType.AREA)
                                    .setDismissCallBack {
                                        refreshable.refreshSelf()
                                    }
                                    .show(activity.supportFragmentManager, "Add Project")
                        }
                        .setDeleteCallBack {
                            removeItemAt(layoutPosition, EntityType.AREA)
                        }
                        .setExtraCallBack {
                            repository.markAreaArchived(areaVM.id, timerService = timerService, markArchived = !isArchive)
                            if (isArchive) {
                                Toast.makeText(activity, activity.getString(R.string.area_unarchived), Toast.LENGTH_LONG).show()
                            } else {
                                Toast.makeText(activity, activity.getString(R.string.area_archived), Toast.LENGTH_LONG).show()
                            }
                            refreshable.refreshAll()
                        }
                        .setEditCallBack {
                            val dialog = AreaDialog().getInstance(areaId = areaVM.id)
                            dialog.setDismissCallBack {
                                refreshable.refreshSelf()
                            }
                            dialog.show(activity.supportFragmentManager, "Edit Area")
                        }
                        .show(activity.supportFragmentManager, "Combined Item Dialog")
            }


            // surface view:
            itemBinding.surfaceViewA.setOnClickListener {
                activity.hideKeyboard()
                repository.collapseArea(areaVM.id)
                refreshable.refreshSelf()
            }
        }
    }

    inner class ProjectViewHolder(
            val itemBinding: ItemProjectBinding,
            private val view: View = itemBinding.root
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(navigationItem: NavigationItem<*>, position: Int) {

            val projectVM = navigationItem.getItemEntity() as ProjectViewModel

            // bind repository:
            itemBinding.viewmodel = projectVM
            itemBinding.executePendingBindings()

            val params = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    convertDpToPx(60)
            )

            params.setMargins(0, 0, 0, convertDpToPx(3))

            itemBinding.itemProject.layoutParams = params

            itemBinding.surfaceProjectView.setOnLongClickListener {
                val collapsed = projectVM.collapsed
                if (projectVM.collapsed) {
                    itemTouchHelper?.start(adapterPosition)
                } else {
                    if (isDraggingEnabled) {
                        Toast.makeText(activity, activity.getString(R.string.collapse_item), Toast.LENGTH_LONG).show()
                    }
                }
                collapsed
            }

            // surface view:
            itemBinding.surfaceProjectView.setOnClickListener {
                activity.hideKeyboard()
                repository.collapseProject(projectVM.id)
                refreshable.refreshSelf()
            }

            itemBinding.projectDialog.setOnClickListener {
                activity.hideKeyboard()
                NavigationItemDialog()
                        .setDialogConfiguration(entityType = EntityType.PROJECT, flag = isArchive, trackProgress = projectVM.trackProgress)
                        .setAddCallBack {
                            TaskDialog()
                                    .getInstance(parentId = projectVM.id, parentType = ParentType.PROJECT)
                                    .setDismissCallBack {
                                        refreshable.refreshSelf()
                                    }
                                    .show(activity.supportFragmentManager, "Add Task")
                        }
                        .setDeleteCallBack {
                            removeItemAt(layoutPosition, EntityType.PROJECT)
                        }
                        .setExtraCallBack {
                            repository.markProjectArchived(projectVM.id, timerService, markArchived = !isArchive)
                            if (isArchive) {
                                Toast.makeText(activity, activity.getString(R.string.project_unarchived), Toast.LENGTH_LONG).show()
                            } else {
                                Toast.makeText(activity, activity.getString(R.string.project_archived), Toast.LENGTH_LONG).show()
                            }
                            refreshable.refreshAll()
                        }
                        .setEditCallBack {
                            ProjectDialog()
                                    .getInstance(projectId = projectVM.id)
                                    .setDismissCallBack {
                                        refreshable.refreshSelf()
                                    }
                                    .show(activity.supportFragmentManager, "Edit Project")
                        }
                        .show(activity.supportFragmentManager, "Combined Item Dialog")
            }
        }
    }

    inner class TaskViewHolder(
            var itemBinding: ItemTaskBinding,
            private val view: View = itemBinding.root
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        var taskVM: TaskViewModel? = null

        fun unbind(taskViewModel: TaskViewModel) {
            itemBinding.viewmodel = null
            itemBinding.executePendingBindings()
        }

        fun clearAnimation() {
            view.clearAnimation()
        }

        fun bind(navigationItem: NavigationItem<*>) {
            val taskViewModel = navigationItem.getItemEntity() as TaskViewModel
            taskVM = taskViewModel

            // bind repository:
            itemBinding.viewmodel = taskViewModel
            itemBinding.executePendingBindings()

            var trackProgress = false
            if (trackProgress != null && taskViewModel.parent is ProjectEntity) {
                trackProgress = (taskViewModel.parent as ProjectEntity).trackProgress
            }

            itemBinding.surfaceViewTask.setOnLongClickListener {
                val completed = taskVM?.completed ?: false
                if (!completed) {
                    itemTouchHelper?.start(adapterPosition)
                }
                completed
            }

            itemBinding.completedContainer.setOnClickListener {
                activity.hideKeyboard()
                repository.updateTask(taskViewModel.apply { completedDate = null })
                refreshable.refreshAll()
                Toast.makeText(activity, activity.getString(R.string.task_uncompleted), Toast.LENGTH_LONG).show()
            }

            if (!isArchive) {

                val taskListener = object : View.OnClickListener {
                    override fun onClick(v: View?) {
                        activity.hideKeyboard()
                        if (taskViewModel.timerRunning) {
                            taskViewModel.timerRunning = false
                            repository.stopTaskTimer(taskViewModel.id)
                            timerService?.stopTimer(taskViewModel.id)
                            // Update task viewmodel (time entries):
                            taskViewModel.timeEntries = repository
                                    .getTaskViewModel(taskId = taskViewModel.id)
                                    ?.timeEntries
                                    ?: mutableListOf()
                            refreshable.refreshAll()
                        } else {
                            val runningTimers = repository.getRunningTasksCount()

                            // handle Free Account
                            if (!BuildConfig.IS_PREMIUM) {
                                if (runningTimers == 1L) {
                                    Toast.makeText(activity, activity.getString(R.string.only_1_timer_allowed_free), Toast.LENGTH_LONG).show()
                                    return
                                }
                            } else {
                                // check if multiple timers allowed
                                if (PrefUtil.getMultipleTimersEnabled(activity)) {
                                    if (runningTimers >= Constants.TIMERS_LIMIT) {
                                        Toast.makeText(activity, activity.getString(R.string.exceeded_timers_amount), Toast.LENGTH_LONG).show()
                                        return
                                    }
                                } else {
                                    if (runningTimers == 1L) {
                                        Toast.makeText(activity, activity.getString(R.string.single_timer_mode), Toast.LENGTH_LONG).show()
                                        return
                                    }
                                }
                            }

                            taskViewModel.timerRunning = true
                            timerService?.startTimer(taskViewModel.id)
                            refreshable.refreshSelf(refreshTimers = true)
                        }
                    }
                }
                itemBinding.toggleTimerContainer.setOnClickListener(taskListener)
                itemBinding.surfaceViewTask.setOnClickListener(taskListener)
            }

            itemBinding.taskDialog.setOnClickListener {
                activity.hideKeyboard()
                NavigationItemDialog()
                        .setDialogConfiguration(entityType = EntityType.TASK, flag = taskViewModel.completed, trackProgress = trackProgress)
                        .setAddCallBack {
                            val dialog = TimeEntryDialog.getInstance(taskId = taskViewModel.id)
                            dialog.setDismissCallBack {
                                refreshable.refreshAll()
                                // Update task viewmodel (time entries) TODO: remove duplication
                                taskViewModel.timeEntries = repository
                                        .getTaskViewModel(taskId = taskViewModel.id)
                                        ?.timeEntries
                                        ?: mutableListOf()
                            }
                            dialog.show(activity.supportFragmentManager, "Add TimeEntry")
                        }
                        .setDeleteCallBack {
                            if (taskViewModel.timerRunning) {
                                Toast.makeText(activity, activity.getString(R.string.stop_timer_before_deleting_task), Toast.LENGTH_LONG).show()
                            } else {
                                removeItemAt(layoutPosition, EntityType.TASK)
                            }
                        }
                        .setExtraCallBack {
                            if (taskViewModel.completed) {
                                repository.updateTask(taskViewModel.apply { completedDate = null })
                                refreshable.refreshAll()
                                Toast.makeText(activity, activity.getString(R.string.task_uncompleted), Toast.LENGTH_LONG).show()
                            } else {
                                if (taskViewModel.timerRunning) {
                                    Toast.makeText(activity, activity.getString(R.string.stop_timer_before_completing_task), Toast.LENGTH_LONG).show()
                                } else {
                                    taskViewModel.completeTask()
                                    repository.updateTask(taskViewModel)
                                    refreshable.refreshAll()
                                    Toast.makeText(activity, activity.getString(R.string.task_completed), Toast.LENGTH_LONG).show()
                                }
                            }
                        }
                        .setEditCallBack {
                            TaskDialog()
                                    .getInstance(taskId = taskViewModel.id)
                                    .setDismissCallBack {
                                        refreshable.refreshAll()
                                    }
                                    .show(activity.supportFragmentManager, "Edit Task")
                        }
                        .show(activity.supportFragmentManager, "Combined Item Dialog")
            }
        }
    }
}
