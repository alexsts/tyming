package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fuzzythread.tyming.global.ui.helpers.ItemTouchHelperAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.fuzzythread.tyming.databinding.ItemParentBinding
import com.fuzzythread.tyming.global.utils.extensions.updateAdapterWithData
import com.fuzzythread.tyming.global.utils.other.ParentType
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ParentViewModel

/**
 * RecyclerView.Adapter that implements [ItemTouchHelperAdapter] to respond to move and
 * dismiss events from a [android.support.v7.widget.helper.ItemTouchHelper].
 * Also uses swipelayout [com.daimajia.swipe.SwipeLayout] to move controls underneath an item.
 */
class ParentsAdapter(
        private val context: Context,
        private var parentId: Long,
        private var parentType: ParentType
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<ParentViewModel> = mutableListOf()
    private var callback: ((Long, ParentType) -> Unit)? = null
    private var recyclerView: RecyclerView? = null
    var selectedItemPosition = RecyclerView.NO_POSITION

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
        this.recyclerView?.layoutManager = LinearLayoutManager(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemBinding = ItemParentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ParentViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder as ParentViewHolder) {
            bind(items[position])
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun refresh(items: MutableList<ParentViewModel>, parentId: Long, parentType: ParentType) {
        val oldItems = this.items
        this.items = items
        this.parentId = parentId
        this.parentType = parentType
        if (parentId == 0L) {
            selectedItemPosition = RecyclerView.NO_POSITION
        }
        // update RV using DiffUtil
        updateAdapterWithData(oldItems, this.items)
    }

    fun setSelectedCallback(callback: (Long, ParentType) -> Unit) {
        this.callback = callback
    }

    inner class ParentViewHolder(
            private val itemBinding: ItemParentBinding,
            private val view: View = itemBinding.root
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(parentVM: ParentViewModel) {

            // bind repository:
            itemBinding.viewmodel = parentVM
            itemBinding.executePendingBindings()

            if (parentId == parentVM.id && selectedItemPosition == RecyclerView.NO_POSITION) {
                selectedItemPosition = layoutPosition
            }

            val isSelected = layoutPosition == selectedItemPosition
            parentVM.selected = isSelected

            itemBinding.itemParent.setOnClickListener {

                if (selectedItemPosition != RecyclerView.NO_POSITION) {
                    notifyItemChanged(selectedItemPosition)
                }

                parentVM.selected = true
                selectedItemPosition = layoutPosition
                notifyItemChanged(layoutPosition)
                callback?.invoke(parentVM.id, parentType)
            }
        }
    }
}
