package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import com.fuzzythread.tyming.R
import android.view.Gravity
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import com.fuzzythread.tyming.databinding.DialogConfirmationBinding
import com.fuzzythread.tyming.global.utils.other.ActionType
import com.fuzzythread.tyming.global.utils.other.EntityType

class ConfirmationDialog : AppCompatDialogFragment() {

    private var confirmationCallback: ((Boolean) -> Unit)? = null
    private var actionType: ActionType = ActionType.DELETE
    private var entityType: EntityType = EntityType.TASK

    // UI:
    private var mView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        // restore bundle:
        actionType = ActionType.valueOf(arguments?.getString(ACTION_TYPE) ?: "DELETE")
        entityType = EntityType.valueOf(arguments?.getString(ENTITY_TYPE) ?: "TASK")
        super.onCreate(savedInstanceState)
    }

    fun setConfirmationCallback(func: ((Boolean) -> Unit)) {
        confirmationCallback = func
    }

    private fun removeCallBacks() {
        confirmationCallback = null
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val binding = DialogConfirmationBinding.inflate(inflater, container, false)
        initializeListeners(binding)
        setDialogPosition()
        return binding.root
    }

    private fun initializeListeners(binding: DialogConfirmationBinding) {

        val actionName = when (actionType){
            ActionType.ADD -> context?.getString(R.string.add)
            ActionType.EDIT -> context?.getString(R.string.edit)
            ActionType.DELETE -> context?.getString(R.string.delete)
        }

        val entityName = when (entityType){
            EntityType.TASK -> context?.getString(R.string.add)
            EntityType.PROJECT -> context?.getString(R.string.project)
            EntityType.AREA -> context?.getString(R.string.area)
            EntityType.TIMERECORD -> context?.getString(R.string.time_record)
        }

        binding.titleTv.text = "${binding.titleTv.text} $actionName ${context?.getString(R.string.the)} $entityName?"
        binding.actionBtn.text = actionType.name

        binding.cancelBtn.setOnClickListener {
            confirmationCallback?.invoke(false)
            dialog?.dismiss()
        }

        binding.actionBtn.setOnClickListener {
            confirmationCallback?.invoke(true)
            dialog?.dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        val params = window!!.attributes
        params.dimAmount = 0.6f
        window.attributes = params
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(width, height)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.setCanceledOnTouchOutside(true)
    }

    private fun setDialogPosition() {
        val window = dialog?.window
        window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL)
        val params = window.attributes
        params.windowAnimations = R.style.DialogSlideBottom
        window.attributes = params
    }

    override fun onDismiss(dialog: DialogInterface) {
        removeCallBacks()
        super.onDismiss(dialog)
    }

    companion object {
        const val ENTITY_TYPE = "EDITING_TYPE"
        const val ACTION_TYPE = "ACTION_TYPE"

        fun getInstance(actionType: ActionType, entityType: EntityType): ConfirmationDialog {
            val fragment = ConfirmationDialog()
            fragment.arguments = Bundle().apply {
                putString(ACTION_TYPE, actionType.toString())
                putString(ENTITY_TYPE, entityType.toString())
            }
            return fragment
        }
    }
}
