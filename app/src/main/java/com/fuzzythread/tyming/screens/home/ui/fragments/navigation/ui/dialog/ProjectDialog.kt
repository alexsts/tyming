package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.lifecycle.ViewModelProvider
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import com.fuzzythread.tyming.BuildConfig
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.DialogProjectBinding
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.global.exception.TitleNotSpecifiedException
import com.fuzzythread.tyming.global.ui.elements.TymingDialog
import com.fuzzythread.tyming.global.utils.other.ActionType
import com.fuzzythread.tyming.global.utils.other.EditingType
import com.fuzzythread.tyming.global.utils.other.ParentType
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model.ProjectDialogRepository
import java.util.*
import javax.inject.Inject


class ProjectDialog : TymingDialog() {

    private var toggleCompletable: AppCompatCheckBox? = null

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val projectDialogVM: ProjectDialogRepository by lazy {
        ViewModelProvider(this, viewModelFactory).get(ProjectDialogRepository::class.java)
    }

    private var projectViewModel: ProjectViewModel? = null
    private var callback: (() -> Unit)? = null
    private var projectId: Long = 0
    private var actionType: ActionType = ActionType.ADD

    fun getInstance(projectId: Long = 0, parentId: Long = 0, parentType: ParentType = ParentType.NONE): ProjectDialog {
        val fragment = ProjectDialog()
        fragment.arguments = Bundle().apply {
            putLong(PROJECT_ID, projectId)
            putLong(PARENT_ID, parentId)
            putString(PARENT_TYPE, parentType.toString())
        }
        return fragment
    }

    fun setDismissCallBack(func: () -> Unit): ProjectDialog {
        callback = func
        return this
    }

    private fun removeDismissCallBack() {
        callback = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // restore bundle:
        projectId = arguments?.getLong(PROJECT_ID) ?: 0

        projectViewModel = if (projectId > 0) {
            actionType = ActionType.EDIT
            projectDialogVM.getProjectViewModel(projectId)?.apply { editing = true }
        } else {
            val parentId = arguments?.getLong(PARENT_ID) ?: 0
            val parentType = ParentType.valueOf(arguments?.getString(PARENT_TYPE) ?: "NONE")
            ProjectViewModel(this.context?.applicationContext).also {
                if (parentId > 0) {
                    it.area = if (parentType == ParentType.AREA) {
                        projectDialogVM.getAreaEntity(parentId)
                    } else {
                        null
                    }
                } else {
                    it.area = null
                }
            }
        }

        // handle Free Account:
        if (actionType == ActionType.ADD && !BuildConfig.IS_PREMIUM) {
            val projectsCountAfter = projectDialogVM.getProjectsCount()
            if (projectsCountAfter >= PROJECT_LIMIT) {
                Toast.makeText(this.context, context?.getString(R.string.oh_no), Toast.LENGTH_LONG).show()
                callback?.invoke()
                dismiss()
            }
        }
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        // bind ViewModel:
        val itemBinding = DialogProjectBinding.inflate(inflater, container, false)
        itemBinding.viewmodel = projectViewModel
        initializeUI(itemBinding)
        setDialogPosition()
        return itemBinding.root
    }

    private fun initializeUI(binding: DialogProjectBinding) {
        initializeCalendar(binding)
        initializeListeners(binding)
    }

    private fun initializeCalendar(binding: DialogProjectBinding) {
        val calendar = Calendar.getInstance()
        projectViewModel!!.deadlineDate?.apply {
            calendar.time = this
        }

        val calendarListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            projectViewModel!!.deadlineDate = calendar.time
        }

        val listener = View.OnClickListener {
            val dialog = DatePickerDialog(
                    dialog?.context!!,
                    R.style.DialogTheme,
                    calendarListener,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH))
            dialog.show()
        }

        // show calendar dialog:
        binding.deadlineAdd.setOnClickListener(listener)
        binding.deadlineDate.setOnClickListener(listener)
    }

    private fun initializeListeners(binding: DialogProjectBinding) {
        binding.color.setOnClickListener {
            hideKeyboard()
            ColorPickerDialogBuilder
                    .with(context, R.style.Theme_Dialog)
                    .setTitle(context?.getString(R.string.choose_color))
                    .showAlphaSlider(false)
                    .initialColor(Color.parseColor(projectViewModel!!.color))
                    .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                    .density(12)
                    .setOnColorSelectedListener { selectedColor ->
                    }
                    .setPositiveButton(context?.getString(R.string.ok)) { dialog, selectedColor, allColors ->
                        projectViewModel!!.color = "#" + Integer.toHexString(selectedColor)
                        dialog.dismiss()
                    }
                    .setNegativeButton(context?.getString(R.string.cancel)) { dialog, which ->
                        dialog.dismiss()
                    }
                    .build()
                    .show()
        }

        binding.checkboxWrapper.setOnClickListener {
            if (projectViewModel!!.trackProgress) {
                if (projectViewModel!!.tasksCompletedNumber > 0) {
                    Toast.makeText(this.context, context?.getString(R.string.project_has_completed_tasks), Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                projectViewModel!!.deadlineDate = null
            }
            projectViewModel!!.trackProgress = !projectViewModel!!.trackProgress
        }

        binding.root.setOnClickListener {
            hideKeyboard()
        }

        binding.save.setOnClickListener {
            try {
                hideKeyboard()
                binding.notes.clearFocus()
                binding.titleTv.clearFocus()
                projectViewModel!!.validate()
                // db update:
                if (actionType == ActionType.ADD) {
                    projectDialogVM.addProject(projectViewModel!!)

                    // handle Free Account:
                    if (!BuildConfig.IS_PREMIUM){
                        val projectsCountAfter = projectDialogVM.getProjectsCount()
                        Toast.makeText(this.context, "${context?.getString(R.string.project_added)} (${PROJECT_LIMIT - projectsCountAfter} ${context?.getString(R.string.left)})", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(this.context, context?.getString(R.string.project_added), Toast.LENGTH_LONG).show()
                    }
                } else {
                    projectDialogVM.updateProject(projectViewModel!!)
                    Toast.makeText(this.context, context?.getString(R.string.project_updated), Toast.LENGTH_LONG).show()
                }

                // update parent view by callback:
                callback?.invoke()
                dialog?.dismiss()
            } catch (titleException: TitleNotSpecifiedException) {
                Toast.makeText(context, titleException.message, Toast.LENGTH_LONG).show()
            }
        }

        binding.parentContainer.setOnClickListener {
            val parentId = projectViewModel!!.area?.id ?: 0
            val parentType = if (parentId > 0) ParentType.AREA else ParentType.NONE
            val dialog = ParentDialog().getInstance(parentId, parentType, EditingType.PROJECT)
            dialog.setChooseParentCallBack { parentId: Long, parentType: ParentType ->
                if (parentType == ParentType.AREA) {
                    projectViewModel!!.area = projectDialogVM.getAreaEntity(parentId)
                } else {
                    projectViewModel!!.area = null
                }
            }
            this.activity?.supportFragmentManager?.let { dialog.show(it, "Edit Project Parent") }
        }
    }

    private fun toggleDeadline() {
        if (projectViewModel!!.trackProgress) {
            if (projectViewModel!!.tasksCompletedNumber > 0) {
                toggleCompletable?.isChecked = true
                Toast.makeText(this.context, context?.getString(R.string.project_has_completed_tasks), Toast.LENGTH_LONG).show()
                return
            }
            projectViewModel!!.deadlineDate = null
        }
        projectViewModel!!.trackProgress = !projectViewModel!!.trackProgress
        toggleCompletable?.isChecked = projectViewModel!!.trackProgress
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        val params = window!!.attributes
        params.dimAmount = 0.6f
        window.attributes = params
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(width, height)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.setCanceledOnTouchOutside(true)
    }

    private fun setDialogPosition() {
        val window = dialog?.window
        window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL)
        val params = window.attributes
        params.windowAnimations = R.style.DialogSlideBottom
        window.attributes = params
    }

    override fun onDismiss(dialog: DialogInterface) {
        callback?.invoke()
        removeDismissCallBack()
        super.onDismiss(dialog)
    }

    companion object {
        const val PROJECT_ID = "PROJECT_ID"
        const val PARENT_ID = "PARENT_ID"
        const val PARENT_TYPE = "PARENT_TYPE"
        const val PROJECT_LIMIT = 8
    }
}
