package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import com.fuzzythread.tyming.R
import android.view.Gravity
import android.view.ViewGroup
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TaskViewModel
import dagger.android.support.DaggerAppCompatDialogFragment
import javax.inject.Inject
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.fuzzythread.tyming.databinding.DialogTaskBinding
import com.fuzzythread.tyming.global.exception.ProjectNotSelectedException
import com.fuzzythread.tyming.global.exception.TitleNotSpecifiedException
import com.fuzzythread.tyming.global.utils.other.ActionType
import com.fuzzythread.tyming.global.utils.other.EditingType
import com.fuzzythread.tyming.global.utils.other.ParentType
import com.fuzzythread.tyming.global.utils.ui.hideKeyboard
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model.TaskDialogVM

class TaskDialog : DaggerAppCompatDialogFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val taskDialogVM: TaskDialogVM by lazy {
        ViewModelProvider(this, viewModelFactory).get(TaskDialogVM::class.java)
    }

    private var taskViewModel: TaskViewModel? = null
    private var callback: (() -> Unit)? = null
    private var taskId: Long = 0
    private var actionType: ActionType = ActionType.ADD

    fun getInstance(taskId: Long = 0, parentId: Long = 0, parentType: ParentType = ParentType.NONE): TaskDialog {
        val fragment = TaskDialog()
        fragment.arguments = Bundle().apply {
            putLong(TASK_ID, taskId)
            putLong(PARENT_ID, parentId)
            putString(PARENT_TYPE, parentType.toString())
        }
        return fragment
    }

    fun setDismissCallBack(func: () -> Unit): TaskDialog {
        callback = func
        return this
    }

    fun removeDismissCallBack() {
        callback = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // restore bundle:
        taskId = arguments?.getLong(TASK_ID) ?: 0
        taskViewModel = if (taskId > 0) {
            actionType = ActionType.EDIT
            taskDialogVM.getTaskViewModel(taskId)?.apply { editing = true }
        } else {
            val parentId = arguments?.getLong(PARENT_ID) ?: 0
            val parentType = ParentType.valueOf(arguments?.getString(PARENT_TYPE) ?: "NONE")
            TaskViewModel(this.context?.applicationContext).also {
                if (parentId > 0) {
                    when (parentType) {
                        ParentType.PROJECT -> {
                            it.parent = taskDialogVM.getProjectEntity(parentId)
                        }
                        ParentType.NONE -> {
                            it.parent = null
                        }

                        ParentType.AREA -> {}
                    }
                } else {
                    it.parent = null
                }
            }
        }
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        // bind ViewModel:
        val itemBinding = DialogTaskBinding.inflate(inflater, container, false)
        itemBinding.viewmodel = taskViewModel

        initializeListeners(itemBinding)
        setDialogPosition()
        return itemBinding.root
    }

    private fun initializeListeners(binding: DialogTaskBinding){
        binding.save.setOnClickListener {
            try {
                taskViewModel!!.validate()
                // db update:
                if (actionType == ActionType.ADD) {
                    taskDialogVM.addTask(taskViewModel!!)
                    Toast.makeText(this.context, context?.getString(R.string.task_added), Toast.LENGTH_LONG).show()
                } else {
                    taskDialogVM.updateTask(taskViewModel!!)
                    Toast.makeText(this.context, context?.getString(R.string.task_updated), Toast.LENGTH_LONG).show()
                }
                // update parent view by callback:
                callback?.invoke()
                dialog?.dismiss()
            } catch (titleException: TitleNotSpecifiedException) {
                Toast.makeText(context, titleException.message, Toast.LENGTH_LONG).show()
            } catch (projectException: ProjectNotSelectedException) {
                Toast.makeText(context, projectException.message, Toast.LENGTH_LONG).show()
            }
        }

        binding.root.setOnClickListener {
            hideKeyboard()
        }

        binding.parentContainer.setOnClickListener {
            val dialog = ParentDialog().getInstance(taskViewModel!!.parentId, taskViewModel!!.parentType, EditingType.TASK)
            dialog.setChooseParentCallBack { parentId: Long, parentType: ParentType ->
                when (parentType) {
                    ParentType.PROJECT -> {
                        taskViewModel!!.parent = taskDialogVM.getProjectEntity(parentId)
                    }
                    ParentType.NONE -> {
                        taskViewModel!!.parent = null
                    }

                    ParentType.AREA -> {}
                }
            }
            this.activity?.supportFragmentManager?.let { it1 -> dialog.show(it1, "Edit Project Parent") }
        }
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        val params = window!!.attributes
        params.dimAmount = 0.6f
        window.attributes = params
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(width, height)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.setCanceledOnTouchOutside(true)
    }

    private fun setDialogPosition() {
        val window = dialog?.window
        window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL)
        val params = window.attributes
        params.windowAnimations = R.style.DialogSlideBottom
        window.attributes = params
    }

    override fun onDismiss(dialog: DialogInterface) {
        callback?.invoke()
        removeDismissCallBack()
        super.onDismiss(dialog)
    }

    companion object {
        const val TASK_ID = "TASK_ID"
        const val PARENT_ID = "PARENT_ID"
        const val PARENT_TYPE = "PARENT_TYPE"
    }
}
