package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.aigestudio.wheelpicker.WheelPicker
import com.aigestudio.wheelpicker.WheelPicker.ALIGN_LEFT
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.DialogTimentryBinding
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.global.exception.DurationNotSelectedException
import com.fuzzythread.tyming.global.exception.TaskNotSelectedException
import com.fuzzythread.tyming.global.ui.elements.TymingDialog
import com.fuzzythread.tyming.global.utils.other.ActionType
import com.fuzzythread.tyming.global.utils.other.DisplayMode
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import com.fuzzythread.tyming.global.utils.ui.hideKeyboard
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TimeEntryViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model.TimeEntryDialogRepository
import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import java.util.*
import javax.inject.Inject

class TimeEntryDialog : TymingDialog() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val timeEntryDialogVM: TimeEntryDialogRepository by lazy {
        ViewModelProvider(this, viewModelFactory).get(TimeEntryDialogRepository::class.java)
    }

    private var timeEntryViewModel: TimeEntryViewModel? = null
    private var dismissCallback: (() -> Unit)? = null
    private var timeEntryId: Long = 0
    private var actionType: ActionType = ActionType.ADD
    private var displayMode = DisplayMode.CLUSTER

    // pickers data:
    private var dates = mutableListOf<Pair<Date, String>>()
    private var tasks = mutableListOf<Pair<Any, String>>()
    private var durationsMinutes = mutableListOf<Pair<Int, String>>()
    private var durationsHours = mutableListOf<Pair<Int, String>>()

    fun setDismissCallBack(func: () -> Unit): TimeEntryDialog {
        dismissCallback = func
        return this
    }

    fun removeDismissCallBack() {
        dismissCallback = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // restore bundle:
        timeEntryId = arguments?.getLong(TIME_ENTRY_ID) ?: 0
        timeEntryViewModel = if (timeEntryId > 0) {
            actionType = ActionType.EDIT
            timeEntryDialogVM.getTimeEntryViewModel(timeEntryId)?.apply { editing = true }
        } else {
            val taskId = arguments?.getLong(TaskDialog.TASK_ID) ?: 0
            TimeEntryViewModel(this.context?.applicationContext).also {
                if (taskId > 0) {
                    it.task = timeEntryDialogVM.getTaskEntity(taskId)
                } else {
                    it.task = null
                }
                it.startDate = Date()
                it.endDate = Date()
            }
        }

        // copy hours/minutes from selected date:
        if (actionType == ActionType.EDIT) {
            fillData(timeEntryViewModel?.startDate)
        } else {
            fillData()
        }

        displayMode = PrefUtil.getDisplayMode(this.requireContext())
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        // bind ViewModel:
        val itemBinding = DialogTimentryBinding.inflate(inflater, container, false)
        itemBinding.viewmodel = timeEntryViewModel
        initializeUI(itemBinding)
        setDialogPosition()
        return itemBinding.root
    }

    private fun initializeUI(binding: DialogTimentryBinding){
        initializeTasksPicker(binding)
        initializetDatePicker(binding)
        initializeDurationPickers(binding)
        initializeStartDatePickers(binding)
        initializeListeners(binding)
    }

    private fun initializeTasksPicker(binding: DialogTimentryBinding){
        if (tasks.isNotEmpty()) {
            binding.taskPicker.itemAlign = ALIGN_LEFT
            binding.taskPicker.isCurved = true
            binding.taskPicker.selectedItemTextColor = Color.parseColor("#eceff1")
            binding.taskPicker.data = tasks.map { it.second }
            tasks.forEachIndexed { index, task ->
                if ((task.first is TaskEntity) && (task.first as TaskEntity).id == timeEntryViewModel?.task?.id) {
                    binding.taskPicker.selectedItemPosition = index
                }
            }
            binding.taskPicker.setOnItemSelectedListener { _, _, position ->
                if (tasks[position].first is ProjectEntity) {
                    binding.taskPicker.selectedItemPosition = position + 1
                } else if (tasks[position].first is TaskEntity) {
                    timeEntryViewModel?.task = (tasks[position].first as TaskEntity)
                    binding.taskName.text = timeEntryViewModel?.task?.title ?: context?.getString(R.string.no_task)
                }
            }
        } else {
            binding.taskPicker.visibility = View.GONE
            binding.taskRv.visibility = View.GONE
        }
    }

    private fun initializetDatePicker(binding: DialogTimentryBinding){
        binding.datePicker.isCurved = true
        binding.datePicker.data = dates.map { it.second }
        if (actionType == ActionType.EDIT) {
            val position = dates.indexOfFirst { DateTime(it.first).withTimeAtStartOfDay() == DateTime(timeEntryViewModel!!.startDate).withTimeAtStartOfDay() }
            binding.datePicker.selectedItemPosition = position
        } else {
            binding.datePicker.selectedItemPosition = DATE_LIMIT
        }
        binding.datePicker.selectedItemTextColor = Color.parseColor("#eceff1")
        binding.datePicker.setOnItemSelectedListener { _, _, position ->
            timeEntryViewModel?.startDate = dates[position].first
            binding.dateValue.text = dates[position].second
        }
    }

    private fun initializeDurationPickers(binding: DialogTimentryBinding){
        val period = Period(DateTime(timeEntryViewModel!!.startDate), DateTime(timeEntryViewModel!!.endDate))
        timeEntryViewModel!!.endDate = timeEntryViewModel!!.startDate
        timeEntryViewModel?.durationHours = (period.days * 24) + period.hours
        timeEntryViewModel!!.durationMinutes = period.minutes
        setupDurationPicker(binding.durationHoursPicker, durationsHours.map { it.second }, timeEntryViewModel!!.durationHours) { position ->
            timeEntryViewModel?.durationHours = durationsHours[position].first
        }
        setupDurationPicker(binding.durationMinutesPicker, durationsMinutes.map { it.second }, timeEntryViewModel!!.durationMinutes) { position ->
            timeEntryViewModel?.durationMinutes = durationsMinutes[position].first
        }
    }

    private fun initializeStartDatePickers(binding: DialogTimentryBinding){
        when (displayMode) {
            DisplayMode.STARTEND -> {
                val startDateDT = DateTime(timeEntryViewModel?.startDate)
                timeEntryViewModel?.startDateMinutes = startDateDT.minuteOfHour
                timeEntryViewModel?.startDateHours = startDateDT.hourOfDay
                timeEntryViewModel?.isStartEnd = true
                setupDurationPicker(binding.startDateHoursPicker, durationsHours.map { it.second }, timeEntryViewModel!!.startDateHours) { position ->
                    timeEntryViewModel?.startDateHours = durationsHours[position].first
                }
                setupDurationPicker(binding.startDateMinutesPicker, durationsMinutes.map { it.second }, timeEntryViewModel!!.startDateMinutes) { position ->
                    timeEntryViewModel?.startDateMinutes = durationsMinutes[position].first
                }
                binding.startDateRv.visibility = View.VISIBLE
            }
            DisplayMode.CLUSTER -> {
                binding.startDateRv.visibility = View.GONE
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initializeListeners(binding: DialogTimentryBinding){
        binding.surfaceViewT.setOnClickListener {
            activity?.hideKeyboard()
        }
        binding.taskRv.setOnClickListener {
            binding.taskPickerWrapper.visibility = if (binding.taskPickerWrapper.visibility == View.VISIBLE) {
                View.GONE
            } else {
                binding.datePickerWrapper.visibility = View.GONE
                binding.durationPickerWrapper.visibility = View.GONE
                binding.startDatePickerWrapper.visibility = View.GONE
                View.VISIBLE
            }
            hideKeyboard()
            binding.notes.clearFocus()
        }
        binding.dateRv.setOnClickListener {
            binding.datePickerWrapper.visibility = if (binding.datePickerWrapper.visibility == View.VISIBLE) {
                View.GONE
            } else {
                binding.taskPickerWrapper.visibility = View.GONE
                binding.durationPickerWrapper.visibility = View.GONE
                binding.startDatePickerWrapper.visibility = View.GONE
                View.VISIBLE
            }
            hideKeyboard()
            binding.notes.clearFocus()
        }
        binding.durationRv.setOnClickListener {
            binding.durationPickerWrapper.visibility = if (binding.durationPickerWrapper.visibility == View.VISIBLE) {
                View.GONE
            } else {
                binding.taskPickerWrapper.visibility = View.GONE
                binding.datePickerWrapper.visibility = View.GONE
                binding.startDatePickerWrapper.visibility = View.GONE
                View.VISIBLE
            }
            hideKeyboard()
            binding.notes.clearFocus()
        }
        binding.startDateRv.setOnClickListener {
            binding.startDatePickerWrapper.visibility = if (binding.startDatePickerWrapper.visibility == View.VISIBLE) {
                View.GONE
            } else {
                binding.durationPickerWrapper.visibility = View.GONE
                binding.taskPickerWrapper.visibility = View.GONE
                binding.datePickerWrapper.visibility = View.GONE
                View.VISIBLE
            }
            hideKeyboard()
            binding.notes.clearFocus()
        }
        binding.notes.setOnTouchListener { tes, ts ->
            binding.taskPickerWrapper.visibility = View.GONE
            binding.datePickerWrapper.visibility = View.GONE
            binding.durationPickerWrapper.visibility = View.GONE
            binding.startDatePickerWrapper.visibility = View.GONE
            false
        }
        binding.save.setOnClickListener {
            try {
                timeEntryViewModel!!.validate()
                // db update:
                if (actionType == ActionType.ADD) {
                    timeEntryDialogVM.addTimeEntry(timeEntryViewModel!!)
                    Toast.makeText(this.context, context?.getString(R.string.timerecord_added), Toast.LENGTH_LONG).show()
                } else {
                    timeEntryDialogVM.updateTimeEntry(timeEntryViewModel!!)
                    Toast.makeText(this.context, context?.getString(R.string.timerecord_updated), Toast.LENGTH_LONG).show()
                }
                // update parent view by callback:
                dismissCallback?.invoke()
                dialog?.dismiss()
            } catch (taskException: TaskNotSelectedException) {
                Toast.makeText(context, taskException.message, Toast.LENGTH_LONG).show()
            } catch (durationException: DurationNotSelectedException) {
                Toast.makeText(context, durationException.message, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun setupDurationPicker(wheelPicker: WheelPicker, data: List<String>, duration: Int, callBack: (Int) -> Unit) {
        wheelPicker.isCurved = true
        wheelPicker.data = data
        wheelPicker.selectedItemTextColor = Color.parseColor("#eceff1")
        wheelPicker.selectedItemPosition = duration
        wheelPicker.setOnItemSelectedListener { _, _, position ->
            callBack.invoke(position)
        }
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        val params = window!!.attributes
        params.dimAmount = 0.6f
        window.attributes = params
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(width, height)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.setCanceledOnTouchOutside(true)
    }

    private fun setDialogPosition() {
        val window = dialog?.window
        window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL)
        val params = window.attributes
        params.windowAnimations = R.style.DialogSlideBottom
        window.attributes = params
    }

    override fun onDismiss(dialog: DialogInterface) {
        dismissCallback?.invoke()
        removeDismissCallBack()
        super.onDismiss(dialog)
    }

    private fun fillData(selectedDate: Date? = null) {
        fillDates(selectedDate)
        fillTasks()
        fillDurations()
    }

    private fun fillDates(selectedDate: Date? = null) {
        val today = if (selectedDate != null) {
            val selectedDt = DateTime(selectedDate)
            DateTime()
                    .withMillisOfSecond(0)
                    .withSecondOfMinute(selectedDt.secondOfMinute)
                    .withMinuteOfHour(selectedDt.minuteOfHour)
                    .withHourOfDay(selectedDt.hourOfDay)
        } else {
            DateTime()
        }

        // fill previous days:
        (DATE_LIMIT downTo 1).forEach {
            val date = today.minusDays(it)
            dates.add(Pair(date.toDate(), dateFormatter.print(date)))
        }

        // append today:
        dates.add(Pair(today.toDate(), context?.getString(R.string.today) ?: "Today"))
    }

    private fun fillTasks() {
        tasks = timeEntryDialogVM.getTasksData()
    }

    private fun fillDurations() {
        // minutes:
        (0..59).forEach {
            durationsMinutes.add(Pair(it, String.format("%02d", it)))
        }
        // hours:
        (0..23).forEach {
            durationsHours.add(Pair(it, String.format("%02d", it)))
        }
    }

    private var dateFormatter = DateTimeFormat.forPattern("EEEE, d MMM yyyy")

    companion object {
        const val DATE_LIMIT = 2000
        const val TASK_ID = "TASK_ID"
        const val TIME_ENTRY_ID = "TIME_ENTRY_ID"

        fun getInstance(taskId: Long = 0, timeEntryId: Long = 0): TimeEntryDialog {
            val fragment = TimeEntryDialog()
            fragment.arguments = Bundle().apply {
                putLong(TASK_ID, taskId)
                putLong(TIME_ENTRY_ID, timeEntryId)
            }
            return fragment
        }
    }
}
