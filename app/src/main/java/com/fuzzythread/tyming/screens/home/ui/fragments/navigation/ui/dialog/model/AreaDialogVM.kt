package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model

import android.content.Context
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.AreaViewModel
import com.fuzzythread.tyming.global.db.GeneralRepository
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.AreaEntity


class AreaDialogVM(
        private val areaDao: AreaDao,
        projectDao: ProjectDao,
        taskDao: TaskDao,
        timeEntryDao: TimeEntryDao,
        private val context: Context
) : GeneralRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {

    fun updateArea(areaVM: AreaViewModel) {
        areaDao.update(
                areaDao.get(areaVM.id)!!.apply {
                    notes = areaVM.notes.capitalize()
                    title = areaVM.title.capitalize()
                    color = areaVM.color
                }
        )
    }

    fun addArea(areaViewModel: AreaViewModel) {
        areaDao.add(
                AreaEntity(
                        notes = areaViewModel.notes.capitalize(),
                        title = areaViewModel.title.capitalize(),
                        color = areaViewModel.color
                )
        )
    }

    fun getAreaViewModel(areaId: Long): AreaViewModel? {
        areaDao.get(areaId)?.also {
            return AreaViewModel(context).apply {
                id = it.id
                notes = it.notes.capitalize()
                title = it.title.capitalize()
                color = if (it.color.isNotEmpty()) it.color else "#1e88e5"
            }
        }
        return null
    }
}
