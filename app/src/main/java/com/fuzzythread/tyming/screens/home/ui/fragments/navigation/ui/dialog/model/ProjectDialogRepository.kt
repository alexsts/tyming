package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model

import android.content.Context
import com.fuzzythread.tyming.global.db.GeneralRepository
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel

class ProjectDialogRepository(
        areaDao: AreaDao,
        private val projectDao: ProjectDao,
        taskDao: TaskDao,
        timeEntryDao: TimeEntryDao,
        private val context: Context
) : GeneralRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {

    fun addProject(projectViewModel: ProjectViewModel) {
        projectDao.add(
                ProjectEntity(
                        notes = projectViewModel.notes.capitalize(),
                        title = projectViewModel.title.capitalize(),
                        color = projectViewModel.color,
                        deadlineDate = projectViewModel.deadlineDate,
                        areaId = projectViewModel.area?.id ?: 0,
                        trackProgress = projectViewModel.trackProgress
                )
        )
    }

    fun updateProject(projectViewModel: ProjectViewModel) {
        projectDao.update(
                projectDao.get(projectViewModel.id)!!.apply {
                    notes = projectViewModel.notes.capitalize()
                    title = projectViewModel.title.capitalize()
                    color = projectViewModel.color
                    deadlineDate = projectViewModel.deadlineDate
                    area.targetId = projectViewModel.area?.id ?: 0
                    trackProgress = projectViewModel.trackProgress
                }
        )
    }
}
