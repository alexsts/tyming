package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model

import android.content.Context
import com.fuzzythread.tyming.global.db.GeneralRepository
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.global.utils.other.ParentType
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TaskViewModel
import java.util.*

class TaskDialogVM(
        areaDao: AreaDao,
        projectDao: ProjectDao,
        private val taskDao: TaskDao,
        timeEntryDao: TimeEntryDao,
        context: Context
) : GeneralRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {
    fun addTask(taskViewModel: TaskViewModel) {
        taskDao.add(TaskEntity(
                createdDate = Date(),
                notes = taskViewModel.notes.capitalize(),
                title = taskViewModel.title.capitalize(),
                deadlineDate = taskViewModel.deadlineDate,
                projectId = taskViewModel.parentId
        ).apply {
            when (taskViewModel.parentType) {
                ParentType.PROJECT -> {
                    project.targetId = taskViewModel.parentId
                }
                ParentType.NONE -> {
                    project.targetId = 0
                }

                ParentType.AREA -> {}
            }
        })
    }
}
