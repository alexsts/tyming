package com.fuzzythread.tyming.screens.journal.model

import android.content.Context
import com.fuzzythread.tyming.global.db.GeneralRepository
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.utils.other.DisplayMode
import com.fuzzythread.tyming.global.utils.other.SortBy
import com.fuzzythread.tyming.global.utils.other.TimeFrame
import com.fuzzythread.tyming.global.utils.time.clearTime
import com.fuzzythread.tyming.global.utils.time.formatTimeToDouble
import com.fuzzythread.tyming.global.utils.time.formatTimeToString
import org.joda.time.DateTime
import org.joda.time.Period
import java.util.*

class JournalRepository(
        areaDao: AreaDao,
        projectDao: ProjectDao,
        taskDao: TaskDao,
        private val timeEntryDao: TimeEntryDao,
        private val context: Context
) : GeneralRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {

    fun getJournalData(date: DateTime, timeFrame: TimeFrame, sortBy: SortBy, displayMode: DisplayMode): JournalData {
        val today = DateTime()
        val firstDay: DateTime
        val timeLineItems = mutableListOf<TimeLineItem<*>>()
        val lastDayId: Int
        val weekDays: String

        if (timeFrame == TimeFrame.WEEK) {
            firstDay = date.withDayOfWeek(1)
            lastDayId = 6
            weekDays = "${firstDay.toString("MMM dd")} - ${date.withDayOfWeek(7).toString("MMM dd")}".toUpperCase(Locale.ROOT)
        } else {
            firstDay = date.withDayOfMonth(1)
            lastDayId = date.dayOfMonth().maximumValue - 1
            weekDays = firstDay.toString("MMMM").toUpperCase(Locale.ROOT)
        }

        when (sortBy) {
            SortBy.DESC -> {
                for (dayId in lastDayId downTo 0) {
                    val day = firstDay.plusDays(dayId)
                    timeLineItems.addAll(getDayTimeLineItems(day, today, displayMode))
                }
            }
            SortBy.ASC -> {
                for (dayId in 0 until lastDayId) {
                    val day = firstDay.plusDays(dayId)
                    timeLineItems.addAll(getDayTimeLineItems(day, today, displayMode))
                }
            }

            SortBy.CUSTOM -> {}
        }

        return JournalData(
                timelineItems = timeLineItems,
                timeframeDays = weekDays
        )
    }

    private fun getDayTimeLineItems(day: DateTime, today: DateTime, displayMode: DisplayMode): MutableList<TimeLineItem<*>> {
        val dayTimeLineItems = mutableListOf<TimeLineItem<*>>()
        val timeEntriesByDay = timeEntryDao.getTimeEntriesByDay(day, isArchived = false)
        val combinedTimeEntries = combineTimeEntriesByTask(timeEntriesByDay, displayMode)

        if (combinedTimeEntries.isNotEmpty()) {
            val dayTimeTracked = combinedTimeEntries.sumByDouble { formatTimeToDouble(it.timeTracked.withSeconds(0)) }

            val dayTimeLineItem = createTimelineItemFromDay(day, today, dayTimeTracked)
            dayTimeLineItems.add(dayTimeLineItem)

            val timeEntriesTimeLineItems = createTimelineItemsFromTimeEntries(combinedTimeEntries)
            dayTimeLineItems.addAll(timeEntriesTimeLineItems)
        }

        return dayTimeLineItems
    }

    private fun createTimelineItemsFromTimeEntries(combinedTimeEntries: MutableList<CombinedTimeEntryViewModel>): List<TimeLineItem<*>> {
        val timeLineItems = mutableListOf<TimeLineItem<CombinedTimeEntryViewModel>>()
        combinedTimeEntries.forEach { timeEntryVM ->
            try {
                if (timeEntryVM.timeTracked.toStandardDuration().standardMinutes > 0) {
                    timeEntryVM.apply {
                        val timeEntryTimeLineItem = TimeLineItem(timeEntryVM).apply {
                            id = timeEntryVM.id
                            title = timeEntryVM.taskName
                            timeTrackedFormatted = timeEntryVM.timeTrackedFormatted
                            color = timeEntryVM.taskParentColor
                            notes = timeEntryVM.notes.trim()
                        }
                        timeLineItems.add(timeEntryTimeLineItem)
                    }
                }
            } catch (ex: UnsupportedOperationException) {
                println(ex)
            }
        }
        return timeLineItems
    }

    private fun createTimelineItemFromDay(day: DateTime, today: DateTime, timeTracked: Double): TimeLineItem<DayViewModel> {
        val dayVM = DayViewModel(context).apply {
            this.today = day.clearTime() == today.clearTime()
            this.weekDay = day.toString("EEE dd MMM")
            this.timeTrackedFormatted = formatTimeToString(timeTracked)
        }

        return TimeLineItem(dayVM).apply {
            id = day.dayOfYear.toLong()
            timeTrackedFormatted = dayVM.timeTrackedFormatted
            weekDay = dayVM.weekDay
        }
    }

    private fun createCombinedViewModel(timeEntry: TimeEntryEntity, mode: DisplayMode): CombinedTimeEntryViewModel {
        val taskEntity = getTaskEntity(timeEntry.task.targetId)
        return CombinedTimeEntryViewModel(context).apply {
            id = timeEntry.id
            notes = timeEntry.notes.trim()
            displayMode = mode
            timeTracked = Period(DateTime(timeEntry.startDate), DateTime(timeEntry.endDate))
            startDate = DateTime(timeEntry.startDate)
            endDate = DateTime(timeEntry.endDate)
            task = taskEntity
            taskParent = if (taskEntity?.project?.targetId ?: 0 > 0) {
                getProjectEntity(taskEntity!!.project.targetId)
            } else {
                null
            }
        }
    }

    private fun combineTimeEntriesByTask(timeEntriesByDay: MutableList<TimeEntryEntity>, displayMode: DisplayMode): MutableList<CombinedTimeEntryViewModel> {
        val combinedTimeEntries = mutableListOf<CombinedTimeEntryViewModel>()

        when (displayMode) {
            DisplayMode.CLUSTER -> {
                val taskTimeEntriesMap = timeEntriesByDay
                        .filter { it.task.targetId != 0L }
                        .map { createCombinedViewModel(it, displayMode) }
                        .groupBy { it.task?.id }

                taskTimeEntriesMap.forEach { task ->
                    val taskTimeEntries = task.value
                    val timeEntryVM = taskTimeEntries[0]
                    timeEntryVM.timeEntriesIds.add(timeEntryVM.id)

                    // combine task time records (sum times, collect ids):
                    taskTimeEntries.forEachIndexed { index, combinedTimeEntryViewModel ->
                        if (index > 0) {
                            timeEntryVM.timeEntriesIds.add(combinedTimeEntryViewModel.id)
                            timeEntryVM.timeTracked = timeEntryVM.timeTracked.plus(combinedTimeEntryViewModel.timeTracked.withSeconds(0))
                        }
                    }
                    combinedTimeEntries.add(timeEntryVM)
                }
                combinedTimeEntries.sortByDescending { it.timeTracked.toStandardDuration() }
            }
            DisplayMode.STARTEND -> {
                val timeEntries = timeEntriesByDay
                        .filter { it.task.targetId != 0L }
                        .map { createCombinedViewModel(it, displayMode) }

                combinedTimeEntries.addAll(timeEntries)
                combinedTimeEntries.sortBy { it.startDate }
            }
        }

        return combinedTimeEntries
    }

    fun removeTimeLineItem(deletedItem: TimeLineItem<*>) {
        val combinedTimeEntry = deletedItem.getItemEntity() as CombinedTimeEntryViewModel
        timeEntryDao.deleteByIds(combinedTimeEntry.timeEntriesIds)
    }

    fun combineTimeEntries(combinedTimeEntryVM: CombinedTimeEntryViewModel) {
        timeEntryDao.combineTimeEntry(combinedTimeEntryVM)
    }

    data class JournalData(val timelineItems: MutableList<TimeLineItem<*>>, val timeframeDays: String)
}

