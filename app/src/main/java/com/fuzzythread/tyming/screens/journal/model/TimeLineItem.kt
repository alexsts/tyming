package com.fuzzythread.tyming.screens.journal.model

import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable
import java.util.*

class TimeLineItem<T>(
        private var entity: T
) : IAdapterComparable<TimeLineItem<*>> {

    var id: Long = 0
    var title: String = ""
    var notes: String = ""
    var time: String = ""
    var timeTrackedFormatted: String = ""
    var weekDay: String = ""
    var color: String = ""
    var date: String = ""
    var trackable: Boolean = false
    var deadlineDate: Date? = null
    var notesLength: Int = 0
        get() {
            return notes.length
        }

    private var itemType: Int = 0

    init {
        val itemType = when (entity) {
            is CombinedTimeEntryViewModel -> TIMEENTRY
            is DayViewModel -> DAY
            else -> -1
        }
        this.itemType = itemType
    }

    fun getItemType(): Int {
        return itemType
    }

    fun getItemEntity(): T {
        return entity
    }

    companion object {
        val TIMEENTRY = 1
        val DAY = 2
    }

    override fun areItemsTheSame(itemToCompare: TimeLineItem<*>): Boolean {
        return itemToCompare.id == id &&
                itemToCompare.itemType == itemType
    }

    override fun areContentsTheSame(itemToCompare: TimeLineItem<*>): Boolean {
        return itemToCompare.id == id &&
                itemToCompare.title == title &&
                itemToCompare.color == color &&
                itemToCompare.date == date &&
                itemToCompare.notes == notes &&
                itemToCompare.deadlineDate == deadlineDate &&
                itemToCompare.trackable == trackable &&
                itemToCompare.itemType == itemType &&
                itemToCompare.timeTrackedFormatted == timeTrackedFormatted &&
                itemToCompare.weekDay == weekDay &&
                itemToCompare.notesLength == notesLength
    }
}
