package com.fuzzythread.tyming.screens.journal.ui.dialog

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.DialogFiltersBinding
import com.fuzzythread.tyming.global.utils.other.*
import com.fuzzythread.tyming.global.utils.ui.convertDpToPx

class FiltersDialog : DialogFragment() {

    private var activityType: ActivityType? = ActivityType.STATS
    private var dismissCallback: (() -> Unit)? = null
    private var filtersDialogVM: FiltersDialogVM? = null

    fun setDismissCallBack(func: () -> Unit) {
        dismissCallback = func
    }

    private fun removeDismissCallBack() {
        dismissCallback = null
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val itemBinding = DialogFiltersBinding.inflate(inflater, container, false)
        itemBinding.viewmodel = filtersDialogVM

        initializeListeners(itemBinding)
        setDialogPosition()
        return itemBinding.root
    }

    private fun initializeListeners(binding: DialogFiltersBinding) {
        binding.weekly.setOnClickListener {
            filtersDialogVM?.timeFrame = TimeFrame.WEEK
            dismiss()
        }

        binding.monthly.setOnClickListener {
            filtersDialogVM?.timeFrame = TimeFrame.MONTH
            dismiss()
        }

        binding.sortByDesc.setOnClickListener {
            filtersDialogVM?.sortBy = SortBy.DESC
            dismiss()
        }

        binding.sortByAsc.setOnClickListener {
            filtersDialogVM?.sortBy = SortBy.ASC
            dismiss()
        }

        binding.sortByCustom.setOnClickListener {
            filtersDialogVM?.sortBy = SortBy.CUSTOM
            dismiss()
        }

        binding.modeStartEnd.setOnClickListener {
            filtersDialogVM?.displayMode = DisplayMode.STARTEND
            dismiss()
        }

        binding.modeCluster.setOnClickListener {
            filtersDialogVM?.displayMode = DisplayMode.CLUSTER
            dismiss()
        }

        binding.modeStartEnd.setOnClickListener {
            filtersDialogVM?.displayMode = DisplayMode.STARTEND
            dismiss()
        }

        binding.modeCluster.setOnClickListener {
            filtersDialogVM?.displayMode = DisplayMode.CLUSTER
            dismiss()
        }

        binding.tasksVisible.setOnClickListener {
            filtersDialogVM?.completedTasks = DisplayCompletedTasks.VISIBLE
            dismiss()
        }

        binding.tasksHidden.setOnClickListener {
            filtersDialogVM?.completedTasks = DisplayCompletedTasks.HIDDEN
            dismiss()
        }

        when (activityType) {
            ActivityType.STATS -> {
                binding.periodContainer.visibility = View.VISIBLE
            }
            ActivityType.JOURNAL -> {
                binding.periodContainer.visibility = View.VISIBLE
                binding.sortByContainer.visibility = View.VISIBLE
                binding.displayModeContainer.visibility = View.VISIBLE
            }
            ActivityType.PROJECTS -> {
                binding.sortByContainer.visibility = View.VISIBLE
                binding.completedTasksContainer.visibility = View.VISIBLE
                binding.customOrderContainer.visibility = View.VISIBLE
            }

            null -> {}
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        filtersDialogVM = FiltersDialogVM()

        // restore bundle:
        activityType = ActivityType.valueOf(arguments?.getString(ACTIVITY_TYPE) ?: "JOURNAL")

        // prefUtil get all properties:
        filtersDialogVM?.apply {
            when (activityType) {
                ActivityType.JOURNAL -> {
                    timeFrame = PrefUtil.getTimeFrame(this@FiltersDialog.requireContext())
                    sortBy = PrefUtil.getSortByJournal(this@FiltersDialog.requireContext())
                    displayMode = PrefUtil.getDisplayMode(this@FiltersDialog.requireContext())
                }
                ActivityType.PROJECTS -> {
                    sortBy = PrefUtil.getSortByProjects(this@FiltersDialog.requireContext())
                    completedTasks = PrefUtil.getDisplayCompletedTasksVisible(this@FiltersDialog.requireContext())
                }
                ActivityType.STATS -> {
                    timeFrame = PrefUtil.getTimeFrame(this@FiltersDialog.requireContext())
                }

                null -> {}
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        val params = window!!.attributes
        params.dimAmount = 0.6f
        window.attributes = params
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(width, height)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.setCanceledOnTouchOutside(true)
    }

    private fun saveFilters() {
        // prefUtil get all properties:
        filtersDialogVM?.apply {
            when (activityType) {
                ActivityType.JOURNAL -> {
                    PrefUtil.setSortByJournal(sortBy, this@FiltersDialog.requireContext())
                    PrefUtil.setDisplayMode(displayMode, this@FiltersDialog.requireContext())
                    PrefUtil.setTimeFrame(timeFrame, this@FiltersDialog.requireContext())
                }
                ActivityType.PROJECTS -> {
                    PrefUtil.setSortByProjects(sortBy, this@FiltersDialog.requireContext())
                    PrefUtil.setDisplayCompletedTasksVisible(completedTasks, this@FiltersDialog.requireContext())
                }
                ActivityType.STATS -> {
                    PrefUtil.setTimeFrame(timeFrame, this@FiltersDialog.requireContext())
                }

                null -> {}
            }
        }
    }

    private fun setDialogPosition() {
        val window = dialog?.window
        window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM)
        val params = window.attributes
        params.y = convertDpToPx(this.requireContext(), 8)
        params.windowAnimations = R.style.DialogSlideBottom
        window.attributes = params
    }

    override fun onDismiss(dialog: DialogInterface) {
        saveFilters()
        dismissCallback?.invoke()
        removeDismissCallBack()
        super.onDismiss(dialog)
    }

    companion object {
        const val ACTIVITY_TYPE = "ACTIVITY_TYPE"

        fun getInstance(activityType: ActivityType): FiltersDialog {
            val fragment = FiltersDialog()
            fragment.arguments = Bundle().apply {
                putString(ACTIVITY_TYPE, activityType.toString())
            }
            return fragment
        }
    }
}
