package com.fuzzythread.tyming.screens.launch.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.fuzzythread.tyming.databinding.FragmentPermissionsBinding
import com.fuzzythread.tyming.global.utils.extensions.allPermissionsGranted
import com.fuzzythread.tyming.screens.launch.LaunchActivity

@Suppress("DEPRECATION")
class PermissionsFragment : Fragment() {

    @SuppressLint("DefaultLocale")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentPermissionsBinding.inflate(inflater, container, false)
        initializeUI(binding)
        return binding.root
    }

    // region UI
    private fun initializeUI(binding: FragmentPermissionsBinding) {
        binding.enablePermissions.setOnClickListener {
            handlePermissions()
        }
    }
    // endregion

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        // check if user granted permission if not reopen dialog:
        handlePermissions()
    }

    private fun handlePermissions() {
        val context: Context = this.context ?: return
        if (!allPermissionsGranted(context)) {
            requestPermissions()
        } else {
            (activity as LaunchActivity).showSignUpFragment()
        }
    }

    private fun requestPermissions() {
        val permissions = arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET
        )
        requestPermissions(permissions, PERMISSIONS_CODE)
    }

    companion object {
        const val TAG = "PERMISSIONS_FRAGMENT"
        const val PERMISSIONS_CODE = 8
        fun newInstance(): PermissionsFragment {
            val fragment = PermissionsFragment()
            val args = Bundle()
            args.putString(TAG, "TAG")
            fragment.arguments = args
            return fragment
        }
    }
}
