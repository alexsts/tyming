package com.fuzzythread.tyming.screens.launch.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.fuzzythread.tyming.databinding.FragmentSignUpBinding
import com.fuzzythread.tyming.screens.launch.LaunchActivity

@Suppress("DEPRECATION")
class SignUpFragment : Fragment() {

    @SuppressLint("DefaultLocale")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentSignUpBinding.inflate(inflater, container, false)
        initializeUI(binding)
        return binding.root
    }

    // region UI
    private fun initializeUI(binding: FragmentSignUpBinding) {
        binding.startNew.setOnClickListener {
            (activity as LaunchActivity).startNewUserJourney()
        }

        binding.alreadyHaveAccount.setOnClickListener {
            (activity as LaunchActivity).signIn()
        }
    }
    // endregion

    companion object {
        const val TAG = "SIGN_UP_FRAGMENT"
        fun newInstance(): SignUpFragment {
            val fragment = SignUpFragment()
            val args = Bundle()
            args.putString(TAG, "TAG")
            fragment.arguments = args
            return fragment
        }
    }
}
