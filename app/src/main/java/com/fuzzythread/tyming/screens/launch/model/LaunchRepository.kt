package com.fuzzythread.tyming.screens.launch.model

import android.content.Context
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.db.GeneralRepository
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.AreaEntity
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import org.joda.time.*
import java.util.*
import kotlin.math.abs

class LaunchRepository(
        private val areaDao: AreaDao,
        private val projectDao: ProjectDao,
        private val taskDao: TaskDao,
        private val timeEntryDao: TimeEntryDao,
        private val context: Context
) : GeneralRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {

    private val random = Random()

    fun generateTutorialData() {
        val dateToday = LocalDateTime.now()

        // areas:
        areaDao.add(AreaEntity(title = context.getString(R.string.first_area), color = "#4caf50", notes = "", createdDate = dateToday.toDate()))

        // projects
        projectDao.add(ProjectEntity(title = context.getString(R.string.my_first_project), notes = context.getString(R.string.this_project_has_deadline), color = "#1e88e5", deadlineDate = dateToday.plusWeeks(2).toDate(), createdDate = dateToday.toDate(), areaId = 0, trackProgress = true))
        projectDao.add(ProjectEntity(title = context.getString(R.string.daily_routine), notes = context.getString(R.string.this_is_routine_project), color = "#ffeb3b", deadlineDate = null, createdDate = dateToday.toDate(), areaId = 1L, trackProgress = false))
        projectDao.add(ProjectEntity(title = context.getString(R.string.study), notes = context.getString(R.string.project_notes), color = "#aa00ff", deadlineDate = null, createdDate = dateToday.toDate(), areaId = 1L, trackProgress = false))

        // tasks
        taskDao.add(TaskEntity(title = context.getString(R.string.first_task), notes = "", completedDate = null, createdDate = dateToday.toDate(), projectId = 1L))
        fillTimeEntries(1L, 6, dateToday)

        taskDao.add(TaskEntity(title = context.getString(R.string.completed_task_1), notes = "", completedDate = dateToday.minusWeeks(4).toDate(), createdDate = dateToday.toDate(), projectId = 1L))
        taskDao.add(TaskEntity(title = context.getString(R.string.completed_task_2), notes = "", completedDate = dateToday.minusWeeks(4).toDate(), createdDate = dateToday.toDate(), projectId = 1L))
        taskDao.add(TaskEntity(title = context.getString(R.string.clean_house), notes = "", completedDate = null, createdDate = dateToday.toDate(), projectId = 2L))
        fillTimeEntries(4L, 6, dateToday)

        taskDao.add(TaskEntity(title = context.getString(R.string.cooking), notes = "", completedDate = null, createdDate = dateToday.toDate(), projectId = 2L))
        timeEntryDao.add(TimeEntryEntity(notes = context.getString(R.string.simple_notes), taskId = 5L, startDate = dateToday.toDate(), endDate = dateToday.plusHours(2).toDate()))
        fillTimeEntries(5L, 8, dateToday)

        taskDao.add(TaskEntity(title = context.getString(R.string.piano), notes = "", completedDate = null, createdDate = dateToday.toDate(), projectId = 3L))
        timeEntryDao.add(TimeEntryEntity(notes = context.getString(R.string.time_records_can_have_notes), taskId = 6L, startDate = dateToday.toDate(), endDate = dateToday.plusHours(3).toDate()))
        fillTimeEntries(6L, 8, dateToday)

        taskDao.add(TaskEntity(title = context.getString(R.string.learn_english), notes = "", completedDate = null, createdDate = dateToday.toDate(), projectId = 3L))
        fillTimeEntries(7L, 10, dateToday)
    }


    private fun fillTimeEntries(taskId: Long, amount: Int, dateToday: LocalDateTime) {
        repeat(amount) {
            val randStartDate = dateToday.minusDays(getRandomMonthDay())
            val randEndDate = randStartDate.plusHours(getRandomHours())
            timeEntryDao.add(TimeEntryEntity(notes = "", taskId = taskId, startDate = randStartDate.toDate(), endDate = randEndDate.toDate()))
        }
    }

    private fun getRandomMonthDay(): Int {
        return abs(rand(2, 30))
    }

    private fun getRandomHours(): Int {
        return rand(1, 3)
    }

    private fun rand(from: Int, to: Int): Int {
        return random.nextInt(to - from) + from
    }
}

