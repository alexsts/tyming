package com.fuzzythread.tyming.screens.premium

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.ActivityPremiumBinding
import com.fuzzythread.tyming.global.TymingActivity

class PremiumActivity : TymingActivity() {

    // region Android hooks
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityPremiumBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTopBarPadding(binding.topBar)
        initialize(binding)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
    // endregion

    // region UI
    fun initialize(binding: ActivityPremiumBinding) {
        initializeUI(binding)
    }

    fun initializeUI(binding: ActivityPremiumBinding) {
        binding.backBtn.setOnClickListener {
            finish()
        }

        binding.upgrade.setOnClickListener {
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.fuzzythread.tyming.premium")))
            } catch (e: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.fuzzythread.tyming.premium")))
            }
        }
    }
    // endregion
}
