package com.fuzzythread.tyming.screens.search.model

import android.content.Context
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.AreaViewModel
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.utils.other.SortBy
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.HomeFragmentRepository
import com.fuzzythread.tyming.screens.home.model.NavigationItem
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel

class SearchRepository(
        private val areaDao: AreaDao,
        private val projectDao: ProjectDao,
        taskDao: TaskDao,
        timeEntryDao: TimeEntryDao,
        private val context: Context
) : HomeFragmentRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {

    fun searchNavigationData(query: String, sortBy: SortBy): MutableList<NavigationItem<*>> {
        val list: MutableList<NavigationItem<*>> = mutableListOf()
        val singularProjectsNavItems = createProjectsNavigationItems(projectDao.getSingularProjects(isArchived = false, sortBy = sortBy), query, sortBy)

        // get areas and their projects
        val areaNavItems = mutableListOf<NavigationItem<*>>()
        areaDao.getAllSorted(sortBy = sortBy).forEach {
            val areaVM = AreaViewModel(it, context)
            val projectsSorted = areaDao.getAreaProjectsSorted(it.id, isArchived = false, sortBy = sortBy)
            val areaProjectsNavItems = createProjectsNavigationItems(projectsSorted, sortBy = sortBy)
            val areaProjectsNavItemsQueried = createProjectsNavigationItems(projectsSorted, query, sortBy)

            if (!areaVM.isArchived || areaProjectsNavItems.size > 0) {
                val areaMatchesQuery = queryMatches(areaVM.title, query)

                if (areaProjectsNavItemsQueried.isNotEmpty()) {
                    // there are project or their tasks matching query for this area
                    areaNavItems.add(createAreaNavItem(areaVM, projectsSorted.size))
                    areaNavItems.addAll(areaProjectsNavItemsQueried)
                } else if (areaMatchesQuery) {
                    // area matches query
                    areaNavItems.add(createAreaNavItem(areaVM, projectsSorted.size))
                    // add projects if not collapsed:
                    if (!areaVM.collapsed) {
                        areaNavItems.addAll(areaProjectsNavItems)
                    }
                }
            }
        }
        // order: singular projects, areas
        list.addAll(singularProjectsNavItems)
        list.addAll(areaNavItems)
        return list
    }

    private fun createProjectsNavigationItems(projects: MutableList<ProjectEntity>, query: String, sortBy: SortBy = SortBy.DESC): MutableList<NavigationItem<*>> {
        val projectNavItems = mutableListOf<NavigationItem<*>>()
        if (sortBy == SortBy.CUSTOM){
            projects.toViewModels()
                    .forEach { projectNavItems.addAll(if (query.isNotEmpty()) {
                        createProjectWithTasksNavigationItem(it, query, sortBy).navItems
                    } else {
                        createProjectWithTasksNavigationItem(it, sortBy = sortBy).navItems
                    })  }
        } else {
            projects.toViewModels()
                    .filter { !it.completed }
                    .forEach { projectNavItems.addAll(if (query.isNotEmpty()) {
                            createProjectWithTasksNavigationItem(it, query, sortBy).navItems
                        } else {
                            createProjectWithTasksNavigationItem(it, sortBy = sortBy).navItems
                        }) }
            // put completed projects at the end:
            projects.toViewModels()
                    .filter { it.completed }
                    .sortedByDescending { it.completedDate }
                    .forEach { projectNavItems.addAll(if (query.isNotEmpty()) {
                            createProjectWithTasksNavigationItem(it, query, sortBy).navItems
                        } else {
                            createProjectWithTasksNavigationItem(it, sortBy = sortBy).navItems
                        }) }
        }
        return projectNavItems
    }

    private fun createProjectWithTasksNavigationItem(projectVM: ProjectViewModel, query: String, sortBy: SortBy): ProjectNavigationData {
        val projectTasksNavItems = mutableListOf<NavigationItem<*>>()

        val projectTasksNavigationItems = getProjectTasksNavigationItems(projectVM, sortBy = sortBy)
        val projectMatchesQuery = queryMatches(projectVM.title, query)
        val tasksMatchingQuery = projectTasksNavigationItems.filter { queryMatches(it.title, query) }

        if (tasksMatchingQuery.isNotEmpty()) {
            // there are tasks matching query for this project
            projectTasksNavItems.add(createProjectNavItem(projectVM))
            projectTasksNavItems.addAll(tasksMatchingQuery)
        } else if (projectMatchesQuery) {
            // project matches query
            projectTasksNavItems.add(createProjectNavItem(projectVM))
            // add tasks if not collapsed:
            if (!projectVM.collapsed) {
                projectTasksNavItems.addAll(projectTasksNavigationItems)
            }
        }

        return ProjectNavigationData(navItems = projectTasksNavItems, isCompleted = projectVM.completed)
    }

    private fun queryMatches(title: String, query: String): Boolean {
        return title.toLowerCase().contains(query.toLowerCase())
    }

}

