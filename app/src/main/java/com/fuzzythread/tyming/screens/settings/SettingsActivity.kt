package com.fuzzythread.tyming.screens.settings

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.ColorInt
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.ActivitySettingsBinding
import com.fuzzythread.tyming.global.TymingActivity
import com.fuzzythread.tyming.global.services.syncing.Synchronizator
import com.fuzzythread.tyming.global.utils.other.Constants
import com.fuzzythread.tyming.global.utils.other.Constants.Companion.RC_SIGN_IN
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import com.fuzzythread.tyming.screens.premium.PremiumActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import de.psdev.licensesdialog.LicensesDialog
import de.psdev.licensesdialog.licenses.ApacheSoftwareLicense20
import de.psdev.licensesdialog.licenses.EclipsePublicLicense10
import de.psdev.licensesdialog.model.Notice
import de.psdev.licensesdialog.model.Notices
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import timber.log.Timber
import javax.inject.Inject

class SettingsActivity : TymingActivity() {

    @Inject
    lateinit var synchronizator: Synchronizator
    private val dateFormatter: DateTimeFormatter = DateTimeFormat.forPattern("d MMMM yyyy '-' kk:mm")
    private var googleSignInClient: GoogleSignInClient? = null
    private var auth: FirebaseAuth? = null
    private lateinit var binding: ActivitySettingsBinding

    // region Android hooks
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTopBarPadding(binding.topBar)
        initializeUI()
        initAuth()
        updateUIWithValues()
    }

    override fun onDestroy() {
        synchronizator.removeCallback()
        super.onDestroy()
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    // endregion

    // region Data
    fun refresh() {
        updateUIWithValues()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)!!
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                Timber.tag(Constants.APP_TAG).e("Google sign in failed")
                refresh()
            }
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth?.signInWithCredential(credential)?.addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                Timber.tag(Constants.APP_TAG).d("SignInWithCredential:success")
                initializeAccount()
                refresh()
            } else {
                Timber.tag(Constants.APP_TAG).d("SignInWithCredential:failure")
                Toast.makeText(this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show()
                refresh()
            }
        }
    }

    private fun initializeAccount() {
        auth?.currentUser?.email?.also { currentEmail ->
            val previousEmail = PrefUtil.getUserEmail(this)
            PrefUtil.setUserEmail(currentEmail, this)
            when (previousEmail) {
                null -> {
                    Timber.tag(Constants.APP_TAG).d("Local user using syncing for first time")
                }
                currentEmail -> {
                    Timber.tag(Constants.APP_TAG).d("Local user used syncing before with the same credentials")
                }
                else -> {
                    Timber.tag(Constants.APP_TAG).d("Different user sign in")
                    synchronizator.syncDifferentAccount()
                }
            }
        }
    }

    private fun signIn() {
        val signInIntent = googleSignInClient?.signInIntent
        startActivityForResult(signInIntent!!, RC_SIGN_IN)
    }

    private fun signOut() {
        // Firebase sign out
        auth?.signOut()

        // Google sign out
        googleSignInClient?.signOut()?.addOnCompleteListener(this) {
            refresh()
        }
    }

    private fun initAuth() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)
        auth = FirebaseAuth.getInstance()
    }
    // endregion

    // region UI
    private fun initializeUI() {
        binding.backBtn.setOnClickListener {
            finish()
        }

        binding.openLicensesLabel.setOnClickListener {
            showLicensesDialog(this)
        }

        binding.privacyNoticeLabel.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://pastebin.com/uEBtjknr"))
            startActivity(browserIntent)
        }

        binding.enableMultipleTimers.setOnClickListener {
            PrefUtil.setMultipleTimersEnabled(binding.enableMultipleTimers.isChecked, this)
        }

        binding.enableAutoSync.setOnClickListener {
            PrefUtil.setAutoSync(binding.enableAutoSync.isChecked, this)
        }

        binding.enableDeadlineNotificationsSwitch.setOnClickListener {
            PrefUtil.setShowDeadlineNotifications(binding.enableDeadlineNotificationsSwitch.isChecked, this)
        }

        binding.logout.setOnClickListener {
            signOut()
        }

        binding.removeAds?.setOnClickListener {
            with(it.context) {
                val intent = Intent(this, PremiumActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }
        }

        binding.syncNowBtn.setOnClickListener {
            synchronizator.sync()
        }

        binding.signUpGoogle.setOnClickListener {
            signIn()
        }

        binding.rateUs.setOnClickListener {
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
            } catch (e: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
            }
        }
    }

    private fun updateUIWithValues() {
        binding.enableAutoSync.isChecked = PrefUtil.getIsAutoSyncEnabled(this)
        binding.enableMultipleTimers.isChecked = PrefUtil.getMultipleTimersEnabled(this)
        binding.enableDeadlineNotificationsSwitch.isChecked = PrefUtil.getShowDeadlineNotifications(this)

        if (auth?.currentUser?.uid != null) {
            binding.signUpContainer.visibility = View.GONE
            binding.accountContainer.visibility = View.VISIBLE
            binding.logout.visibility = View.VISIBLE
            updateAccountElement()
        } else {
            binding.accountContainer.visibility = View.GONE
            binding.signUpContainer.visibility = View.VISIBLE
            binding.logout.visibility = View.GONE
        }
    }

    private fun updateAccountElement() {
        binding.userName.text = auth?.currentUser?.email ?: ""
        refreshLastSynced()
        synchronizator.setCallback { isLoading, _ ->
            CoroutineScope(Dispatchers.Default).launch {
                runOnUiThread {
                    if (isLoading) {
                        binding.syncingBar.visibility = View.VISIBLE
                    } else {
                        binding.syncingBar.visibility = View.GONE
                        refreshLastSynced()
                    }
                }
            }
        }
    }

    private fun refreshLastSynced() {
        val lastSyncDate = PrefUtil.getLastSyncDate(this@SettingsActivity)
        if (lastSyncDate != null) {
            binding.lastSynced.text = "${this.getString(R.string.synced)}: ${dateFormatter.print(DateTime(lastSyncDate))}"
        } else {
            binding.lastSynced.text = this.getString(R.string.not_synced_yet)
        }
    }

    private fun showLicensesDialog(context: Context) {
        val notices = Notices()
        notices.addNotice(Notice("MPAndroidChart", "https://github.com/PhilJay/MPAndroidChart", "Copyright 2018 Philipp Jahoda", ApacheSoftwareLicense20()))
        notices.addNotice(Notice("objectbox-java", "https://github.com/objectbox/objectbox-java", "Copyright 2017-2018 ObjectBox Ltd.", ApacheSoftwareLicense20()))
        notices.addNotice(Notice("joda-time-android", "https://github.com/dlew/joda-time-android", "", ApacheSoftwareLicense20()))
        notices.addNotice(Notice("WheelPicker", "https://github.com/AigeStudio/WheelPicker", "Copyright 2015-2017 AigeStudio", ApacheSoftwareLicense20()))
        notices.addNotice(Notice("dagger", "https://github.com/google/dagger", "Copyright 2012 The Dagger Authors", ApacheSoftwareLicense20()))
        notices.addNotice(Notice("timber", "https://github.com/JakeWharton/timber", "Copyright 2013 Jake Wharton", ApacheSoftwareLicense20()))
        notices.addNotice(Notice("colorpicker", "https://github.com/QuadFlask/colorpicker", "Copyright 2014-2017 QuadFlask", ApacheSoftwareLicense20()))
        notices.addNotice(Notice("junit4", "https://github.com/junit-team/junit4", "", EclipsePublicLicense10()))
        notices.addNotice(Notice("Material View Pager Dots Indicator", "https://github.com/tommybuonomo/dotsindicator", "Copyright 2016 Tommy Buonomo", ApacheSoftwareLicense20()))
        notices.addNotice(Notice("Spotlight", "https://github.com/TakuSemba/Spotlight", "Copyright 2017 Taku Semba", ApacheSoftwareLicense20()))

        // set dialog colors
        val formatString = getString(R.string.custom_notices_format_style)
        val pBg: String = getRGBAString(Color.parseColor("#9E9E9E"))
        val bodyBg: String = getRGBAString(Color.parseColor("#424242"))
        val preBg: String = getRGBAString(Color.parseColor("#808080"))
        val liColor = "color: #ffffff"
        val linkColor = "color: #1976D2"
        val style = String.format(formatString, pBg, bodyBg, preBg, liColor, linkColor)

        LicensesDialog.Builder(context)
                .setNotices(notices)
                .setThemeResourceId(R.style.LicensesDialogTheme)
                .setNoticesCssStyle(style)
                .setIncludeOwnLicense(true)
                .build()
                .show()
    }

    private fun getRGBAString(@ColorInt color: Int): String {
        val red: Int = Color.red(color)
        val green: Int = Color.green(color)
        val blue: Int = Color.blue(color)
        val alpha = Color.alpha(color).toFloat() / 255
        return String.format(resources.getString(R.string.rgba_background_format), red, green, blue, alpha)
    }
    // endregion
}
