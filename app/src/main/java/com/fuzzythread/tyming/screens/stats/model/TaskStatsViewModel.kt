package com.fuzzythread.tyming.screens.stats.model

import androidx.databinding.Bindable
import com.fuzzythread.tyming.BR
import com.fuzzythread.tyming.global.ObservableViewModel
import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable
import com.fuzzythread.tyming.global.utils.other.ParentType
import kotlin.math.round

class TaskStatsViewModel : ObservableViewModel(), IAdapterComparable<TaskStatsViewModel> {
    var id: Long = 0

    @get:Bindable
    var titleTask: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }

    @get:Bindable
    var titleProject: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }

    @get:Bindable
    var time: Float = 0.0f
        set(value) {
            field = value
            notifyPropertyChanged(BR.timeFormatted)
        }

    @get:Bindable
    var timeFormatted: String = "00:00"
        get() {
            return String.format("%02d:%02d", time.toInt(), round((time - time.toInt()) * 60).toInt())
        }

    @get:Bindable
    var color: String = "#1e88e5"
        get() {
            if (field.isEmpty()) {
                return "#1e88e5"
            }
            return field
        }
        set(value) {
            field = value
            notifyPropertyChanged(BR.color)
        }

    @get:Bindable
    var type: ParentType = ParentType.PROJECT
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentType)
        }


    override fun areItemsTheSame(itemToCompare: TaskStatsViewModel): Boolean {
        return itemToCompare.id == id && itemToCompare.type == type
    }

    override fun areContentsTheSame(itemToCompare: TaskStatsViewModel): Boolean {
        return itemToCompare.id == id &&
                itemToCompare.type == type &&
                itemToCompare.titleTask == titleTask &&
                itemToCompare.titleProject == titleProject &&
                itemToCompare.color == color &&
                itemToCompare.time == time &&
                itemToCompare.timeFormatted == timeFormatted
    }
}
