package com.fuzzythread.tyming.screens.stats.ui

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.FragmentStatsBinding
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.global.ui.charts.*
import com.fuzzythread.tyming.global.ui.elements.TymingFragment
import com.fuzzythread.tyming.global.utils.other.Constants
import com.fuzzythread.tyming.global.utils.other.TimeFrame
import com.fuzzythread.tyming.global.utils.time.formatTimeToString
import com.fuzzythread.tyming.screens.journal.ui.JournalFragment
import com.fuzzythread.tyming.screens.journal.ui.adapter.FragmentLifecycle
import com.fuzzythread.tyming.screens.stats.model.StatsRepository
import com.fuzzythread.tyming.screens.stats.model.StatsRepository.StatsData
import com.fuzzythread.tyming.screens.stats.ui.adapter.TasksDetailsAdapter
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import org.joda.time.DateTime
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext
import kotlin.math.abs

class StatsFragment : TymingFragment(), CoroutineScope, FragmentLifecycle {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private var nestedScrollView: NestedScrollView? = null
    private val repository: StatsRepository by lazy {
        ViewModelProvider(this, viewModelFactory).get(StatsRepository::class.java)
    }

    var masterJob: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + masterJob

    private var statsTimeFrameLabel: TextView? = null
    private var separator: View? = null
    private var dayTime: TextView? = null
    private var weekDay: TextView? = null
    private var bar: LinearLayout? = null
    private var typeface: Typeface? = null
    private var timeFrameTV: AppCompatTextView? = null
    private var date: DateTime = DateTime()
    private var timeFrame: TimeFrame = TimeFrame.WEEK
    private var dateLabel: String = ""
    private var statsDetailed: ConstraintLayout? = null
    private var statsTop: ConstraintLayout? = null
    private var performanceDecreased: ImageView? = null
    private var performanceIncreased: ImageView? = null
    private var noDataContainer: ConstraintLayout? = null
    private var noDataLabel: AppCompatTextView? = null
    private var performanceTV: AppCompatTextView? = null
    private var averageHoursTV: AppCompatTextView? = null
    private var hoursWorkedTV: AppCompatTextView? = null
    private var statsData: StatsData? = null
    private var chartWeek: BarChart? = null
    private var pieWeek: PieChart? = null
    private var tasksAdapter: TasksDetailsAdapter? = null
    private var tasksRV: RecyclerView? = null

    var isFragmentResumed: Boolean = false
    var isCurrentTimeFrame: Boolean = false
    var isAnimationPlayed: Boolean = false

    // region Android hooks
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)

        // load passed arguments:
        timeFrame = TimeFrame.valueOf(arguments?.getString(JournalFragment.TIME_FRAME) ?: "WEEK")
        date = DateTime(arguments?.getString(JournalFragment.DATE) ?: "")
        dateLabel = arguments?.getString(JournalFragment.DATE_LABEL) ?: ""

        val context: Context = this.context ?: return
        typeface = ResourcesCompat.getFont(context, R.font.quicksand_light)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentStatsBinding.inflate(inflater, container, false)
        initializeUI(binding)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        refreshAsync()
        isFragmentResumed = true
    }

    override fun onPause() {
        super.onPause()
        isFragmentResumed = false
    }

    override fun onDestroy() {
        masterJob.cancel()
        super.onDestroy()
    }

    override fun onPauseFragment() {
        Timber.tag(Constants.APP_TAG).d("Fragment paused")
    }

    override fun onResumeFragment() {
        Timber.tag(Constants.APP_TAG).d("Fragment resumed")
    }
    // endregion

    // region Data
    fun refreshAsync() {
        // reset previous job:
        masterJob.cancel()
        masterJob = Job()

        launch(this.coroutineContext) {
            if (!isAnimationPlayed) {
                Thread.sleep(1000)
                isAnimationPlayed = true
                if (!isCurrentTimeFrame) {
                    async { refreshData() }.await()
                    withContext(Dispatchers.Main) {
                        updateUIWithValues()
                    }
                }
            } else {
                async { refreshData() }.await()
                withContext(Dispatchers.Main) {
                    updateUIWithValues()
                }
            }
        }
    }

    private fun refreshData() {
        statsData = repository.getStatsData(date, timeFrame)
    }

    private fun getDaysRange(timeFrame: TimeFrame, monthDaysAmount: Int = 28): IntRange {
        return if (timeFrame == TimeFrame.WEEK) {
            1..7
        } else {
            1..monthDaysAmount
        }
    }
    // endregion

    // region UI
    private fun initializeUI(binding: FragmentStatsBinding) {
        initViews(binding)
        initCharts()
        initBarChartListener()
        initRV()
        // load data for first time frame first, then load async other time-frames
        if (isCurrentTimeFrame) {
            refreshData()
            updateUIWithValues()
        }
    }

    private fun initViews(binding: FragmentStatsBinding) {
        timeFrameTV = binding.weekTv
        nestedScrollView = binding.nestedScroll
        hoursWorkedTV = binding.hoursWorked
        chartWeek = binding.hrsWorked
        pieWeek = binding.pieChart
        averageHoursTV = binding.averageHours
        performanceTV = binding.performance
        tasksRV = binding.tasksStats
        noDataContainer = binding.noDataContainer
        noDataLabel = binding.noData
        statsTop = binding.statsTop
        statsDetailed = binding.statsDetailed
        statsTimeFrameLabel = binding.timeframeLabel
        bar = binding.bar
        dayTime = binding.dayTime
        weekDay = binding.weekDay
        separator = binding.separator2
        performanceIncreased = binding.performanceIncreased
        performanceDecreased = binding.performanceDecreased
        nestedScrollView?.isFocusableInTouchMode = true
        nestedScrollView?.descendantFocusability = ViewGroup.FOCUS_BEFORE_DESCENDANTS
    }

    private fun initCharts() {
        chartWeek?.apply {
            renderer = RoundedBarChartRenderer(this, animator, viewPortHandler, 12f)
            disableBarChartDefaults()
            hideBarChartAxises()
        }
        pieWeek?.apply {
            disablePieChartDefaults()
            addMarkerView(this@StatsFragment.context)
        }
    }

    private fun initRV() {
        tasksAdapter = TasksDetailsAdapter(requireContext())
        tasksRV?.adapter = tasksAdapter
    }

    private fun updateUIWithValues(animate: Boolean = true) {
        statsData?.apply {
            timeFrameTV?.text = periodDays
            if (handleUiVisibility(tasks.isNotEmpty(), timeFrame)) {
                updateTextViews()
                updateCharts(animate)
                tasksAdapter?.refresh(tasks)
            }
        }
    }

    private fun handleUiVisibility(isDataCollected: Boolean, timeFrame: TimeFrame): Boolean {
        if (isDataCollected) {
            noDataContainer?.visibility = View.GONE
            statsDetailed?.visibility = View.VISIBLE
            statsTop?.visibility = View.VISIBLE
        } else {
            statsDetailed?.visibility = View.GONE
            statsTop?.visibility = View.GONE
            noDataContainer?.visibility = View.VISIBLE
            noDataLabel?.text = "${context?.getString(R.string.no_data_collected_this)} ${if (timeFrame == TimeFrame.WEEK) {
                context?.getString(R.string.week)
            } else context?.getString(R.string.month)}"
        }
        return isDataCollected
    }

    private fun updatePerformance(currentTimeFramePeriod: Float, previousTimeFramePeriod: Float) {
        // reset performance arrow visibility:
        performanceDecreased?.visibility = View.GONE
        performanceIncreased?.visibility = View.GONE

        val performance = currentTimeFramePeriod - previousTimeFramePeriod
        when {
            previousTimeFramePeriod == 0f -> {
                performanceTV?.text = context?.getString(R.string.no_data)
            }
            performance == 0f -> {
                performanceTV?.text = context?.getString(R.string.same_time)
            }
            performance > 0 -> {
                performanceTV?.text = "${formatTimeToString(performance)}${context?.getString(R.string._h)}"
                performanceIncreased?.visibility = View.VISIBLE
            }
            else -> {
                performanceTV?.text = "${formatTimeToString(abs(performance))}${context?.getString(R.string._h)}"
                performanceDecreased?.visibility = View.VISIBLE
            }
        }
    }

    private fun StatsData.updateTextViews() {
        with(this) {
            hoursWorkedTV?.text = formatTimeToString(overallTime)
            averageHoursTV?.text = "$workedDays / ${days.size}"
            updatePerformance(overallTime, previousTimeFramePeriod)
            if (timeFrame == TimeFrame.WEEK) {
                statsTimeFrameLabel?.text = context?.getString(R.string.weekly)
            } else {
                statsTimeFrameLabel?.text = context?.getString(R.string.monthly)
            }
        }
    }

    private fun StatsData.updateCharts(animate: Boolean) {
        with(this) {
            if (animate) {
                chartWeek?.animateY(500)
                pieWeek?.animateY(1000, Easing.EaseOutBack)
            }

            // pie chart:
            pieWeek?.data = pieData
            pieWeek?.invalidate()

            // bar chart:
            chartWeek?.data = barData
            chartWeek?.drawBarChartLimitLines(barData, averageHours, overallTime, typeface!!, context)
            chartWeek?.initBarChartDaysLabels(isCurrentTimeFrame, daysNames, getDaysRange(timeFrame, daysNames.size), DateTime().dayOfWeek)
            chartWeek?.initStyle(timeFrame, daysNames.size)
//            chartWeek?.xAxis?.labelCount = 7
            chartWeek?.invalidate()
        }
    }

    private fun initBarChartListener() {
        chartWeek?.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onNothingSelected() {
                showDayTasks(null)
            }

            override fun onValueSelected(e: Entry?, h: Highlight?) {
                // highlight inverse:
                if (e != null && e.x >= 0 && e.y > 0) {
                    val invertedHighlights = createInverseHighlights(e.x.toInt(), statsData?.days?.size
                            ?: 6)
                    chartWeek?.highlightValues(invertedHighlights)
                    // load selected day (day, time, tasks)
                    showDayTasks(statsData?.days?.get(e.x.toInt()))
                } else {
                    showDayTasks(null)
                }
            }
        })
    }

    private fun showDayTasks(selectedDay: DateTime?) {
        if (selectedDay != null) {
            repository.getSelectedDayTime(selectedDay).apply {
                dayTime?.visibility = View.VISIBLE
                weekDay?.visibility = View.VISIBLE
                separator?.visibility = View.VISIBLE
                dayTime?.text = this.trackedTime
                weekDay?.text = this.formattedDate
                if (this.tasks.isNotEmpty()) {
                    tasksAdapter?.refresh(this.tasks)
                }
            }
        } else {
            dayTime?.visibility = View.GONE
            weekDay?.visibility = View.GONE
            separator?.visibility = View.GONE
            tasksAdapter?.refresh(statsData?.tasks ?: mutableListOf())
        }
    }
    // endregion

    companion object {
        const val TIME_FRAME = "TIME_FRAME"
        const val DATE = "DATE"
        const val DATE_LABEL = "DATE_LABEL"

        fun getInstance(timeFrame: TimeFrame, date: DateTime, dateLabel: String): StatsFragment {
            val fragment = StatsFragment()
            fragment.arguments = Bundle().apply {
                putString(TIME_FRAME, timeFrame.toString())
                putString(DATE, date.toString())
                putString(DATE_LABEL, dateLabel)
            }
            return fragment
        }
    }
}
