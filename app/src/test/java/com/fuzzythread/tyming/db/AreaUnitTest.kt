package com.fuzzythread.tyming.db

import com.fuzzythread.tyming.global.db.entities.AreaEntity
import com.fuzzythread.tyming.global.db.entities.AreaEntity_
import org.junit.Test

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull

class AreaUnitTest : AbstractObjectBoxTest() {

    @Test
    fun emptyAreaTest() {
        val areaBox = store.boxFor(AreaEntity::class.java)
        assertEquals(0, areaBox.all.size)
    }

    @Test
    fun addAreaTest() {
        val areaBox = store.boxFor(AreaEntity::class.java)
        areaBox.put(AreaUnitTest.AREA)
        assertEquals(1, areaBox.all.size)
    }

    @Test
    fun deleteAreaTest() {

        val areaBox = store.boxFor(AreaEntity::class.java)
        areaBox.put(AreaUnitTest.AREA)

        val dbArea = areaBox.query().build().findFirst()
        assertNotNull(dbArea)
        assertEquals(AreaUnitTest.AREA.notes, dbArea!!.notes)
        assertEquals(AreaUnitTest.AREA.title, dbArea.title)

        areaBox.remove(dbArea)
        assertEquals(0, areaBox.all.size)
    }

    @Test
    fun contentAreaTest() {

        val areaBox = store.boxFor(AreaEntity::class.java)
        areaBox.put(AreaUnitTest.AREA)

        val dbArea = areaBox.query().build().findFirst()
        assertNotNull(dbArea)
        assertEquals(AreaUnitTest.AREA.notes, dbArea!!.notes)
        assertEquals(AreaUnitTest.AREA.title, dbArea.title)
    }

    @Test
    fun findQueryAreaTest() {

        val areaBox = store.boxFor(AreaEntity::class.java)
        areaBox.put(AreaUnitTest.AREA)

        val dbArea = areaBox.query().equal(AreaEntity_.id, AreaUnitTest.AREA.id)
                .build().findFirst()
        assertNotNull(dbArea)
        assertEquals(AreaUnitTest.AREA.notes, dbArea!!.notes)
        assertEquals(AreaUnitTest.AREA.title, dbArea.title)
        assertEquals(AreaUnitTest.AREA.color, dbArea.color)
    }

    companion object {

        private val AREA = AreaEntity()

        init {
            AREA.notes = "Test note"
            AREA.title = "Test title"
            AREA.color = "#FFFFFF"
        }
    }
}
