package com.fuzzythread.tyming.db

import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.ProjectEntity_

import org.junit.Test

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull

class ProjectUnitTest : AbstractObjectBoxTest() {

    @Test
    fun emptyProjectTest() {
        val projectBox = store.boxFor(ProjectEntity::class.java)
        assertEquals(0, projectBox.all.size)
    }

    @Test
    fun addProjectTest() {
        val projectBox = store.boxFor(ProjectEntity::class.java)
        projectBox.put(ProjectUnitTest.PROJECT)
        assertEquals(1, projectBox.all.size)
    }

    @Test
    fun deleteProjectTest() {

        val projectBox = store.boxFor(ProjectEntity::class.java)
        projectBox.put(ProjectUnitTest.PROJECT)

        val dbProject = projectBox.query().build().findFirst()
        assertNotNull(dbProject)
        assertEquals(ProjectUnitTest.PROJECT.notes, dbProject!!.notes)
        assertEquals(ProjectUnitTest.PROJECT.title, dbProject.title)

        projectBox.remove(dbProject)
        assertEquals(0, projectBox.all.size)
    }

    @Test
    fun contentProjectTest() {

        val projectBox = store.boxFor(ProjectEntity::class.java)
        projectBox.put(ProjectUnitTest.PROJECT)

        val dbProject = projectBox.query().build().findFirst()
        assertNotNull(dbProject)
        assertEquals(ProjectUnitTest.PROJECT.notes, dbProject!!.notes)
        assertEquals(ProjectUnitTest.PROJECT.title, dbProject.title)
    }

    @Test
    fun findQueryProjectTest() {

        val projectBox = store.boxFor(ProjectEntity::class.java)
        projectBox.put(ProjectUnitTest.PROJECT)

        val dbProject = projectBox.query().equal(ProjectEntity_.id, ProjectUnitTest.PROJECT.id)
                .build().findFirst()
        assertNotNull(dbProject)
        assertEquals(ProjectUnitTest.PROJECT.notes, dbProject!!.notes)
        assertEquals(ProjectUnitTest.PROJECT.title, dbProject.title)
        assertEquals(ProjectUnitTest.PROJECT.color, dbProject.color)
    }

    companion object {

        private val PROJECT = ProjectEntity()

        init {
            PROJECT.notes = "Test note"
            PROJECT.title = "Test title"
            PROJECT.color = "#FFFFFF"
        }
    }
}
