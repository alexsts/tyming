package com.fuzzythread.tyming.viewmodels

import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TaskViewModel
import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TaskVMUnitTest {

    @Test
    fun checkProjectVmField() {
        assertEquals(title, TASK_VM.title)
        assertEquals(notes, TASK_VM.notes)
        assertEquals(id, TASK_VM.id)
        assertEquals(createdDate, TASK_VM.createdDate)
    }

    @Test
    fun checkProjectVmFieldsEmpty() {

        val tempProjectVM = TaskViewModel(context = null)

        assertEquals("", tempProjectVM.title)
        assertEquals("", tempProjectVM.notes)
        assertEquals(0, tempProjectVM.id)
        assertEquals(null, tempProjectVM.createdDate)
    }

    companion object {

        private val TASK_VM = TaskViewModel(context = null)

        private const val title = "Test title"
        private const val notes = "Test note"
        private const val id = 1.toLong()
        private val createdDate = Date()

        init {
            TASK_VM.notes = notes
            TASK_VM.title = title
            TASK_VM.id = id
            TASK_VM.createdDate = createdDate
        }
    }
}
